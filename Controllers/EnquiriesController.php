<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\Enquiry;
use App\Model\Country;
use App\Model\Technology;
use App\Model\EnquirySuccess;
use App\Model\Configuration;
use App\Model\Project;
use App\Model\EnquiryCancel;
use App\Model\EnquiryViewer;
use App\Model\enquiryNotResponse;
use App\Model\EmailThread;
use App\User;
use App\Model\EnquiryMessage;
use App\Events\EnquiryNotResponseRequest;
use App\Events\EnquiryCancelRequest;
use App\Events\EnquirySuccessRequest;
use App\Events\MailSendToEnqOwner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class EnquiriesController extends Controller
{

    public $statusTypes = array('New' => 0, 'Running' => 0, 'Success' => 0, 'Hold' => 0, 'Cancelled' => 0, 'Not Response' => 0);

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        /**
         * @desc set controller name
         */
        $this->controller = "Enquiries";

        /**
         * @desc set page title
         */
        $this->title = "Pre-Sales";

        $this->model = Enquiry::class;
    }

    /**
     * Show the application dashboard.
     * 
     * @method index
     * @param Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $this->_data['params'] = $request->all();
        $this->_data['limit'] = $this->limit = $request->get('perpage') ? $request->get('perpage') : $this->limit;
        $this->_data['search'] = $request;

        $filter_data = $request->all();
        $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;
        $status = (!empty($request->get('status')))?$request->get('status'):'';
        $query = Enquiry::where(function($query) use($status){
            if (isset($status) && !empty($status)) {
                $query->where('status', ucfirst(str_replace('_',' ',$status)));
            } else {
                $query->whereIn('status', ['New', 'Running']);
            }
            if (custom_permit('enquiries')) {
                $query->where('assigned_to', Auth::user()->role_id);
            }
        });
        $query = $query->orderBy('id', 'DESC');
        // add filter in records
        searching_string($query, $request->form);

        // sort records
        db_sort($query, $request);
        
        $data = $query->paginate($limit);
        
        $this->_data['data'] = $data;

        // total enquiry data
        $totalStatusArr = $this->statusTypes;
        $queryTotalStatusCounts = Enquiry::select('status', \DB::raw('count(status) as status_count'));
        if (custom_permit('enquiries')) {
            $queryTotalStatusCounts->where('assigned_to', Auth::user()->role_id);
        }
        $totalStatusCounts = $queryTotalStatusCounts->groupBy('status')->get();
        foreach ($totalStatusCounts as $totalStatusCount) {
            if ($totalStatusCount->status !== 'Running' || $totalStatusCount->status !== 'New') {
                $totalStatusArr[$totalStatusCount->status] = $totalStatusCount->status_count;
            }
        }
        $this->_data['totalStatusArr'] = $totalStatusArr;

        return view('front.enquiries.index', $this->_data);
    }

    /**
     * show detail form
     *
     * @method show
     * @param Request $request, $id
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, $id) 
    {
        try {
            $queryFindEnquiry = Enquiry::where('id', $id);
            if (custom_permit('enquiries')) {
                $queryFindEnquiry->where('assigned_to', Auth::user()->role_id);
            }
            $enquiryDetail = $queryFindEnquiry->with('getSuccessEnquiryData','getCancelEnquiryData','getNoResEnquiryData','getApprovedByUser')->first();
            if (!$enquiryDetail) {
                $request->session()->flash('error', 'Sorry, You are not authorized for this.');
                return redirect()->to(route('enquiries.index'));
            }

            $curDateTime =  Carbon::now();
            $curDate =  $curDateTime->toDateString();
            if (isset($enquiryDetail['getSuccessEnquiryData']->technology)) { // fetch technologies name
                $technologies = explode(',',$enquiryDetail['getSuccessEnquiryData']->technology);
                $getTechnologiesSeprate = Technology::whereIn('id',$technologies)->pluck('title')->toArray();
                $enquiryDetail['getSuccessEnquiryData']['getTechnology'] = implode(', ',$getTechnologiesSeprate);
            }
            
            $this->_data["data"]             =  $enquiryDetail;
            $this->_data["enquiry_messages"] =  EnquiryMessage::where('enquiry_id', $id)->with(["author"])->get();
            $this->_data["technologies"]     =  Technology::where('status', Technology::Active)->pluck('title','id');
            $getUsers                        =  User::whereIn('id', explode(',',$this->_data["data"]['assigned_to']))
                                                ->where('status', User::Active)
                                                ->pluck('name')->toArray();
            $this->_data["users"]            =  implode(', ',$getUsers);
            $this->_data["website_url"]      =  Configuration::where('key', 'website_url')->first();

               $emailThreadList = array();
               $v = 0;
               $emailThreadsParents   =  EmailThread::where('lead_id', $id)
                                            ->where('parent_id', NULL)
                                            ->with('getUserDetail','getEnquiryDetail')
                                            ->get();
                foreach($emailThreadsParents as $key => $emailThreadsParent){
                    $emailThreadList[$v] = $emailThreadsParent;
                    $v++;
                    $emailThreadsChilds   =  EmailThread::where('parent_id', $emailThreadsParent->id)
                                            ->with('getUserDetail','getEnquiryDetail')
                                            ->orderBy('id', 'ASC')
                                            ->get();
                        foreach($emailThreadsChilds as $emailThreadsChild){      
                            $emailThreadList[$v] = $emailThreadsChild;
                            $v++;
                        }                       
                }                                    
                $this->_data["email_threads"] = $emailThreadList;
                                             
            // data insert and update for enquiry viewer users list
            $getData = EnquiryViewer::where('enquiry_id', $id)
                        ->where('user_id', \Auth::user()->id)
                        ->whereDate('created_at', '>=', $curDate." 00:00:00")
                        ->whereDate('created_at', '<=', $curDate." 23:59:59")
                        ->first();

            if (isset($getData) && !empty($getData)) {
                $enquiryViewer = EnquiryViewer::find($getData->id);
                $enquiryViewer->updated_at = $curDateTime;
                $enquiryViewer->save();
            } else {
                $enquiryViewerData['enquiry_id']    =   $id;                                        
                $enquiryViewerData['user_id']       =   \Auth::user()->id;                                        
                EnquiryViewer::create($enquiryViewerData);
            }

            return view('front.enquiries.show', $this->_data); 
        } catch(\Illuminate\Database\QueryException $ex) {
            return redirect()->back()->with(['message' => $ex->getMessage(), 'alert-class' => 'alert-danger']);
        }
    }

    /**
     * show create form
     *
     * @method create
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request) 
    {
        $this->_data["breadcrumb"]["Create"] =  "javascript:void(0)";
        $this->_data["countries"] = Country::where('status', 1)->pluck('name', 'name');
        $this->_data["users"] = User::whereIn('role_id', [User::PRE_SALE, User::POST_SALE, User::MANAGER])->where('status', User::Active)->pluck('name', 'id');
        $this->_data["services"] = ['Mobile App Development' => 'Mobile App Development','Web Development' => 'Web Development', 'Custom Software' => 'Custom Software','Readymade Solutions' => 'Readymade Solutions','Hire Dedicated Developer' => 'Hire Dedicated Developer', 'Partnership Opportunities' => 'Partnership Opportunities'];
        
        return view('front.enquiries.create', $this->_data);
    }

    /**
     * store new records
     *
     * @method store
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) 
    {
        $request->validate(Enquiry::webRules(), Enquiry::rulesMessages());
        $data = $request->all();
        if ($request->hasFile("file")) {
            $files = $request->file("file");
            foreach ($files as $file) {
                $f[] = _upload($file, "enquiries");
            }
            $data["file"] = implode(",", $f);
        }
        if (isset($data['service_id']) && !empty($data['service_id'])) {
            $data["service_id"] = implode(",", $data['service_id']);
        }
        if (isset($data['assigned_to']) && !empty($data['assigned_to'])) {
            $data["assigned_to"] = implode(",", $data['assigned_to']);
        }
        $store = Enquiry::create($data);
        if ($store) {
            $request->session()->flash('success', $this->success_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route('enquiries.index'));
    }

    /**
     * show edit form
     *
     * @method edit
     * @param Request $request
     * @param Enquiry $enquiry
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request, Enquiry $enquiry) 
    {
        if ($enquiry->assigned_to === Auth::user()->id || Auth::user()->role_id === 1) {
            $this->_data["breadcrumb"]["Update"] =  "javascript:void(0)";
            $enquiry->load('getUserDetail');
            $enquiry['user_type'] = $enquiry->getUserDetail['admin_type'];
            $enquiry["service_id"] = explode(",", $enquiry['service_id']);
            $enquiry["assigned_to"] = explode(",", $enquiry['assigned_to']);
            $this->_data["data"] = $enquiry;
            $this->_data["website_url"]      =  Configuration::where('key', 'website_url')->first();
            $this->_data["countries"] = Country::where('status', 1)->pluck('name', 'name');
            $this->_data["users"] = User::whereIn('role_id', [User::PRE_SALE, User::POST_SALE, User::MANAGER])->pluck('name','id');
            $this->_data["services"] = ['Mobile App Development' => 'Mobile App Development','Web Development' => 'Web Development', 'Custom Software' => 'Custom Software','Readymade Solutions' => 'Readymade Solutions','Hire Dedicated Developer' => 'Hire Dedicated Developer', 'Partnership Opportunities' => 'Partnership Opportunities'];

            return view('front.enquiries.edit', $this->_data);
        } else {
            $request->session()->flash('error', 'Sorry, You are not authorized for this.');
            return redirect()->to(route('enquiries.index'));
        }
    }

    /**
     * update information
     *
     * @method update
     * @param Request $request
     * @param Enquiry $enquiry
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Enquiry $enquiry) 
    {
        $request->validate(Enquiry::webRules(), Enquiry::rulesMessages());
        $data = $request->except("_token");
        if ($request->hasFile("file")) {
            $files = $request->file("file");
            foreach ($files as $file) {
                $f[] = _upload($file, "enquiries");
            }
            $data["file"] = implode(",", $f);
        }
        if (isset($data['service_id']) && !empty($data['service_id'])) {
            $data["service_id"] = implode(",", $data['service_id']);
        }
        if (isset($data['assigned_to']) && !empty($data['assigned_to'])) {
            $data["assigned_to"] = implode(",", $data['assigned_to']);
        }
        $data['add_from'] = 0;
        $udpate = $enquiry->update($data);
        if ($udpate) {
            $request->session()->flash('success', $this->update_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route('enquiries.index'));
    }

    /**
     * enquirySuccess
     *
     * @method enquirySuccess
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function enquirySuccess(Request $request){
        $validator = Validator::make(
            $request->all(), EnquirySuccess::rules()
        );
        if ($validator->passes()) {
            try {
                $data = $request->all();
                $enqSuccessOldData = EnquirySuccess::where('enquiry_id', $request->get('enquiry_id'))->first();
                if (!empty($enqSuccessOldData)) {
                    $enqSuccessOldData->user_id = \Auth::user()->id;
                    $enqSuccessOldData->project_type = $data['project_type'];
                    $enqSuccessOldData->project_support = $data['project_support'];
                    $enqSuccessOldData->project_amount = $data['project_amount'];
                    $enqSuccessOldData->fee_included = (isset($data['fee_included']) && !empty($data['fee_included']))?$data['fee_included']:0;
                    $enqSuccessOldData->payment_type = $data['payment_type'];
                    if (isset($data['technology']) && !empty($data['technology'])) {
                        $enqSuccessOldData->technology = implode(",", $data['technology']);
                    }
                    $enqSuccessOldData->payment_detail = $data['payment_detail'];
                    $enqSuccessOldData->save();
                } else {
                    $data['user_id'] = \Auth::user()->id;
                    if (isset($data['technology']) && !empty($data['technology'])) {
                        $data["technology"] = implode(",", $data['technology']);
                    }
                    EnquirySuccess::create($data);
                }
    
                $enquiry = Enquiry::find($request->get('enquiry_id'));
                $enquiry->status = 'Success';
                $enquiry->save();

                event(new EnquirySuccessRequest($enquiry));
            } catch (\Exception $e) {
                return throughGetMessage($e);
            }
            return response()->json(['success' => 'Enquiry has been updated successfully.']);
        } else {
            return response()->json(['error' => $validator->errors()->all()]);
        }
    }

    /**
     * enquiryStatusChange
     *
     * @method enquiryStatusChange
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function enquiryStatusChange(Request $request){
        
        if ($request->get('status') == 'Completed' || $request->get('status') == 'Not Response' || $request->get('status') == 'Hold') {
            try {
                $enquiry = Enquiry::find($request->get('enquiry_id'));
                $enquiry->status = $request->get('status');
                $enquiry->save();
                enquiryNotResponse::updateOrCreate(
                    ['enquiry_id' =>  $request->get('enquiry_id')],
                    ['user_id' => \Auth::user()->id]
                );
                //event(new EnquiryNotResponseRequest($enquiry));
            } catch (\Exception $e) {
                return throughGetMessage($e);
            }
            return response()->json(['success' => 'Enquiry has been updated successfully.']);
        } else {
            $validator = Validator::make(
                $request->all(), EnquiryCancel::rules(), EnquiryCancel::messages() 
            );
            if ($validator->passes()) {
                try {
                    $data = $request->all();
                    $fetchEnquiry = EnquiryCancel::where('enquiry_id', $request->get('enquiry_id') )->first();
                    if (!empty($fetchEnquiry)) {
                        if ($request->hasFile("douments")) {
                            $files = $request->file("douments");
                            foreach ($files as $file) {
                                $v[] = _upload($file, "enquiries");
                            }
                            $fetchEnquiry->douments = implode(",", $v);
                        }
                        $fetchEnquiry->user_id = \Auth::user()->id;
                        $fetchEnquiry->reason = $data["reason"];
                        $fetchEnquiry->save();
                    } else {
                        if ($request->hasFile("douments")) {
                            $files = $request->file("douments");
                            foreach ($files as $file) {
                                $v[] = _upload($file, "enquiries");
                            }
                            $data["douments"] = implode(",", $v);
                        }
                        $data['user_id'] = \Auth::user()->id;
                        EnquiryCancel::create($data);
                    }
        
                    $enquiry = Enquiry::find($request->get('enquiry_id'));
                    $enquiry->status = $request->get('status');
                    $enquiry->save();
                    //event(new EnquiryCancelRequest($enquiry));
                } catch (\Exception $e) {
                    return throughGetMessage($e);
                }
                return response()->json(['success' => 'Enquiry has been updated successfully.']);
            } else {
                return response()->json(['error' => $validator->errors()->all()]);
            }
        }
    }

    /**
     * store new message
     *
     * @method message
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function message(Request $request) 
    {
        $request->validate(EnquiryMessage::rules(), EnquiryMessage::rulesMessage());
        $data = $request->all();
        $data['user_id'] = Auth::id();
        if ($request->hasFile("file")) {
            $files = $request->file("file");
            foreach ($files as $file) {
                $f[] = _upload($file, "enquiries");
            }
            $data["file"] = implode(",", $f);
        }
        $data['reminder_at'] = $data['reminder_at'].':00';
        $store = EnquiryMessage::create($data);
        if ($store) {
            $request->session()->flash('success', $this->success_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route('enquiries.show', $data['enquiry_id']));
    }

    /**
     * rejected information
     *
     * @method reject
     * @param Request $request
     * @param Enquiry $enquiry
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reject(Request $request)
    {
        $enquiry = Enquiry::find($request->get('id'));

        if ($enquiry->status == 'Cancelled' || $enquiry->status == 'Not Response') {
            EnquiryCancel::where('enquiry_id', $request->get('id'))->where('user_id', \Auth::user()->id)->delete();
        } else if ($enquiry->status == 'Success') {
            EnquirySuccess::where('enquiry_id', $request->get('id'))->where('user_id', \Auth::user()->id)->delete();
        }
        $enquiry->status      = 'New';
        $enquiry->approved_by =  null;
        $update = $enquiry->save();

        if ($update) {
            $request->session()->flash('success', $this->update_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route('enquiries.index'));
    }

    /**
     * approved information
     *
     * @method approve
     * @param Request $request
     * @param Enquiry $enquiry
     * @return \Illuminate\Http\RedirectResponse
     */
    public function approve(Request $request, Enquiry $enquiry) 
    {
        $data = $request->all();
        
        $enquiryDetail = Enquiry::where('id', $data['id'])->first()->toArray();
        
        if (!($enquiryDetail['status'] == 'Cancelled')) {
            $enquirySuccessDetail = EnquirySuccess::where('enquiry_id', $data['id'])->first()->toArray();
        }
        
        // Start - create project if status = success
        if ((isset($enquiryDetail) && !empty($enquiryDetail)) && (isset($enquirySuccessDetail) && !empty($enquirySuccessDetail)) && $enquiryDetail['status'] === 'Success') {
            $unsetEnquiryFields = array('id', 'deleted_at', 'created_at', 'updated_at', 'assigned_to', 'assigned_by', 'approved_by', 'status');
            $enquiryDetail['pre_assigned_to'] = $enquiryDetail['assigned_to'];
            $enquiryDetail['pre_assigned_by'] = $enquiryDetail['assigned_by'];
            $enquiryDetail['pre_approved_by'] = $enquiryDetail['approved_by'];
            $enquiryDetail['created_by'] = \Auth::user()->id;
            $enquiryDetail['enquiry_id'] = $enquiryDetail['id'];
            foreach ($unsetEnquiryFields as $key => $unsetEnquiryField) {
                unset($enquiryDetail[$unsetEnquiryField]);
            }

            $setEnquirySuccessFields = array('project_type', 'project_support', 'project_amount', 'fee_included', 'payment_type', 'technology', 'payment_detail');
            $enquiryDetail['success_by'] = $enquirySuccessDetail['user_id'];
            foreach ($setEnquirySuccessFields as $key => $setEnquirySuccessField) {
                $enquiryDetail[$setEnquirySuccessField] = $enquirySuccessDetail[$setEnquirySuccessField];
            }
            Project::create($enquiryDetail);
        }
        // End - create project if status = success

        $udpate = Enquiry::where('id', $data['id'])->update(['approved_by' => Auth::user()->id]);
        if ($udpate) {
            $request->session()->flash('success', $this->update_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route('enquiries.index'));
    }

    /**
     * mail send
     *
     * @method get
     * @param Request $request
     * @param mail data $enquiry
     * @return \Illuminate\Http\RedirectResponse
     */

     public function mailSend(Request $request, $id){
        $this->_data["breadcrumb"]["Create"] =  "javascript:void(0)";
        $this->_data["id"] =  $id;
        return view('front.enquiries.mail', $this->_data);
     }

     public function mailSubmit(Request $request){
         
        $request->validate(EmailThread::rules());
        $data = $request->all();
        
        $data['user_id'] = \Auth::user()->id;
        $data['status'] = EmailThread::sent;
        if ($request->hasFile("attachments")) {
            $files = $request->file("attachments");
            foreach ($files as $file) {
                $f[] = _upload($file, "enquiries");
            }
            $data["attachments"] = implode(",", $f);
        }
        
        $store = EmailThread::create($data);
        $enquiry = Enquiry::find($data['lead_id']);
        
        event(new MailSendToEnqOwner($enquiry, $data['subject'], $data['message']));
        if ($store) {
            $request->session()->flash('success', $this->mail_send);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route('enquiries.index'));
     }

     public function customerMailSubmit(Request $request){
        $validator = Validator::make(
            $request->all(), EmailThread::rules()
        );
        if ($validator->passes()) {
            try {
                $data = $request->all();
                $emailThread = EmailThread::find($data['table_id']);
                
                if ($request->hasFile("attachments")) {
                    $files = $request->file("attachments");
                    foreach ($files as $file) {
                        $f[] = _upload($file, "enquiries");
                    }
                    $data["attachments"] = implode(",", $f);
                }
                $data["parent_id"]  =   $data['table_id'];
                $data["lead_id"]    =   $emailThread->lead_id;
                $data['status']     =   EmailThread::received;
                EmailThread::create($data);

            } catch (\Exception $e) {
                return throughGetMessage($e);
            }
            return response()->json(['success' => 'Customer reply has been updated successfully.']);
        } else {
            return response()->json(['error' => $validator->errors()->all()]);
        }
     }

}
