<?php

namespace App\Http\Controllers;

use App\Model\Enquiry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\User;

class ApiController extends Controller
{


    /**
     * Save enquiry Filled by front user.
     *
     */
    public function saveEnquiry(Request $request) {
        
        $request->validate(Enquiry::rules());
        $data = $request->all();
        $curDate = date("Y-m-d");
        if($request->hasFile("file")) {
            $files = $request->file("file");
            $f = array();
            foreach ($files as $file){
                $f[] = _upload($file, "enquiries");
            }
            $data["file"] = implode(",", $f);
        }
        if(empty($request->input('country_id'))){
            $ip = request('client_ip_address');
            $details = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=".$ip));
            $data["country_id"] = $details['geoplugin_countryName'];
        }
        $data["add_from"] = Enquiry::FromSemidotWebsite;
        
        // Assign user for enquiry
        $countEnqUser = array();
        $getPreSaleUsers = User::where('role_id', User::PRE_SALE)->get();
        foreach($getPreSaleUsers as $getPreSaleUser){
            $enqData = Enquiry::whereRaw("find_in_set($getPreSaleUser->id , assigned_to)")
                                    ->where('created_at', '>=', $curDate. " 00:00:00")
                                    ->count();
            $countEnqUser[$getPreSaleUser->id]  = $enqData;
        }
        $minEnqAssigned = array_keys($countEnqUser, min($countEnqUser));
        $data['assigned_to'] = $minEnqAssigned[0];

        $store = Enquiry::create($data);
        if($store) {
            return response()->json(["status" => true]);
        } else {
            return response()->json(["status" => false,  "msg" => "Oops"]);
        }
    }
}
