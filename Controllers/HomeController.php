<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use App\Model\Announcement;
use App\Model\Job;
use App\Model\Course;
use App\Model\Page;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Mail\ForgotPasswordAdminUser;
use Illuminate\Support\Facades\Mail;

use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->_data['menus'] = get_menus();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (!Auth::check()){
            return view('front.login');
        }

        $modules = [
                    
                    ['title' => "Users", 'icon' => icon("users"), 'link' => ("admin.users.users.index"), "count" => User::where("role_id", "!=", User::ADMIN)->count()],
                    ['title' => "Announcements", 'icon' => icon("announcements"), 'link' => ("admin.announcements.announcements.index"), "count" => Announcement::where("status", "!=", '')->count()],
                    ['title' => "Jobs", 'icon' => icon("jobs"), 'link' => ("admin.jobs.jobs.index"), "count" => Job::where("status", "!=", '')->count()],
                    ['title' => "Courses", 'icon' => icon("Courses"), 'link' => ("admin.courses.courses.index"), "count" => Course::where("status", "!=", '')->count()],
                    /*['title' => "Web Pages", 'icon' => icon("Web Pages"), 'link' => ("admin.pages.pages.index"), "count" => Page::where("status", "!=", '')->count()],*/
                   ];
        $this->_data["modules"] = $modules;
        
        return view('admin.dashboard', $this->_data);
    }

    /**
     * Edit Profile
     *
     * 
     */
    public function editProfile(Request $request)
    {
        if ($request->isMethod("put")) {
            $request->validate([
                "name" => "required",
                "password" => "nullable|min:6|max:20"
            ]);
            $data = $request->except("_token", "email");
            if($data["password"]  == ""){
                unset($data["password"]);
            } else {
                $data["password"] = bcrypt($data["password"]);
            }
            $user = get_user_info();
            $udpate = $user->update($data);
            if($udpate) {
                $request->session()->flash('success', "Profile has been successfully updated.");
            } else {
                $request->session()->flash('error', $this->error_response);
            }
        }

        $this->_data["data"] = get_user_info();
        return view('front.edit-profile', $this->_data);
    }
    public function forgot_password(){
        if (!Auth::check()){
            return view('admin.forgot_password');
        }
    }

    public function privacyPolicy(){
        return view('front.privacy_policy');
    }
    public function forgot_password_link(Request $request){
        $input = $request->all();
        
        $user_data  = User::where("status", '1')->where('email',$input['email'])
                    ->select("id", "email", "name")
                    ->get()->first();

        if ($user_data->count() >1) {

            $user_data->token = md5(strtotime(date('Y-m-d H:i:s')));

            $data = array('id'=>$user_data->id,'token'=>$user_data->token);

            User::where('id',$user_data->id)->update(["token"=>$user_data->token]);

            $this->sendForgotPasswordMamil($user_data);
            
             $request->session()->flash('success', 'Please check your mail to reset the password.');
            return redirect()->to('/admin/login'); 
        }else{
           $request->session()->flash('error', 'Email not found.'); 
        }
    
    }
    public function sendForgotPasswordMamil($user_data){
       
        Mail::to($user_data["email"])->send(new ForgotPasswordAdminUser($user_data));
    }
    public function reset_password(Request $request){
        $data = $request->all();
        $uer_data = User::where('token',$data['token'])->get()->first();
        
        if ($uer_data) {
            return view('admin.reset_password',compact('uer_data'));
        }else{
            $request->session()->flash('error', 'Invalid token.');
        }
        return redirect()->to('/admin/forgot_password');
        
    }
    public function reet_password_store(Request $request) {
        $data = $request->except("_token");
       
        $request->validate(User::resetPasswordRule($data['id']));       
        
        
        if($data["password"]  == ""){
            unset($data["password"]);
        } else {
            $data["password"] = bcrypt($data["password"]);
            unset($data["confirm_password"]);
        }
        
        $udpate = User::where('id',$data['id'])->update(["password"=>$data["password"],'token'=>'']);
        
        if($udpate) { 
                
            $request->session()->flash('success', 'Password has been reset, Please login.');
        } else {
            $request->session()->flash('error', 'Password has been reset, Please login.');
        }
        return redirect()->to('/admin/login');
    }
}
