<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\Page;
use App\Model\Setting;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use App\Notifications\ContactUs;
use Illuminate\Support\Facades\Notification;

class PagesController extends Controller
{
    /**
     * @method aboutUs
     * @desc Show the about us content.
     *
     */
    public function aboutUs(){
        $this->_data['data'] = Page::getPageData('about-us');
        return view(load_view('front.pages.about_us'), $this->_data);
    }
    /**
     * @method contactUs
     * @desc Show the contact us content.
     *
     */
    public function contactUs(){
        $this->_data['data'] = '';//Page::getPageData('about-us');
        return view(load_view('front.pages.contact_us'), $this->_data);
    }
    /**
     * @method privacyPolicy
     * @desc Show the privacy Policy content.
     *
     */
    public function privacyPolicy(){
        $this->_data['data'] = Page::getPageData('privacy-policy');
        return view(load_view('front.pages.privacy_policy'), $this->_data);
    }
    /**
     * @method termsConditions
     * @desc Show the terms conditions content.
     *
     */
    public function termsConditions(){
        $this->_data['data'] = Page::getPageData('terms-conditions');
        return view(load_view('front.pages.terms_conditions'), $this->_data);
    }
    /**
     * @method contactUsRequest
     * @desc send contact us mail.
     *
     */
    public function contactUsRequest(Request $request){
        $data = $request->all();
        $settings = Setting::getSettings('admin_email');
        Notification::route('mail', $settings->value)->notify(new ContactUs($data));
        $success['success'] =  ___('contact_success');                             
        return response()->json(['msg' => $success, 'status' => true], 200);
    }
}
