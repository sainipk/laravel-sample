<?php

namespace App\Http\Controllers;

use App\Model\Course;
use App\Model\UserCertificate;
use App\Traits\DefaultRoutes;
use App\Traits\LogMessages\BulkActions;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Twilio\Rest\Client;
use Auth, PDF;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, BulkActions, DefaultRoutes;


    public $limit = 10;

    protected $_data = array();

    public $controller, $title, $model;

    public $success_response, $error_response, $update_response, $add_message, $record_activated, $record_deactivated, $record_deleted, $not_authorized, $mail_send;

    public function __construct() {

        $this->_data['controller'] = &$this->controller;
        $this->_data['limits'] =  [10 => 10, 20 => 20, 50 => 50, 100 => 100, 500 => 500];
        $this->_data['title'] = &$this->title;
        $this->_data['current_url'] = url()->current();
        $this->_data['menus'] = get_menus();


        $this->not_authorized       = __('common.not_authorized');
        $this->success_response     = __('common.success');
        $this->error_response       = __('common.oops');
        $this->update_response      = __('common.update');
        $this->add_message          = __('common.success');
        $this->record_activated     = __('common.record_activated');
        $this->record_deactivated   = __('common.record_deactivated');
        $this->record_deleted       = __('common.record_deleted');
        $this->mail_send            = __('common.mail_send');

        $this->defineControllerRoutes();


        $this->_data['routes'] = $this->routes;
    }

    /**
    * Sends sms to user using Twilio's programmable sms client
    * @param String $message Body of sms
    * @param Number $recipients string or array of phone number of recepient
    */
    protected function sendPhoneOTP($message, $recipients){
       
        $sid    = getenv("TWILIO_SID");
        $token     = getenv("TWILIO_AUTH_TOKEN");
        $twilio_number  = getenv("TWILIO_NUMBER");
        
        $client = new Client($sid, $token);

        $client->messages->create('+91'.$recipients, [
            'from' => $twilio_number, 
            'body' => $message
        ]);
    }
    
    /**
    * @method _checkValidGSTNumber
    * check whether customer gst number is valid or not
    * @param gst number
    * @return bool true/false
    */
    protected function _checkValidGSTNumber($gst_number){
        // set post fields
        $endpoint = "https://appyflow.in/api/verifyGST";
        $client = new \GuzzleHttp\Client();
               
       $response = $client->request('POST', $endpoint, ['query' => [
            'gstNo' => $gst_number, 
            'key_secret' => env('GST_KEY_SECRET'),
        ]]);

       $statusCode  = $response->getStatusCode();
       $content     = json_decode($response->getBody(),true);
       
       if (isset($content['error'])) {
           return true; // change this to false when api works
       }
       
       if (isset($content['taxpayerInfo']['sts']) && strtolower($content['taxpayerInfo']['sts'])=='active') {
           return true;
       }
    }

    public function generateCertificate($test_data){
        $course_detail = Course::where('id', $test_data['course_id'])->first();
        $data = ['data' => array('name' => Auth::user()->name, 'course_detail' => $course_detail )];
        $pdf = PDF::loadView('certificate', ['data' => $data]);
        $filename = Auth::user()->id.'_'.time().'.pdf';
        \Storage::put('public/certificate/'.$filename, $pdf->output());

        $data = [
            'user_id' => Auth::user()->id,
            'course_id' => $test_data['course_id'],
            'link' => $filename,
            'issued_at' => date('Y-m-d H:i:s'),
            'status' => UserCertificate::Active
        ];
        UserCertificate::create($data);

        return $filename;
    }
}
