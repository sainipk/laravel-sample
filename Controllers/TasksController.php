<?php

namespace App\Http\Controllers;

use App\User;
use App\Events\TaskReleaseRequest;
use App\Model\ProjectTask;
use App\Model\TaskMessage;
use App\Model\Project;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class TasksController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        parent::__construct();
        /**
         * @desc set controller name
         */
        $this->controller = "Tasks";
        /**
         * @desc set page title
         */
        $this->title = "Tasks";
        $this->model = ProjectTask::class;
    }

    /**
     * Show the application dashboard.
     * 
     * @method index
     * @param Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request){ 
        $this->_data['params'] = $request->all();
        $this->_data['limit'] = $this->limit = $request->get('perpage') ? $request->get('perpage') : $this->limit;
        $this->_data['search'] = $request;

        $filter_data = $request->all();
        $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;
        $query = ProjectTask::query();
            
        // add filter in records
        searching_string($query, $request->form);

        // sort records
        db_sort($query, $request);
        $data = $query->paginate($limit);

        $this->_data['data'] = $data;

        return view('front.tasks.index', $this->_data);
    }

    /**
     * show detail form
     *
     * @method show
     * @param Request $request, $id
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, $id){
        try {
            $query = ProjectTask::where('id', $id);

            $taskDetail = $query->with('getAssignedToUser')->first();
            
            $this->_data['data'] = $taskDetail;
            $this->_data["task_messages"] = TaskMessage::where('project_id', $taskDetail->project_id)
                                            ->where('task_id', $taskDetail->id)
                                            ->with(["author"])
                                            ->get();
            
            return view('front.tasks.show', $this->_data); 
        } catch(\Illuminate\Database\QueryException $ex){
            return redirect()->back()->with(['message' => $ex->getMessage(), 'alert-class' => 'alert-danger']);
        }
    }

    /**
     * store new message
     *
     * @method message
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function message(Request $request) {
        $request->validate(TaskMessage::rules());

        $data = $request->all();
        $data['user_id'] = Auth::id();
        $data['reminder_at'] = $data['reminder_at'].':00';
        $store = TaskMessage::create($data);
        if ($store) {
            $request->session()->flash('success', $this->success_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route('tasks.show', $data['task_id']));
    }

    public function tasksList(Request $request, $id){
        try {
            $this->_data['params'] = $request->all();
            $this->_data['limit'] = $this->limit = $request->get('perpage') ? $request->get('perpage') : $this->limit;
            $this->_data['search'] = $request;

            $filter_data = $request->all();
            $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;
            $query = ProjectTask::where('project_id', $id);
                
            // add filter in records
            searching_string($query, $request->form);

            // sort records
            db_sort($query, $request);
            $data = $query->paginate($limit);

            $this->_data['data'] = $data;
            
            $this->_data['project_id'] = $id;
            return view('front.tasks.index', $this->_data); 
        } catch(\Illuminate\Database\QueryException $ex){
            return redirect()->back()->with(['message' => $ex->getMessage(), 'alert-class' => 'alert-danger']);
        }
    }

    /**
     * show create form
     *
     * @method create
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request, $project_id){
        $this->_data['project_id'] = $project_id;
        $this->_data["users"] = User::whereIn('role_id', [User::POST_SALE])
                                    ->where('status', User::Active)
                                    ->pluck('name', 'id');
        return view('front.tasks.add', $this->_data);
    }

    /**
     * show create form
     *
     * @method create
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request){
        $this->_data["breadcrumb"]["Create"] = "javascript:void(0)";
        $this->_data["users"] = User::whereIn('role_id', [User::PRE_SALE, User::POST_SALE, User::MANAGER])
                                    ->where('status', User::Active)
                                    ->pluck('name', 'id');
        return view('front.tasks.create', $this->_data);
    }

    /**
     * store new records
     *
     * @method store
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request){
        $request->validate(ProjectTask::rules());
        $data = $request->all();
        if ($request->hasFile("file")) {
            $files = $request->file("file");
            foreach ($files as $file) {
                $v[] = _upload($file, "tasks");
            }
            $data["file"] = implode(",", $v);
        }
        $data['assigned_by'] = \Auth::user()->id;
        $store = ProjectTask::create($data);
        if ($store) {
            $request->session()->flash('success', $this->success_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route('tasks.list', $data['project_id']));
    }

    /**
     * show edit form
     *
     * @method edit
     * @param ProjectTask $projectTask
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function edit(Request $request, ProjectTask $task){
        $this->_data["breadcrumb"]["Update"] =  "javascript:void(0)";
        $this->_data["data"] = $task;
        $this->_data['project_id'] = $task->project_id;
        $this->_data["users"] = User::whereIn('role_id', [User::POST_SALE])
                                    ->where('status', User::Active)
                                    ->pluck('name', 'id');
        return view('front.tasks.edit', $this->_data);
    }

    /**
     * update information
     *
     * @method update
     * @param Request $request
     * @param ProjectTask $projectTask
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, ProjectTask $task){
        $amount_received = ($request->has('amount_received')) ? 1 : 0 ;
        $client_approved = ($request->has('client_approved')) ? 1 : 0 ;
        $admin_approved = ($request->has('admin_approved')) ? 1 : 0 ;
        $quote_sent = ($request->has('quote_sent')) ? 1 : 0 ;
        $invoice_sent = ($request->has('invoice_sent')) ? 1 : 0 ;

        $data = $request->except("_token");
        if ($request->hasFile("file")) {
            $files = $request->file("file");
            foreach ($files as $file) {
                $f[] = _upload($file, "tasks");
            }
            $data["file"] = implode(",", $f);
        }
        $data["amount_received"] = $amount_received;
        $data["client_approved"] = $client_approved;
        $data["admin_approved"] = $admin_approved;
        $data["quote_sent"] = $quote_sent;
        $data["invoice_sent"] = $invoice_sent;
        $data['assigned_by'] = \Auth::user()->id;
        $udpate = $task->update($data);
        if ($udpate) {
            $request->session()->flash('success', $this->update_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route('tasks.list', $task->project_id));
    }

    /**
     * delete information
     *
     * @method delete
     * @param Request $request
     * @param Enquiry $milestone
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, ProjectTask $projectTask){
        $projectTask->delete();
        $request->session()->flash('success', $this->record_deleted);
        return redirect()->to(route('projects.index', $projectTask->project_id));
    }

    /**
     * Release request form
     *
     * @method release
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
    */
    public function updateReleaseRequest(Request $request, $id){
        
        $task = ProjectTask::find($id);
        $task->release_request = 1;
        $task->requested_by = \Auth::user()->id;
        $task->save();

        $curProjectTask = ProjectTask::find($id);
        event(new TaskReleaseRequest($curProjectTask));

        return redirect()->to(route('tasks.list', $task->project_id));
    }
}