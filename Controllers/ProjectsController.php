<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\Enquiry;
use App\Model\Country;
use App\Model\Technology;
use App\Model\Project;
use App\Model\EnquirySuccess;
use App\Model\ProjectMessage;
use App\Model\ProjectMilestone;
use App\Model\Configuration;
use App\Model\EmailThread;
use App\User;
use App\Events\MailSendToProjectOwner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class ProjectsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        /**
         * @desc set controller name
         */
        $this->controller = "Projects";

        /**
         * @desc set page title
         */
        $this->title = "Post-Sales";

        $this->model = Project::class;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $this->_data['params'] = $request->all();
        $this->_data['limit'] = $this->limit = $request->get('perpage') ? $request->get('perpage') : $this->limit;
        $this->_data['search'] = $request;

        $filter_data = $request->all();
        $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;
        $status = (!empty($request->get('status')))?$request->get('status'):'';
        $date = (!empty($request->get('date')))?$request->get('date'):'';
        $curDate = Carbon::now();

        $query = Project::with(["enquiry", "postAssignedTo", "postAssignedBy", "createdBy"])->where(function($query) use($status, $date, $curDate){
            if(isset($status) && !empty($status)){
                $query->where('status', ucfirst(str_replace('_',' ',$status)));
            }
            if(isset($date) && !empty($date)){
                $query->whereDate('created_at', $curDate);
            }
            if (custom_permit('projects')) {
                $query->where('post_assigned_to', Auth::user()->role_id);
            }
        });

        // add filter in records
        searching_string($query, $request->form);

        // sort records
        db_sort($query, $request);
        $data = $query->paginate($limit);

        $this->_data['data'] = $data;

        return view('front.projects.index', $this->_data);
    }

    /**
     * show detail form
     *
     * @method detail
     * @param Project $project
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, $id) 
    {
        try {
            $queryFindProject = Project::where('id', $id);
            if (custom_permit('projects')) {
                $queryFindProject->where('post_assigned_to', Auth::user()->role_id);
            }

            $projectDetail = $queryFindProject->with(["postAssignedTo", "postAssignedBy", "preAssignedBy"])->first();
            if (!$projectDetail) {
                $request->session()->flash('error', 'Sorry, You are not authorized for this.');
                return redirect()->to(route('projects.index'));
            }

            $technologies = explode(',', $projectDetail->technology);
            $getTechnologiesSeprate = Technology::whereIn('id',$technologies)->pluck('title')->toArray();
            $projectDetail['technology'] = implode(', ',$getTechnologiesSeprate);
            
            $this->_data["project_messages"] =  ProjectMessage::where('project_id', $id)->with(["author"])->get();
            $getPreAssignedUsers  =  User::whereIn('id', explode(',',$projectDetail['pre_assigned_to']))
                                        ->where('status', User::Active)
                                        ->pluck('name')->toArray();
            $projectDetail["preAssignedTo"]  =  implode(', ',$getPreAssignedUsers);

            $getPostAssignedUsers  =  User::whereIn('id', explode(',',$projectDetail['post_assigned_to']))
                                        ->where('status', User::Active)
                                        ->pluck('name')->toArray();
            
            $projectDetail["postAssignedTo"]  =  implode(', ',$getPostAssignedUsers);
            $this->_data["data"] =  $projectDetail;
            $this->_data["website_url"]      =  Configuration::where('key', 'website_url')->first();

            $emailThreadList = array();
            $v = 0;
            $emailThreadsParents   =  EmailThread::where('project_id', $id)
                                        ->where('parent_id', NULL)
                                        ->with('getUserDetail','getEnquiryDetail')
                                        ->get();
            foreach($emailThreadsParents as $key => $emailThreadsParent){
                $emailThreadList[$v] = $emailThreadsParent;
                $v++;
                $emailThreadsChilds   =  EmailThread::where('parent_id', $emailThreadsParent->id)
                                        ->with('getUserDetail','getEnquiryDetail')
                                        ->orderBy('id', 'ASC')
                                        ->get();
                    foreach($emailThreadsChilds as $emailThreadsChild){      
                        $emailThreadList[$v] = $emailThreadsChild;
                        $v++;
                    }                       
            }                                    
            $this->_data["email_threads"] = $emailThreadList;
            
            return view('front.projects.show', $this->_data); 
        } catch(\Illuminate\Database\QueryException $ex){
            return redirect()->back()->with(['message' => $ex->getMessage(), 'alert-class' => 'alert-danger']);
        }
    }

    /**
     * show create form
     *
     * @method create
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request) 
    {
        $this->_data["breadcrumb"]["Create"] =  "javascript:void(0)";
        $this->_data["countries"] = Country::where('status', 1)->pluck('name', 'id');
        $this->_data["users"] = User::whereIn('role_id', [User::PRE_SALE, User::POST_SALE, User::MANAGER])->where('status', User::Active)->pluck('name', 'id');
        return view('front.projects.create', $this->_data);
    }

    /**
     * store new records
     *
     * @method store
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) 
    {
        $request->validate(Project::rules());
        $data = $request->all();
        if ($request->hasFile("file")) {
            $files = $request->file("file");
            foreach ($files as $file){
                $f[] = _upload($file, "projects");
            }
            $data["file"] = implode(",", $f);
        }
        
        $data["service_id"] = implode(",", $data['service_id']);
        $store = Project::create($data);
        if ($store) {
            $request->session()->flash('success', $this->success_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route('projects.index'));
    }

    /**
     * show edit form
     *
     * @method edit
     * @param Project $project
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request, Project $project) 
    {
        if ($project->post_assigned_to === Auth::user()->id || Auth::user()->role_id === 1) {
            $this->_data["breadcrumb"]["Update"] =  "javascript:void(0)";
            $project["service_id"] = explode(",", $project['service_id']);
            $project["technology"] = explode(",", $project['technology']);
            $this->_data["data"] = $project;
            $this->_data["countries"] = Country::where('status', 1)->pluck('name', 'name');
            $this->_data["technologies"]     =  Technology::where('status', Technology::Active)->pluck('title','id');
            $this->_data["users"] = User::whereIn('role_id', [User::PRE_SALE, User::POST_SALE, User::MANAGER])->pluck('name','id');
            $this->_data["website_url"]      =  Configuration::where('key', 'website_url')->first();
            return view('front.projects.edit', $this->_data);
        } else {
            $request->session()->flash('error', 'Sorry, You are not authorized for this.');
            return redirect()->to(route('home'));
        }
    }

    /**
     * update information
     *
     * @method update
     * @param Request $request
     * @param Project $project
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Project $project) 
    {
        $request->validate(Project::rules($project->id));
        $data = $request->except("_token");
        if ($request->hasFile("file")) {
            $files = $request->file("file");
            foreach ($files as $file){
                $f[] = _upload($file, "projects");
            }
            $data["file"] = implode(",", $f);
        }
        $data['service_id'] = implode(",", $data['service_id']);
        $data['technology'] = implode(",", $data['technology']);
        if(isset($data['post_assigned_to']) && !empty($data['post_assigned_to'])){
            $data["post_assigned_to"] = implode(",", $data['post_assigned_to']);
        }
        $data['post_assigned_by'] = \Auth::user()->id;
        $data['add_from'] = 0;
        $udpate = $project->update($data);
        if ($udpate) {
            $request->session()->flash('success', $this->update_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route('projects.index'));
    }

    /**
     * store new message
     *
     * @method message
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function message(Request $request) 
    {
        $request->validate(ProjectMessage::rules());
        $data = $request->all();
        $data['user_id'] = Auth::id();
        if ($request->hasFile("file")) {
            $files = $request->file("file");
            foreach ($files as $file){
                $f[] = _upload($file, "projects");
            }
            $data["file"] = implode(",", $f);
        }
        $data['reminder_at'] = $data['reminder_at'].':00';
        $store = ProjectMessage::create($data);
        if ($store) {
            $request->session()->flash('success', $this->success_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route('projects.show', $data['project_id']));
    }

    /**
     * update project status
     *
     * @method projectChangeStatus
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeStatus(Request $request, $id)
    {
        try {
            $project = Project::find($id);
            $project->status = $request->get('status');
            $project->save();
        } catch (\Exception $e) {
            return throughGetMessage($e);
        }
        $request->session()->flash('success', $this->update_response);
        return redirect()->to(route('projects.index'));
    }

    /**
     * approved information
     *
     * @method approve
     * @param Request $request
     * @param Project $project
     * @return \Illuminate\Http\RedirectResponse
     */
    public function approve(Request $request, Project $project) 
    {
        $data = $request->all();
        $project['enquiry_id'] = $data['id'];
        $project['created_by'] = \Auth::user()->id;
        $enquiryDetail = Project::where('id', $data['id'])->first();
        if ((isset($enquiryDetail) && !empty($enquiryDetail)) && $enquiryDetail['status'] === 'Success') {
            Project::create($project);
        }
        $udpate = Project::where('id', $data['id'])->update(['approved_by' => Auth::user()->id]);
        if($udpate) {
            $request->session()->flash('success', $this->update_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route('projects.index'));
    }

    public function mailSend(Request $request, $id){
        $this->_data["breadcrumb"]["Create"] =  "javascript:void(0)";
        $this->_data["id"] =  $id;
        return view('front.projects.mail', $this->_data);
     }

     public function mailSubmit(Request $request){
        $request->validate(EmailThread::rules());
        $data = $request->all();
        
        $data['user_id'] = \Auth::user()->id;
        $data['status'] = EmailThread::sent;
        if ($request->hasFile("attachments")) {
            $files = $request->file("attachments");
            foreach ($files as $file) {
                $f[] = _upload($file, "enquiries");
            }
            $data["attachments"] = implode(",", $f);
        }
        
        $store = EmailThread::create($data);
        $project = Project::find($data['project_id']);
        event(new MailSendToProjectOwner($project, $data['subject'], $data['message']));
        if ($store) {
            $request->session()->flash('success', $this->mail_send);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route('projects.index'));
     }

     public function customerMailSubmit(Request $request){
         
        $validator = Validator::make(
            $request->all(), EmailThread::rules()
        );
        if ($validator->passes()) {
            try {
                $data = $request->all();
                $emailThread = EmailThread::find($data['table_id']);
                
                if ($request->hasFile("attachments")) {
                    $files = $request->file("attachments");
                    foreach ($files as $file) {
                        $f[] = _upload($file, "enquiries");
                    }
                    $data["attachments"] = implode(",", $f);
                }
                $data["parent_id"]      =   $data['table_id'];
                $data["project_id"]     =   $emailThread->project_id;
                $data['status']         =   EmailThread::received;
                EmailThread::create($data);

            } catch (\Exception $e) {
                return throughGetMessage($e);
            }
            return response()->json(['success' => 'Customer reply has been updated successfully.']);
        } else {
            return response()->json(['error' => $validator->errors()->all()]);
        }
     }

}
