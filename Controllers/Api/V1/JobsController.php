<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Model\Job;
use App\Model\UserAppliedOnJob;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class JobsController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->successStatus = apistatus('success');
        $this->errorStatus = apistatus('internalservererror');
        $this->unauthorizedStatus = apistatus('unauthorized');   
    }

    /**
     * @method GET/POST
     * @desc get Jobs List
     * @return course data array
     */
    public function jobList(Request $request){
        try {
            $job_list = Job::whereStatus(Job::Active)
                                ->where(function($query) {
                                    if(request()->isMethod('POST')){
                                        $query->whereUserId(Auth::user()->id);
                                    }
                                })
                                ->has('userDetail')
                                ->with('userDetail')
                                ->orderBy('id', 'Desc')
                                ->get();
            if(request()->isMethod('GET')){
                foreach($job_list as $key => $job_apply){
                    $job_list[$key]->is_applied = UserAppliedOnJob::whereJobId($job_apply->id)
                                                    ->whereUserId(Auth::user()->id)
                                                    ->whereStatus(UserAppliedOnJob::Active)
                                                    ->count();
                }
            } else {
                $job_list->load('appliedJobUserDetail');
            }
            $success['success'] =  ___('job_list_fetched');                         
            $success['data']['jobs']   = $job_list;                                   
            return response()->json($success, $this->successStatus);
        } catch(\Exception $e) {
            $error['error'] = $e->getMessage();
                return response()->json($error, $this->errorStatus);
        }
    }

    /**
     * @method POST
     * @desc create job
     * @return success array
    */
    public function createJob(Request $request){
        $request->validate(Job::createRules());
        try {
            $request->request->add([
                'user_id' => Auth::user()->id,
                'status' => Job::Active
            ]);
            Job::create($request->all());

            $success['success'] =  ___('job_created');                      
            return response()->json($success, $this->successStatus);

        } catch(\Exception $e) {
            $error['error'] = $e->getMessage();
            return response()->json($error, $this->errorStatus);
        }
    }

    /**
     * @method POST
     * @desc apply job
     * @return success array
    */
    public function applyJob(Request $request){
        $request->validate(UserAppliedOnJob::rules());
        try {
            $data = $request->all();
            if ($request->hasFile("resume")) {
                $file = $request->file("resume");
                $f = _upload($file, "resume");
                $data["uploaed_resume"] = $f;
            }
            $data['user_id']        =     Auth::user()->id;
            $data['is_confirmed']   =     UserAppliedOnJob::isConfirmedNo;
            $data['status']         =     UserAppliedOnJob::Active;
            $data['confirmed_by']   =     0;

            UserAppliedOnJob::create($data);

            $success['success'] =  ___('job_applied');
            return response()->json($success, $this->successStatus);
        } catch(\Exception $e){
            $error['error'] = $e->getMessage();
            return response()->json($error, $this->errorStatus);
        }
    }

}
