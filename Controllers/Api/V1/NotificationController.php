<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\User;
use App\Model\Course;
use App\Model\UserCourse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Helpers\NotificationHelper;

class NotificationController extends Controller{   
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->successStatus = apistatus('success');
        $this->errorStatus = apistatus('internalservererror');
        $this->unauthorizedStatus = apistatus('unauthorized');   
    }
    /**
     * @method listing
     * @desc prepare data to send view
     * @param Request $request
     * @return array
     */
    public function checkPushNotification(Request $request) {
        
        $input = $request->all();        

        if (isset($input['title']) && $input['title']!='') {
            $title = $input['title'];
        }else{
            $title = 'Course';
        }

        if (isset($input['message']) && $input['message']!='') {
            $message = $input['message'];
        }else{
            $message = 'Course Created';
        }

        if (isset($input['device_id']) && $input['device_id']!='') {
            $device_id = $input['device_id'];
        }else{
            $device_id = 'eJ520EjVQjinN-qM6Ujzzb:APA91bEBZjF9hbHG-6-mBita5h1rPSsj4VkdHKyz5Sd_OGlr_R3aSrClTCHZGVU1XFQFalNHy9bNXphA45qwoyl6-emU16TilDmIhj8DH3MHT4fiRHWbVQc9qtnqEluQUR-dsjfNs71R';
        }        

        $response = NotificationHelper::sendNotification($message, $device_id, $title);

        $success['success']  =  'Push Notification.';
        $success['data']     =  json_decode($response);

        return response()->json($success, $this->successStatus);
       
    }

}
