<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Model\Document;
use App\Model\UserDocumentActivity;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class DocumentController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->successStatus = apistatus('success');
        $this->errorStatus = apistatus('internalservererror');
        $this->unauthorizedStatus = apistatus('unauthorized');   
    }

    /**
     * @method GET/POST
     * @desc get Document List
     * @return course data array
     */
    public function index(Request $request){
        try {
            $search = $request->get('search');
            $document_list = Document::whereHas('documentActivityDetail', function($query){
                                    $query->whereNotNull('published_by')
                                    ->whereNotNull('published_at');
                                })
                                ->where(function($query) use ($search){
                                    if(!empty($search)){
                                        $query->where('title', 'Like', "%$search%");
                                    }
                                })
                                ->whereStatus(Document::Active)
                                ->with('userDetail')
                                ->orderBy('id', 'Desc')
                                ->get();
            foreach($document_list as $key => $document){       
                $document_list[$key]->links = asset('/images/documents/'.$document->links);
                $document_list[$key]->cover_image = asset('/images/documents/cover_image/'.$document->cover_image);  
            }                    
            $success['success'] =  ___('document_list_fetched');                         
            $success['data']['documents']   = $document_list;                                   
            return response()->json($success, $this->successStatus);
        } catch(\Exception $e) {
            $error['error'] = $e->getMessage();
                return response()->json($error, $this->errorStatus);
        }
    }

    public function documentRead(Request $request){
        $rules = UserDocumentActivity::rules();
        $validator = \Validator::make($request->all(), $rules);
        if($validator->fails()) {
            return response()->json(["status" => false, "error" => $validator->errors()->first()]);
        }
        try {
            $data['document_id']    =   $request->get('document_id');
            $data['user_id']        =   Auth::user()->id;

            UserDocumentActivity::updateOrCreate($data);
            
            $success['success'] =  'Document read status has been updated successfully.';     
            return response()->json($success, $this->successStatus);
        } catch(\Exception $e) {
            $error['error'] = $e->getMessage();
            return response()->json($error, $this->errorStatus);
        }  
    }



}
