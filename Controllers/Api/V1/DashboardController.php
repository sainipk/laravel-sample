<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\User;
use App\Model\Announcement;
use App\Model\Job;
use App\Model\UserCourse;
use App\Model\Page;
use App\Model\Course;
use App\Model\Category;
use App\Model\Setting;
use App\Model\AnnouncementSubscriber;
use App\Model\Configuration;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class DashboardController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        
        $this->successStatus = apistatus('success');
        $this->errorStatus = apistatus('internalservererror');
        $this->unauthorizedStatus = apistatus('unauthorized');   
    }



    /**
     * @method dashboard
     * @desc user dashboard
     * @return course data array, accouncement data array
     */
    public function dashboard(Request $request){
       
        $data = $request->all();
        $user_type_id = User::getUserTypeId($data['id']);
        if ($user_type_id == 7) {
            $business_courses = $this->getBusinessCourses();
        }
        
        $indivi_courses = $this->getIndividualCourses();
      
        $success['success'] =  ___('data_set');
        $success['pages']['about'] = url("/about_us");
        $success['pages']['contact'] = url("/contact_us");
        $success['pages']['privecy'] = url("/privacy_policy");
        $success['pages']['terms_conditions'] = url("/terms_conditions");

        $categories = Category::getActiveCategories();
        array_unshift($categories ,array('key' => 0, 'title' => 'All Courses'));
  
        if ($user_type_id == 7) {
            $courses = $this->manageBusinessCourses($categories, $business_courses, $data['id']);
            $courses = $this->manageIndividualCourses($courses, $indivi_courses, $data['id']);
        } else {
            $courses = $this->manageIndividualCourses($categories, $indivi_courses, $data['id']);
        }
        $announcement_data = Announcement::getAllAnnouncement($user_type_id);
        $announcement_data = $this->manageAnnouncementData($announcement_data, $data['id']);
        foreach($announcement_data as $key => $announcement){
			$announcement_data[$key]['event_date_time'] = date('dS, M Y - h:i A', strtotime($announcement->event_date_time));
		}
        $success['data']['announcements'] =  $announcement_data;
        $success['data']['categories']    =  $courses;        
        $success['data']['video_uploaded']=  $announcement_data;
        $success['data']['my_courses']    =  UserCourse::getUserCourses($data['id']);
        return response()->json($success, $this->successStatus);
    }

    /**
     * @method manageBusinessCourses
     * @desc manage business course data in category array
     * @return category with course data array
     */
    public function manageBusinessCourses($categories, $courses, $user_id){

        foreach ($categories as $key => $value) {
            $sme_course = array();
            if ($value['key'] == 0) {
                $i = 0;
                foreach ($courses as $key_1 => $value_1) {
                    foreach ($value_1['courses'] as $key_2 => $value_2) {
                         $value_2['is_purchased'] = $this->checkCourseIsPurchased($value_2['id'], $user_id);
                        if ($value_2['course_type'] == 2) {
                            $value_2['short_description'] = str_replace('<div>', '',  $value_2['short_description']);
                            $value_2['course_duration'] = get_days($value_2['course_duration']);
                            $sme_course[$i] = $value_2;
                        }
                        $i++;
                    }
                }
            } else {
                $i = 0;
                foreach ($courses as $key_1 => $value_1) {
                    if (count($value_1['courses']) > 0) {
                        foreach ($value_1['courses'] as $key_2 => $value_2) {
                            $value_2['is_purchased'] = $this->checkCourseIsPurchased($value_2['id'], $user_id);
                            if ($value['key'] == $value_2['category_id']) {
                                if ($value_2['course_type'] == 2) {
                                    $value_2['short_description'] = str_replace('<div>', '',  $value_2['short_description']);
                                    $value_2['course_duration'] = get_days($value_2['course_duration']);
                                    $sme_course[$i] = $value_2;
                                }
                                $i++;
                            }
                            
                        }
                    }                    
                }
            }
            if(isset($sme_course)){
                foreach($sme_course as $key => $course){
                    $sme_course[$key]['published_at'] = date('dS, M Y - h:i A', strtotime($course['published_at']));
                }
            }
            $categories[$key]['business_course'] = $sme_course;
        }
        return $categories;
    }
    
    /**
     * @method manageIndividualCourses
     * @desc manage course data in category
     * @return category with course data array
     */
    public function manageIndividualCourses($categories, $courses,$user_id){
        foreach ($categories as $key => $value) {
            $user_course = array();
            if ($value['key'] == 0) {
                $i = 0;
                foreach ($courses as $key_1 => $value_1) {
                    foreach ($value_1['courses'] as $key_2 => $value_2) {
                        $value_2['is_purchased'] = $this->checkCourseIsPurchased($value_2['id'], $user_id);
                        if ($value_2['course_type'] == 1) {
                            $value_2['short_description'] = str_replace('<div>', '',  $value_2['short_description']);
                            $value_2['course_duration'] = get_days($value_2['course_duration']);
                            $user_course[$i] = $value_2;
                        }
                        $i++;
                    }
                }
            } else {
                $i = 0;
                foreach ($courses as $key_1 => $value_1) {

                    if (count($value_1['courses']) > 0) {
                        foreach ($value_1['courses'] as $key_2 => $value_2) {
                            $value_2['is_purchased'] = $this->checkCourseIsPurchased($value_2['id'], $user_id);
                            if ($value['key'] == $value_2['category_id']) {
                                if ($value_2['course_type'] == 1) {
                                    $value_2['short_description'] = str_replace('<div>', '',  $value_2['short_description']);
                                    $value_2['course_duration'] = get_days($value_2['course_duration']);
                                    $user_course[$i] = $value_2;
                                }
                                $i++;
                            }
                            
                        }
                    }                    
                }
            }
            $categories[$key]['user_course'] = $user_course;
        }
        return $categories;
    }

    /**
     * @method getBusinessCourses
     * @desc get all sme courses
     * @return data array
     */
    public function getBusinessCourses(){
        $business_courses = Category::all()->each(function($business_courses) {

            $business_courses->courses = $business_courses->courses()
                                        ->addSelect(['courses.id', 'courses.title', 'courses.category_id', 'courses.course_version_id', 'courses.version_no', 'courses.slug', 'courses.short_description', 'courses.price', 'courses.course_type', 'courses.status', 'courses.course_duration', 'course_activity_log.published_by', 'course_activity_log.published_at'])
                                        ->whereNotNull('course_activity_log.published_by')
                                        ->whereNotNull('course_activity_log.published_at')
                                        ->where('courses.course_type','2')
                                        ->where('courses.status','1')
                                        ->latest('courses.created_at')->take(10)->get()->toArray();
            
            
        });

        $business_courses = $business_courses->toArray();

        return  $business_courses;
    }

    /**
     * @method getIndividualCourses
     * @desc get individual course data
     * @return course data array
     */
    public function getIndividualCourses(){
        $user_courses = Category::all()->each(function($user_courses) {

            $user_courses->courses = $user_courses->courses()
                                        ->addSelect(['courses.id', 'courses.title', 'courses.category_id', 'courses.course_version_id', 'courses.version_no', 'courses.slug', 'courses.short_description', 'courses.price', 'courses.course_type', 'courses.status', 'courses.course_duration', 'course_activity_log.published_by', 'course_activity_log.published_at'])
                                        ->whereNotNull('course_activity_log.published_by')
                                        ->whereNotNull('course_activity_log.published_at')
                                        ->where('courses.course_type','1')
                                        ->where('courses.status','1')
                                        ->latest('courses.created_at')->take(10)->get()->toArray();
            
            
        });

        $user_courses = $user_courses->toArray();
        if(isset($user_courses)){
            foreach($user_courses as $key => $user_course){
                foreach($user_course['courses'] as $keyy => $courses){
                    $user_courses[$key]['courses'][$keyy]['published_at'] = date('dS, M Y - h:i A', strtotime($courses['published_at']));
                }
            }
        }

        return  $user_courses;
    }
    /**
     * @method manageAnnouncementData
     * @desc remove tags from descriptions
     * @return course data array
     */
    public function manageAnnouncementData($data, $user_id){
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $value->description = str_replace('<div>', '', $value->description);
                $value->terms_and_condition = str_replace('<div>', '', $value->terms_and_condition);
                $value->is_subscribed = AnnouncementSubscriber::checkAlreadySubscribedForAnnouncement(['user_id'=>$user_id,'announcement_id'=>$value->id]);
            }
        }
        return $data;
    }
    
    public function checkCourseIsPurchased($course_id, $user_id){
        
        $data = UserCourse::where('course_id',$course_id)
        ->where('user_id',$user_id)
        ->where('status','1')
        ->get();

        if ($data->count()>0) {
            return true;
        }else{
            return false;
        }
    }

    public function settingsList(Request $request){
        try {
            $settings_list = Configuration::get()->toArray();
            if(isset($settings_list)){
                foreach($settings_list as $key => $settings){
                    $settings_list[$key]['created_at'] = date('dS, M Y - h:i A', strtotime($settings['created_at']));
                    
                }
            }                   
            $success['success'] =  'Settings List fetched successfully.';                          
            $success['data']['settingsList']   = $settings_list;                                   
            return response()->json($success, $this->successStatus);
        } catch(\Exception $e) {
            $error['error'] = $e->getMessage();
                return response()->json($error, $this->errorStatus);
        }
    }


}
