<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\V1\QuizController;
use App\User;
use App\Model\Course;
use App\Model\UserCourse;
use App\Model\UserCertificate;
use App\Model\UserCoupon;
use App\Model\UserExam;
use App\Model\TestExamSetting;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Helpers\NotificationHelper;
use App\Model\UserNotification;

class CoursesController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $getQuizResult;

    public function __construct(QuizController $getQuizResult) {
        $this->getQuizResult = $getQuizResult;
        $this->successStatus = apistatus('success');
        $this->errorStatus = apistatus('internalservererror');
        $this->unauthorizedStatus = apistatus('unauthorized');   
    }

    /**
     * @method getCourseDetail
     * @desc get Course Detail
     * @return course data array
     */
    public function getCourseDetail(Course $course, Request $request){
        
        $success['success']                 =  ___('successGet_course_detail');
        $success['data']['course_detail']   =  $course;
        foreach($course->courseChapters as $key => $chapters){
            foreach($chapters->chapterDocuments as $docKey => $documents){
                if(strtolower($documents->document_type) == 'pdf'){
                    $course->courseChapters[$key]->chapterDocuments[$docKey]->link = asset('images/chapters/pdf/'.$documents->link);
                    $course->courseChapters[$key]->chapterDocuments[$docKey]->is_read = check_document_read_status($documents->id);
                } elseif(strtolower($documents->document_type) == 'video'){
                    $course->courseChapters[$key]->chapterDocuments[$docKey]->link = asset('images/chapters/video/'.$documents->link);
                    $course->courseChapters[$key]->chapterDocuments[$docKey]->is_read = check_document_read_status($documents->id);
                } 
            }
            $eligible_for_final = false;
            $get_result_id = $this->getTestExamSetting($course->id, $chapters->id, 'chapter');
            if(isset($get_result_id) && !empty($get_result_id)){
                $eligible_for_final = true;
                $request->request->add([
                    'result_id' => $get_result_id->result_id
                ]);
                $result_data = json_decode(json_encode($this->getQuizResult->quizResult($request)));
                $course->courseChapters[$key]->score = $result_data->original->result_data->score;

                $course->courseChapters[$key]->chapter_status = 'Completed';
            } elseif($course->courseChapters[$key]->chapterDocuments[0]->is_read > 0 && $course->courseChapters[$key]->chapterDocuments[1]->is_read > 0 ){
                $eligible_for_final = false;
                $course->courseChapters[$key]->chapter_status = 'In-Progress';
            } else {
                $eligible_for_final = false;
                $course->courseChapters[$key]->chapter_status = 'Not Started';
            }
        }                           
        $success['data']['course_detail']['eligible_for_final'] = $eligible_for_final;
        $success['data']['course_detail']['course_chapters']    = $course->courseChapters;
        $success['data']['course_detail']['certificate']        =  UserCertificate::where('user_id', Auth::user()->id)
                                                                    ->where('course_id', $course->id)
                                                                    ->orderBy('id', 'Desc')
                                                                    ->first();
        return response()->json($success, $this->successStatus);
    }

    private function getTestExamSetting($course_id, $chapter_id, $test_type){
        $test_exam_setting = TestExamSetting::where('course_id', $course_id)
                                ->where('chapter_id', $chapter_id)
                                ->where('test_type', $test_type)
                                ->where('status', TestExamSetting::Active)
                                ->first();
        if(isset($test_exam_setting) && !empty($test_exam_setting)){
            return UserExam::where('course_id', $course_id)
                    ->where('chapter_id', $chapter_id)
                    ->where('user_id', Auth::user()->id)
                    ->where('test_exam_id', $test_exam_setting->id)
                    ->orderBy('id', 'Desc')
                    ->first();
        }  
    }

    /**
     * @method POST
     * @desc purchase course
     * @return course data array
     */
    public function purchaseCourse(Course $course){
        try {
            $data = [
                'user_id' => Auth::user()->id,
                'course_id' => $course->id,
                'course_issued_at' => date('Y-m-d H:i:s'),
                'course_expire_at' => date("Y-m-d H:i:s", strtotime(date('Y-m-d H:i:s'))+(60*$course->course_duration)),
                'total_chapters' => $course->courseChapters->count(),
                'completed_chapters' => 0,
                'is_course' => UserCourse::isNew,
                'status' => UserCourse::Active,
            ];
            UserCourse::create($data);

            $success['success'] =  ___('course_purchased');
            return response()->json($success, $this->successStatus);
        } catch(\Exception $e) {
            $error['error'] = $e->getMessage();
             return response()->json($error, $this->errorStatus);
        }
    }

    /**
     * @method GET
     * @desc My Courses
     * @return array
    */

    public function purchaseCoursesList(Request $request){
        try {
            $success['success'] =   'My courses fetched successfully.';
            $datas = UserCourse::authId()->with('courseDetail')->get();
            if(isset($datas)){
                foreach($datas as $key => $data){
                    $get_parent_data = UserCoupon::with('getSmeCoupons')
                                            ->courseId($data['course_id'])
                                            ->userCourseId($data['id'])
                                            ->usedBy()
                                            ->status()
                                            ->first();
                    $datas[$key]['course_issued_at'] = date('dS, M Y - h:i A', strtotime($data['course_issued_at']));
                    $datas[$key]['no_of_coupons'] = $get_parent_data;
                    $datas[$key]['courseDetail']->course_duration = get_days($data['courseDetail']['course_duration']);
                }
            }
            $success['data']    =   $datas;
            return response()->json($success, $this->successStatus);
        } catch(\Exception $e) {
            $error['error'] = $e->getMessage();
                return response()->json($error, $this->errorStatus);
        }
    }
    /**
     * @method POST
     * @desc exam started
     * @return success array
    */
    public function renewCourse(Request $request){
        
        $data = $request->all();

        $title = 'Course renew';
        $send_param = array('id'=>1,'title'=>$title,'module_type'=>'renew_course','user_id'=>$data['user_id']);

        $this->sendCoursePublishNotification($send_param);

        $success['success'] =  'Course renew.';

        return response()->json($success, $this->successStatus);
        
    }
    /**
     * send push notification to user when course published
     *
     * @method sendCoursePublishNotification
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendCoursePublishNotification($params){

        $user_data = User::where('status','1')->where('id',$params['user_id'])->get()->first();
        
        $send_status = 2;

        $message = $params['title'];
        $title = $params['title'];
        
        $device_id = $user_data->device_id;            

        $response = NotificationHelper::sendNotification($message, $device_id, $title);
        $result = json_decode($response);
       
        if (is_object($result) && property_exists($result, 'success') && $result->success) {
            $send_status = 1;
        }
        
        $data = array(
            'module_id'=>$params['id'],
            'user_id'=>$user_data->id,
            'icon'=>Null,
            'module_type'=>$params['module_type'],
            'message'=>$message,
            'is_send'=>$send_status
            );
        
        UserNotification::create($data);         
        
    }
    



}
