<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\User;
use App\Model\AnnouncementSubscriber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class UsersController extends Controller {

    public $successStatus       =   200;
    public $errorStatus         =   500;
    public $unauthorizedStatus  =   401;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        
        $this->successStatus = apistatus('success');
        $this->errorStatus = apistatus('internalservererror');
        $this->unauthorizedStatus = apistatus('unauthorized');   
    }
    /**
    * User detail api
    *
    * @return \Illuminate\Http\Response
    */
    public function userDetails(Request $request) {
        
        try {
            $fields = ['parent_id', 'otp', 'device_token'];
            $user = Auth::user();
            $user = collect($user->toArray())->except($fields);

            $data['success'] = ___('successGet_User_Detail');
            $data['data'] = $user;            
            return response()->json($data, $this->successStatus);
        } catch (\Exception $e) {
             $error['error'] = $e->getMessage();
             return response()->json($error, $this->errorStatus);
        }
    }
    /**
    * @method announcementUserSubscription
    * @desc user apply for announcement subscription
    *
    * @return \Illuminate\Http\Response
    */
    public function announcementUserSubscription(Request $request) {
        
         $data = $request->all();
         $status = AnnouncementSubscriber::checkAlreadySubscribedForAnnouncement($data);

         if (!$status ) {
             $store = AnnouncementSubscriber::create(['user_id'=>$data['user_id'], 'announcement_id'=>$data['announcement_id'], 'is_agree'=>$data['is_agree'], 'status'=>2]);
         
             if ($store->id) {
                $success['success'] = ___('announcement_User_Subscription');
                return response()->json($success, $this->successStatus);
             }else{
                $success['error'] = 'Subscription could not be added.';
                return response()->json($success, $this->successStatus);
             }
         }else{
            $success['error'] = 'You have been already subscribed for this announcement.';
            return response()->json($success, $this->successStatus);
         }
         
    }
}
