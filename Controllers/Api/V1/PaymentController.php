<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\User;
use App\Model\Course;
use App\Model\UserCourse;
use App\Model\UserCoupon;
use App\Model\Payment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Helpers\NotificationHelper;
use App\Model\UserNotification;
use DB;

class PaymentController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->successStatus = apistatus('success');
        $this->errorStatus = apistatus('internalservererror');
        $this->unauthorizedStatus = apistatus('unauthorized');   
    }
    /**
     * @method index
     * @desc payment process
     * @return json response
    */
    public function store(Request $request){
        $data = $request->all();
        if(isset($data['success']) && $data['success'] > 0){
            $status = $this->successPayment($data);
            if($status){
                $success['success'] = 'Payment has been successfully done.';
                return response()->json($success, $this->successStatus);
            } else {
               $success['error'] = 'Payment could not be completed.';
                return response()->json($success, $this->successStatus); 
            }
        } else {
           $status = $this->failurePayment($data);
            if($status){
                $success['error'] = 'Data could not be updated.';
                return response()->json($success, $this->successStatus);
            } else {
               $success['error'] = 'Data could not be updated.';
               return response()->json($success, $this->successStatus); 
            }
        }
    }
    /**
     * @method successPayment
     * @desc this method will execute when payment success
     * @return response
    */
    private function successPayment($params){
        DB::beginTransaction();
       try {
            $course_data = Course::withCount('courseChapters')
                            ->where('id', $params['course_id'])
                            ->first();
            
            $new_time = date('Y-m-d H:i:s', strtotime('+'.$course_data->course_duration.' minutes', strtotime(date('Y-m-d H:i:s'))));

            $user_course_data = array(
                'user_id' => Auth::user()->id,
                'course_id' => $params['course_id'],
                'number_of_course_units' => $params['number_of_course_units'],
                'course_issued_at' => date('Y-m-d H:i:s'),
                'course_expire_at' => $new_time,
                'is_certificate_generate' => 0,
                'is_course_passed' => 0,
                'total_chapters' => $course_data->course_chapters_count,
                'completed_chapters' => 0,
                'is_course' => 1,
                'status' => 1
            );
            $store = UserCourse::create($user_course_data);

            $user_payment_data = array(
                'user_id' => Auth::user()->id,
                'course_id' => $params['course_id'],
                'user_course_id' => $store->id,
                'payment_method' => $params['payment_method'],
                'amount' => $params['amount'],
                'discount' => (isset($params['discount'])) ? $params['discount'] : '0',
                'total_amount' => (isset($params['total_amount'])) ? $params['total_amount'] : '0',
                'payment_status' => $params['payment_status'],
                'transaction_id' => $params['transaction_id'],
                'status' => 1,                
                'response_data' => json_encode($params['response_data'])
            );
            
            Payment::create($user_payment_data);
            
            if (isset($params['coupon_code']) && $params['coupon_code'] != ""  && $params['type'] == "offer" ) {
                $user_chapter_data = array(
                    'used_by' => Auth::user()->id,
                    'user_course_id' => $store->id,
                    'course_id' => $params['course_id'],
                    'coupon_number' => $params['coupon_code'],
                    'used_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                );
                $parent_coupon_data = UserCoupon::create($user_chapter_data);
            } else if (isset($params['coupon_code']) && $params['coupon_code'] != ""  && $params['type'] == "sme_coupon" ) {
                UserCoupon::where('coupon_number', $params['coupon_code'])
                    ->whereNull('used_by')
                    ->update(array('used_by' => Auth::user()->id));
            }

            if (isset($params['course_for_sme_individual']) && $params['course_for_sme_individual']=='for_individual' && $params['coupon_code'] != ""  && $params['type'] == "offer") {
                for ($i = 1; $i <= $params['number_of_course_units']; $i++) {
                    $user_chapter_data = array(
                        'user_id' => Auth::user()->id,
                        'parent_coupon_id' => $parent_coupon_data->id,
                        'course_id' => $params['course_id'],
                        'coupon_number' => strtoupper($this->cryptoRandSecure(12)),
                        'issued_at' => date('Y-m-d H:i:s'),
                        'status' => 1
                    );
                    UserCoupon::create($user_chapter_data);
                }
            }
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }
    /**
     * @method POST
     * @desc exam finished
     * @return success array
    */

    private function failurePayment($params){
        DB::beginTransaction();
        try {
            $user_payment_data = array(
                'user_id' => Auth::user()->id,
                'course_id' => $params['course_id'],
                'user_course_id' => 0,
                'payment_method' => $params['payment_method'],
                'amount' => $params['amount'],
                'discount' => $params['discount'],
                'total_amount' => $params['total_amount'],
                'payment_status' => $params['payment_status'],
                'transaction_id' => $params['transaction_id'],
                'status' => 1,
                'response_data' => json_encode($params['response_data'])
            );
            Payment::create($user_payment_data); 
            DB::commit();
            return true;
        }catch (\Exception $e) {
            DB::rollback();
            return false;
        }
        
    }

    /**
     * send push notification to user when course published
     *
     * @method sendCoursePublishNotification
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */

    /*public function sendCoursePublishNotification($params){

        $user_data = User::where('status','1')->where('id',$params['user_id'])->get()->first();
        
        $send_status = 2;

        $message = $params['title'];
        $title = $params['title'];
        
        $device_id = $user_data->device_id;            

        $response = NotificationHelper::sendNotification($message, $device_id, $title);
        $result = json_decode($response);
       
        if (is_object($result) && property_exists($result, 'success') && $result->success) {
            $send_status = 1;
        }
        
        $data = array(
            'module_id'=>$params['id'],
            'user_id'=>$user_data->id,
            'icon'=>Null,
            'module_type'=>$params['module_type'],
            'message'=>$message,
            'is_send'=>$send_status
            );
        
        UserNotification::create($data);
    }*/

    function cryptoRandSecure($length){
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet); // edited

        for ($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->crypto_secure(0, $max-1)];
        }

        return $token;
    }

    private function crypto_secure($min, $max){
        $range = $max - $min;
        if ($range < 1) return $min; // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd > $range);
        return $min + $rnd;
    }
}