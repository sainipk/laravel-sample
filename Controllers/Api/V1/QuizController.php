<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\User;
use App\Model\ChapterQuestions;
use App\Model\Course;
use App\Model\UserExam;
use App\Model\UserScreenActivity;
use App\Model\TestExamSetting;
use App\Model\ChapterQuestionOption;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Helpers\NotificationHelper;
use App\Services\UserScreenActivities;
use App\Model\UserNotification;
use App\Http\Requests\SaveScreenshotRequest;
use DB, PDF;

class QuizController extends Controller {

    private $user_activity;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->successStatus = apistatus('success');
        $this->errorStatus = apistatus('internalservererror');
        $this->unauthorizedStatus = apistatus('unauthorized');   
    }

    private function getExamSettingData($course_id, $type, $chapter_id){
        return TestExamSetting::where('course_id', $course_id)
                    ->whereTestType($type)
                    ->where(function($query) use ($chapter_id) {
                        if($chapter_id > 0){
                            $query->where('chapter_id', $chapter_id);
                        }
                    })->first();
                    
    }

    private function getChapterSettingData($course_id, $type, $chapter_id){
        return TestExamSetting::where('course_id', $course_id)
                    ->whereTestType('chapter')
                    ->where($type.'_test_chapter_no_of_question', '>', 0)
                    ->select('id', 'course_id', 'chapter_id', 'test_type', $type.'_test_chapter_no_of_question', 'duration')
                    ->whereStatus(TestExamSetting::Active)->get()->toArray();
    }

    /**
     * @method POST
     * @desc purchase course
     * @return course data array
    */
    public function getQuizData(Request $request, $course_id, $type, $chapter_id = 0){
        try {
            if($type == 'practice'){
                $test_exam_setting_data         =   $this->getExamSettingData($course_id, $type, $chapter_id);
                $test_chapter_setting_data      =   $this->getChapterSettingData($course_id, $type, $chapter_id);
            } else if($type == 'final'){
                $test_exam_setting_data         =   $this->getExamSettingData($course_id, $type, $chapter_id);
                $test_chapter_setting_data      =   $this->getChapterSettingData($course_id, $type, $chapter_id);
            } else if($type == 'chapter'){
                $test_exam_setting_data         =   $this->getExamSettingData($course_id, $type, $chapter_id);
            }

            $chapter_ids = [];
            if(isset($test_chapter_setting_data) && count($test_chapter_setting_data) > 0){
                foreach($test_chapter_setting_data as $test_chapter_setting){
                    $chapter_data = ChapterQuestions::whereHas('approvalDetails', function($query){
                                        $query->whereNotNull('approved_by')->whereNotNull('reviewed_by');
                                    })
                                    ->where(function($query) use ($chapter_id, $type, $test_chapter_setting){
                                        $query->where('chapter_id', $test_chapter_setting['chapter_id']);
                                    })
                                    ->whereCourseId($course_id)
                                    ->status()
                                    ->limit($test_chapter_setting[$type.'_test_chapter_no_of_question'])
                                    ->inRandomOrder()
                                    ->pluck('id')->toArray();
                    foreach($chapter_data as $chaptesids){
                        array_push($chapter_ids, $chaptesids);
                    }
                }
            }
            $chapter_data = ChapterQuestions::with('questionOptions', 'courseDetails')
                                ->whereHas('approvalDetails', function($query){
                                    $query->whereNotNull('approved_by');
                                })
                                ->where(function($query) use ($chapter_id, $type) {
                                    if($chapter_id > 0 && $type == 'chapter'){
                                        $query->where('chapter_id', $chapter_id);
                                    }
                                })
                                ->where(function($query) use ($type, $chapter_ids) {
                                    if(count($chapter_ids) > 0 && ($type == 'practice' || $type == 'final')){
                                        $query->whereIn('id', $chapter_ids);
                                    }
                                })
                                ->whereCourseId($course_id)
                                ->status()
                                ->limit($test_exam_setting_data->no_of_question)
                                ->inRandomOrder()
                                ->get();

            $data_arr = $this->modifyData($chapter_data, $test_exam_setting_data->duration);
            $success['data']    =  $data_arr;
            return response()->json($success, $this->successStatus);
        } catch(\Exception $e) {
            $error['error'] = $e->getMessage();
             return response()->json($error, $this->errorStatus);
        }
    }

    private function modifyData($chapter_data, $duration){
        $data_arr = [];
        foreach($chapter_data as $key => $data){
            $data_arr[$key]['duration']     =   $duration;
            $data_arr[$key]['q_id']         =   $data['id'];
            $data_arr[$key]['qno']          =   $key+1;
            $data_arr[$key]['q_text']       =   $data['title'];
            foreach($data->questionOptions as $option_key => $datas){
                $data_arr[$key]['qOptions'][$option_key]['label'] = $datas->title;
                $data_arr[$key]['qOptions'][$option_key]['value'] = $datas->id;
                $data_arr[$key]['qOptions'][$option_key]['size']  = 20;
                $data_arr[$key]['qOptions'][$option_key]['unselectedcolor'] = "#707070";
                $data_arr[$key]['qOptions'][$option_key]['selectedcolor'] = "#F47C22";
                $data_arr[$key]['qOptions'][$option_key]['selected'] = false;
            }
        }
        return $data_arr;
    }

    /**
     * @method POST
     * @data submit quiz
     * @return success
    */

    public function quizDataSubmit(Request $request){
        $test_submit_rules = UserExam::testSubmitRules();
        $validator = \Validator::make($request->all(), $test_submit_rules);
        if($validator->fails()) {
            return response()->json(["status" => false, "error" => $validator->errors()->first()]);
        }
      
        DB::beginTransaction();
        try {
            $test_data = $request->all();
            $test_type = $test_data['test_type'];
            
            foreach($test_data['test_data'] as $key => $data){
                $chapter_question_data = ChapterQuestions::with('correctQuestionOptions')
                                            ->where('id', $data['q_id'])
                                            ->where('course_id', $test_data['course_id'])
                                            ->first();
                if(isset($chapter_question_data) && !empty($chapter_question_data)){
                    $chapter_id = $chapter_question_data->chapter_id;
                    $test_exam_setting_data = TestExamSetting::where('course_id', $test_data['course_id'])
                                                ->where(function($query) use ($chapter_id, $test_type){
                                                    if($test_type == 'chapter'){
                                                        $query->where('chapter_id', $chapter_id);
                                                    }
                                                })
                                                ->where('test_type', $test_data['test_type'])
                                                ->first();
                    if(isset($test_exam_setting_data) && !empty($test_exam_setting_data)){
                        $data_arr = [
                            'user_id' => Auth::user()->id,
                            'course_id' => $test_data['course_id'],
                            'chapter_id' => $chapter_id,
                            'question_id' => $data['q_id'],
                            'test_exam_id' => $test_exam_setting_data->id,
                            'user_answer_id' => $data['selected_answer_option'],
                            'right_answer_id' => $chapter_question_data->correctQuestionOptions[0]->id,
                            'created_by' => Auth::user()->id,
                            'status' => 1,
                        ];
                        $quiz_submit = UserExam::create($data_arr);
                        UserScreenActivity::where('user_id', Auth::user()->id)
                            ->where('course_id', $test_data['course_id'])
                            ->where(function($query) use ($test_type, $chapter_id){
                                if($test_type == 'chapter'){
                                    $query->where('chapter_id', $chapter_id);
                                }
                            })
                            ->where('test_type', $test_type)
                            ->update(array('ip_address' => $test_data['ip_address'], 'latitude' => $test_data['latitude'], 'longitude' => $test_data['longitude']));
                        if($key == 0){
                            $result_id = $quiz_submit->id;
                        }
                        UserExam::where('id', $quiz_submit->id)
                                ->update(array('result_id' => $result_id));
                    }
                }
            }
            DB::commit();
            if($test_data['test_type'] == 'final'){
                $this->generateCertificate($test_data);
            }
            $success['success']     =   ___('test_submitted');
            $success['result_id']   =   $result_id;
            return response()->json($success, $this->successStatus);
        } catch(\Exception $e) {
            DB::rollback();
            $error['error'] = $e->getMessage();
            return response()->json($error, $this->errorStatus);
        }
    }

    public function quizResult(Request $request){
        $exam_data = UserExam::with('courseDetail', 'chapterQuestionDetails', 'userAnswerDetail', 'testExamDetail')
                        ->where('result_id', $request->get('result_id'))
                        ->where('user_id', Auth::user()->id)
                        ->get(['id', 'user_id', 'course_id', 'chapter_id', 'question_id', 'test_exam_id', 'user_answer_id', 'right_answer_id', 'result_id'])->toArray();
        $success['success']         =   ___('quiz_result');
        $success['exam_history']    =   $exam_data;
        
        $result_data = [];
        $v = 0;
        foreach($exam_data as $data){
            if($data['user_answer_id'] == $data['right_answer_id']){
                $v++;
            }
            $result_data['score']                   = (($v/$data['test_exam_detail']['no_of_question'])*100).'/100';
            $result_data['question_attempt_title']  = $v." Correct Answers Out Of ".$data['test_exam_detail']['no_of_question'];
        }
        $success['result_data']            =   $result_data;
        return response()->json($success, $this->successStatus);
    }

    public function saveScreenActivity(UserScreenActivities $user_activity, SaveScreenshotRequest $request){
        return $user_activity->savedUserScreenActivity($request->all());
    }

}
