<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Model\UserCoupon;
use App\Model\UserCourse;
use App\Model\Course;
use App\Model\Offer;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CouponController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->successStatus = apistatus('success');
        $this->errorStatus = apistatus('internalservererror');
        $this->unauthorizedStatus = apistatus('unauthorized');   
    }

    /**
     * @method GET/POST
     * @desc get Document List
     * @return course data array
    */

    private function checkAlreadyUseThisCoupon($data){
        return UserCoupon::where('used_by', Auth::user()->id)
                ->where('status', UserCoupon::Active)
                ->where('coupon_number', $data['coupon_number'])
                ->count();
    }
    
    public function applyCoupon(Request $request){
        $rules = UserCoupon::rules();
        $validator = \Validator::make($request->all(), $rules);
        if($validator->fails()) {
            return response()->json(["status" => false, "error" => $validator->errors()->first()]);
        }
        try {
            $coupon_for_user_type = (Auth::user()->role_id == User::Individual) ? 'individual' : 'sme';
            $offer_data = Offer::whereHas('offerActivityDetail', function($query) {
                                    $query->whereNotNull('reviewed_by')
                                    ->whereNotNull('published_by');
                                })
                                ->where('coupon_code', $request->get('coupon_number'))
                                ->where('coupon_for_user_type', $coupon_for_user_type)
                                ->where('status', Offer::Active)
                                ->first();
            if(isset($offer_data)){
                if($offer_data->coupon_type == 'number_of_uses' && $offer_data->remaining_coupons > 0){
                    $data = $request->all();
                    if(!$this->checkAlreadyUseThisCoupon($data)){
                        $success['success'] =  ___('coupon_apply');
                        $success['data']    =  $offer_data;
                        $success['data']['type']    =  'offer';
                        return response()->json($success, $this->successStatus);
                    } else {
                        return response()->json(["status" => false, "error" => ___('already_use_coupon')]);
                    }
                } elseif($offer_data->coupon_type == 'date_range' && (date("Y-m-d") >= $offer_data->start_date && date("Y-m-d") <= $offer_data->end_date) ) {
                    $data = $request->all();
                    if(!$this->checkAlreadyUseThisCoupon($data)){
                        $success['success'] =  ___('coupon_apply');
                        $success['data']    =  $offer_data;
                        $success['data']['type']    =  'offer';
                        return response()->json($success, $this->successStatus);
                    } else {
                        return response()->json(["status" => false, "error" => ___('already_use_coupon')]);
                    }
                } else {
                    return response()->json(["status" => false, "error" => ___('coupon_expired')]);
                }
            } else if($coupon_for_user_type == 'individual'){
                $user_coupon = UserCoupon::where('coupon_number', $request->get('coupon_number'))
                                    ->where('course_id', $request->get('course_id'))
                                    ->whereNull('used_by')
                                    ->first();
                if(isset($user_coupon)){
                    $data = $request->all();
                    $success['success'] =  ___('coupon_apply');
                    $success['data']    =  $user_coupon;
                    $success['data']['percentage']    =  "100.00";
                    $success['data']['type']    =  'sme_coupon';
                    return response()->json($success, $this->successStatus);
                } else {     
                    return response()->json(["status" => false, "error" => ___('invalid_coupon')]);
                }
            } else {     
                return response()->json(["status" => false, "error" => ___('invalid_coupon')]);
            }
        } catch(\Exception $e) {
            $error['error'] = $e->getMessage();
            return response()->json($error, $this->errorStatus);
        }
    }  
    
    public function getCouponDetails(Request $request, $course_id, $user_course_id){
        try {
            $coupon_data = UserCoupon::with('courseDetail', 'userDetail', 'getSmeCoupons')
                                ->courseId($course_id)
                                ->userCourseId($user_course_id)
                                ->usedBy()
                                ->status()
                                ->whereNotNull('user_course_id')
                                ->orderBy('id', 'Desc')
                                ->first();
            $coupon_data->courseDetail->course_duration = get_days($coupon_data->courseDetail->course_duration);
                      
            $success['success']         =  ___('coupon_list_fetched');                          
            $success['data']            =   $coupon_data; 
            return response()->json($success, $this->successStatus);
        } catch(\Exception $e) {
            $error['error'] = $e->getMessage();
                return response()->json($error, $this->errorStatus);
        }
    }

}
