<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\User;
use App\Model\Job;
use App\Model\UserAppliedOnJob;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Helpers\NotificationHelper;
use App\Model\UserNotification;

class ExamController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->successStatus = apistatus('success');
        $this->errorStatus = apistatus('internalservererror');
        $this->unauthorizedStatus = apistatus('unauthorized');   
    }
    /**
     * @method POST
     * @desc exam started
     * @return success array
    */
    public function examStarted(Request $request){
        
        $data = $request->all();

        $title = 'Course Final Exam Started';
        $send_param = array('id'=>1,'title'=>$title,'module_type'=>'exam_started','user_id'=>$data['user_id']);

        $this->sendCoursePublishNotification($send_param);

        $success['success'] =  'Course final exam start notification sent.';

        return response()->json($success, $this->successStatus);
        
    }
    /**
     * @method POST
     * @desc exam finished
     * @return success array
    */
    public function examFinished(Request $request){
        
        $data = $request->all();

        $title = 'Course Final Exam Finished';
        $send_param = array('id'=>1,'title'=>$title,'module_type'=>'exam_finish','user_id'=>$data['user_id']);

        $this->sendCoursePublishNotification($send_param);

        $success['success'] =  'Course final exam finish notification sent.';

        return response()->json($success, $this->successStatus);
        
    }
    /**
     * send push notification to user when course published
     *
     * @method sendCoursePublishNotification
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendCoursePublishNotification($params){

        $user_data = User::where('status','1')->where('id',$params['user_id'])->get()->first();
        
        $send_status = 2;

        $message = $params['title'];
        $title = $params['title'];
        
        $device_id = $user_data->device_id;            

        $response = NotificationHelper::sendNotification($message, $device_id, $title);
        $result = json_decode($response);
       
        if (is_object($result) && property_exists($result, 'success') && $result->success) {
            $send_status = 1;
        }
        
        $data = array(
            'module_id'=>$params['id'],
            'user_id'=>$user_data->id,
            'icon'=>Null,
            'module_type'=>$params['module_type'],
            'message'=>$message,
            'is_send'=>$send_status
            );
        
        UserNotification::create($data);         
        
    }

}
