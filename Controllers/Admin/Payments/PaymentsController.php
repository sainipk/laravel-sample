<?php
namespace App\Http\Controllers\Admin\Payments;

use App\Http\Controllers\Controller;
use App\Model\Permission;
use App\User;
use App\Model\Report;
use App\Model\Payment;
use App\Model\Category;
use App\Model\CourseTypes;
use App\Model\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Exports\PaymentsExport;
use Maatwebsite\Excel\Facades\Excel;

class PaymentsController extends Controller
{
    protected $route_base = "admin.payments.payments";

    public function __construct() {
        parent::__construct();

        $this->controller = "Payments";

        $this->title = "Payments";

        $this->model = Payment::class;

        $this->_data["breadcrumb"] = [
            "Payment" => route("admin.payments.payments.index")
        ];
    }

    public function index(Request $request) {
        $params = $request->all();
        $this->_data["courseCategory"] = Category::categoryList();
        $this->_data["coursetypes"] = CourseTypes::CourseTypeList();
        $this->_data["list_courses"] = Course::CourseList();
        $this->_data['params'] = $request->all();
        $this->_data['limit'] = $this->limit = $request->get('perpage') ? $request->get('perpage') : $this->limit;
        $this->_data['search'] = $request;
        $filter_data = $request->all();
        $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;

        $query = Payment::leftJoin('user_courses','user_courses.id','=','payments.user_course_id')
                        ->join('users','users.id','=','payments.user_id')
                        ->leftJoin('courses','courses.id','=','user_courses.course_id');
         $query->whereNull('users.deleted_at');
         $query->whereNull('courses.deleted_at');

        //dd($params);

        if (isset($params['name']) && $params['name']!='') {
            $query->where('users.name', 'like', '%' . $params['name']. '%');
        }
        if (isset($params['course_id']) && $params['course_id']!='') {
            //dd($params);
            $query->where('payments.course_id', $params['course_id']);
        }
        if (isset($params['from_date']) && $params['from_date']!='') {
            
            $date = explode(' - ', $params['from_date']);
            
            $end = date('Y-m-d',strtotime($date[1].'+1 day'));
           
            $query->where('user_courses.created_at','>=',$date[0]);
            $query->where('user_courses.created_at','<=',$end);
        }
        /*
        * export to excel
        */
        if (isset($params['export']) && $params['export']=='export') {
            
            $query->select('courses.title','users.name', 'user_courses.course_issued_at', 'user_courses.course_expire_at', 'user_courses.is_course_passed', 'user_courses.is_course', 'payments.payment_method', 'payments.amount', 'payments.discount', 'payments.total_amount', 'payments.payment_status', 'user_courses.created_at');;

            $new_data = $query->get()->toArray();
            
            if (!empty($new_data)) {
                foreach ($new_data as $key => $value) {

                    $new_data[$key]['created_at'] =  date('Y-m-d H:i:s',strtotime($value['created_at']));
                    $new_data[$key]['course_issued_at'] =  date('Y-m-d H:i:s',strtotime($value['course_issued_at']));
                    $new_data[$key]['course_expire_at'] =  date('Y-m-d H:i:s',strtotime($value['course_expire_at']));

                    if ($value['is_course_passed']==1) {
                        $new_data[$key]['is_course_passed'] =  'Yes';
                    }else{
                        $new_data[$key]['is_course_passed'] =  'No';
                    }
                    if ($value['is_course']==1) {
                        $new_data[$key]['is_course'] =  'New';
                    }else if ($value['is_course']==2) {
                        $new_data[$key]['is_course'] =  'Renew';
                    }else{
                        $new_data[$key]['is_course'] =  'Additional Chapter';
                    }
                }
            }
            return Excel::download(new PaymentsExport($new_data), time().'_payment_reports.xlsx');

        }else{

            $query->select('courses.title','user_courses.id', 'user_courses.course_id', 'payments.user_id', 'users.name', 'payments.payment_method', 'payments.amount', 'payments.discount', 'payments.total_amount', 'payments.payment_status', 'user_courses.course_issued_at', 'user_courses.course_expire_at','user_courses.is_course_passed', 'user_courses.is_course', 'user_courses.created_at');;

        }
        /*
        * get total record for showing total recrod
        */
        $this->_data['total_record'] =$query->count();
        /*
        * get all record to calculate total amount
        */
        $this->_data['total_data'] =$query->get();

        $total_amount = 0;
        
        if (!empty($this->_data['total_data'])) {
            foreach ($this->_data['total_data'] as $key => $value) {
                
                $total_amount +=$value->total_amount;
            }
        }

        $this->_data['total_amount'] = $total_amount;
        $this->_data['currency'] =icon('currency');
        // sort records
        //db_sort($query, $request);
        $data = $query->paginate($limit);
        
        $this->_data['data'] = $data;
        
        return view(load_view(), $this->_data);
    }
    public function export() 
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }
    
}