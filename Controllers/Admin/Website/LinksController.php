<?php
namespace App\Http\Controllers\Admin\Website;

use App\Http\Controllers\Controller;
use App\Model\Blog;
use App\Model\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class LinksController extends Controller
{
    /**
     * @desc all routes for this controller
     * @var string
     */

    public $route_base = "admin.website.links";

    /**
     * PageController constructor.
     */

    public function __construct() {
        parent::__construct();

        /**
         * @desc set controller name
         */
        $this->controller = "links";

        /**
         * @desc set page title
         */
        $this->title = "Links";

        $this->model = Blog::class;

        $this->_data["breadcrumb"] = [
            "Website" => route("admin.website.index"),
            $this->controller => route($this->routes["index"])
        ];
    }

    public function index(Request $request) {
        /**
         * send all params to view
         */
        $this->_data['params'] = $request->all();
        $this->_data['limit'] = $this->limit = $request->get('perpage') ? $request->get('perpage') : $this->limit;
        $this->_data['search'] = $request;
        $this->listing($request);
        return view(load_view(), $this->_data);
    }

    /**
     * @method listing
     * @desc prepare data to send react view
     * @param Request $request
     * @return array
     */
    public function listing(Request $request) {
        $filter_data = $request->all();
        $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;
        $query = Blog::where('page_type', Blog::LINK);
        // add filter in records
        searching_string($query, $request->form);

        // sort records
        db_sort($query, $request);
        $data = $query->paginate($limit);

        $this->_data['data'] = $data;

        return $this->_data;
    }

    /**
     * show create form
     *
     * @method create
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {
        $this->_data["breadcrumb"]["Create"] =  "javascript:void(0)";
        $this->_data["service"] = services_list();
        $this->_data["company"] = companies_list();
        return view(load_view(), $this->_data);
    }
    /**
     * store new records
     *
     * @method store
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) {

        $data = $request->all();
        $request->validate(Blog::linkrules());
        $banner = upload_file("file");
        if($banner){
            $data["banner"] = $banner;
        }
        if ($request->get("slug") === NULL) {
            $data["slug"] = Str::slug($request->get("name"));
        }
        $store = Blog::create($data);
        if($store) {
            if($request->has("service")){
                $store->services()->attach($request->get('service'));
            }
            if($request->has("company")){
                $store->companies()->attach($request->get('company'));
            }
            $request->session()->flash('success', $this->success_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route($this->routes["index"]));
    }

    /**
     * show edit form
     *
     * @method edit
     * @param Blog $blog
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Blog $link) {
        $this->_data["breadcrumb"]["Update"] =  "javascript:void(0)";
        $this->_data["service"] = services_list();
        $this->_data["company"] = companies_list();
        $this->_data["data"] = $link;
        return view(load_view(), $this->_data);
    }

    /**
     * update information
     *
     * @method update
     * @param Request $request
     * @param Blog $blog
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Blog $link) {
        $request->validate(Blog::linkrules($link->id));

        $data = $request->except("_token", "service");
        $banner = upload_file("file");
        if($banner){
            $data["banner"] = $banner;
        }
        if ($request->get("slug") === NULL) {
            $data["slug"] = Str::slug($request->get("name"));
        }
        $update = $link->update($data);
        if($update) {
            $link->services()->detach();
            if($request->has("service")){
                $link->services()->attach($request->get('service'));
            }
            $link->companies()->detach();
            if($request->has("company")){
                $link->companies()->attach($request->get('company'));
            }
            $request->session()->flash('success', $this->update_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route($this->routes["index"]));
    }
}
