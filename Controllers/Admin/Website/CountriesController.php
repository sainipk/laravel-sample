<?php
/**
 * Created by PhpStorm.
 * User: jitendrameena
 * Date: 12/05/20
 * Time: 4:42 PM
 */

namespace App\Http\Controllers\Admin\Website;


use App\Http\Controllers\Controller;
use App\Model\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CountriesController extends Controller
{
    /**
     * @desc all routes for this controller
     * @var string
     */

    public $route_base = "admin.website.countries";


    /**
     * CountriesController constructor.
     */

    public function __construct() {
        parent::__construct();

        /**
         * @desc set controller name
         */
        $this->controller = "Countries";

        /**
         * @desc set page title
         */
        $this->title = "Countries";

        $this->model = Country::class;


        $this->_data["breadcrumb"] = [
            "Website" => route("admin.website.index"),
            "Country" => route($this->routes["index"])
        ];
    }

    public function index(Request $request) {
        /**
         * send all params to view
         */
        $this->_data['params'] = $request->all();
        $this->_data['limit'] = $this->limit = $request->get('perpage') ? $request->get('perpage') : $this->limit;
        $this->_data['search'] = $request;
        $this->listing($request);
        return view(load_view(), $this->_data);
    }

    /**
     * @method listing
     * @desc prepare data to send react view
     * @param Request $request
     * @return array
     */
    public function listing(Request $request) {
        $filter_data = $request->all();
        $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;
        $query = Country::query();

        // add filter in records
        searching_string($query, $request->form);

        // sort records
        db_sort($query, $request);
        $data = $query->paginate($limit);

        $this->_data['data'] = $data;

        return $this->_data;
    }

    /**
     * show create form
     *
     * @method create
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {
        $this->_data["breadcrumb"]["Create"] =  "javascript:void(0)";
        return view(load_view(), $this->_data);
    }

    /**
     * store new records
     *
     * @method store
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) {
        $request->validate(Country::rules());
        $data = $request->all();
        if ($request->get("slug") === NULL) {
            $data["slug"] = Str::slug($request->get("name"));
        }
        $data["icon"] = upload_file("icon");
        $store = Country::create($data);
        if($store) {
            $request->session()->flash('success', $this->success_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route($this->routes["index"]));
    }

    /**
     * show edit form
     *
     * @method edit
     * @param Country $country
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Country $country) {
        $this->_data["breadcrumb"]["Update"] =  "javascript:void(0)";
        $this->_data["data"] = $country;
        return view(load_view(), $this->_data);
    }

    /**
     * update information
     *
     * @method update
     * @param Request $request
     * @param Country $country
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Country $country) {
        $request->validate(Country::rules($country->id));
        $data = $request->except("_token");
        if ($request->get("slug") === NULL) {
            $data["slug"] = Str::slug($request->get("name"));
        }
        $icon = upload_file("icon");
        if($icon){
            $data["icon"] = $icon;
        }
        $udpate = $country->update($data);
        if($udpate) {
            $request->session()->flash('success', $this->update_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route($this->routes["index"]));
    }
}
