<?php
namespace App\Http\Controllers\Admin\Website;


use App\Http\Controllers\Controller;
use App\Model\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MenuController extends Controller
{

    protected $route_base = "admin.website.menus";

    /**
     * @desc set controller name
     */
    public $controller = "Menu";

    /**
     * @desc set page title
     */
    public $title = "Menu";

    public $model = Menu::class;

    /**
     * TechnologyController constructor.
     */

    public function __construct() {
        parent::__construct();

        $this->_data["breadcrumb"] = [
            "Website" => route("admin.website.index"),
            $this->controller => route($this->routes["index"])
        ];
    }


    public function index(Request $request) {
        /**
         * send all params to view
         */
        $this->_data['params'] = $request->all();
        $this->_data['limit'] = $this->limit = $request->get('perpage') ? $request->get('perpage') : $this->limit;
        $this->_data['search'] = $request;
        $this->listing($request);
        return view(load_view(), $this->_data);
    }


    /**
     * @method listing
     * @desc prepare data to send react view
     * @param Request $request
     * @return array
     */
    public function listing(Request $request) {
        $filter_data = $request->all();
        $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;
        $query = Menu::with("page");

        // add filter in records
        searching_string($query, $request->form);

        // sort records
        db_sort($query, $request);
        $data = $query->paginate($limit);

        $this->_data['data'] = $data;

        return $this->_data;
    }


    /**
     * show create form
     *
     * @method create
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {
        $this->_data["breadcrumb"]["Create"] =  "javascript:void(0)";
        $this->_data["pages"] = page_list();
        $this->_data["menu"] = menu_parent_list();
       # dd(Menu::showCategories());
        return view(load_view(), $this->_data);
    }


    /**
     * store new records
     *
     * @method store
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) {
        $request->validate(Menu::$rules);
        $data = $request->all();
        $data["icon"] = upload_file("file");
        $store = Menu::create($data);
        if($store) {
            $request->session()->flash('success', $this->success_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route($this->routes["index"]));
    }


    /**
     * show edit form
     *
     * @method edit
     * @param Menu $menu
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Menu $menu) {
        $menus = menu_parent_list();
        unset($menus[$menu->id]);
        $this->_data["breadcrumb"]["Update"] =  "javascript:void(0)";
        $this->_data["pages"] = page_list();
        $this->_data["menu"] = $menus;
        $this->_data["data"] = $menu;
        return view(load_view(), $this->_data);
    }


    /**
     * update information
     *
     * @method update
     * @param Request $request
     * @param Menu $menu
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Menu $menu) {
        $request->validate(Menu::$rules);
        $data = $request->except("_token");
        $banner = upload_file("file");
        if($banner){
            $data["icon"] = $banner;
        }
        if(!$request->has("emerging_tech")){
            $data["emerging_tech"] = 0;
        }
        if($data['link_type'] == '1'){
            $data['blog_id'] = '';
        } else if($data['link_type'] == '2'){
            $data['link'] = '';
        } else {
            $data['blog_id'] = '';
            $data['link']    = '';
        }
        
        $update = $menu->update($data);
        if($update) {
            $request->session()->flash('success', $this->update_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route($this->routes["index"]));
    }


    public function treeView(Request $request){
        if($request->isMethod("post")){
            $data = json_decode($request->get("data"));

            if($data){
                Menu::updateMenu($data);
            }
        }

        $this->_data["breadcrumb"]["Tree VIew"] =  "javascript:void(0)";
        $data = Menu::where("status", 1)->orderBy("position")->get(["id", "title", "parent_id", DB::raw("label as text")])->toArray();
        $this->_data["data"] = json_encode(Menu::createJsTreeData($data));
        #$this->_data["data"] = $data;
        #echo "<pre>";
        #print_r($this->_data["data"]); die;
        return view(load_view(), $this->_data);
    }

}