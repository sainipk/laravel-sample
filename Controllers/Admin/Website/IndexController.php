<?php
/**
 * Created by PhpStorm.
 * User: jitendrameena
 * Date: 14/05/20
 * Time: 6:14 PM
 */

namespace App\Http\Controllers\Admin\Website;


use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index() {
        $this->_data["breadcrumb"] = [
            "Website" => "Javascript:void(0)",
        ];

        $modules = [
            ['title' => "Menus", 'icon' => 'flaticon-questions-circular-button', 'link' => ("admin.website.menus.index")],
            ['title' => "Pages", 'icon' => 'flaticon-book', 'link' => ("admin.website.pages.index")],
            ['title' => "Technologies", 'icon' => 'flaticon-settings-1', 'link' => ("admin.website.technologies.index")],
            ['title' => "Services", 'icon' => 'flaticon-users', 'link' => ("admin.website.services.index")],
            ['title' => "Countries", 'icon' => 'flaticon-home', 'link' => ("admin.website.countries.index")]
        ];
        $this->_data["modules"] = $modules;
        return view("admin.index", $this->_data);
    }
}
