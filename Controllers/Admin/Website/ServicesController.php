<?php
/**
 * Created by PhpStorm.
 * User: jitendrameena
 * Date: 12/05/20
 * Time: 4:42 PM
 */

namespace App\Http\Controllers\Admin\Website;


use App\Http\Controllers\Controller;
use App\Model\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ServicesController extends Controller
{
    /**
     * @desc all routes for this controller
     * @var string
     */

    public $route_base = "admin.website.services";


    /**
     * ServicesController constructor.
     */

    public function __construct() {
        parent::__construct();

        /**
         * @desc set controller name
         */
        $this->controller = "Services";

        /**
         * @desc set page title
         */
        $this->title = "Services";

        $this->model = Service::class;


        $this->_data["breadcrumb"] = [
            "Website" => route("admin.website.index"),
            "Service" => route($this->routes["index"])
        ];
    }

    public function index(Request $request) {
        /**
         * send all params to view
         */
        $this->_data['params'] = $request->all();
        $this->_data['limit'] = $this->limit = $request->get('perpage') ? $request->get('perpage') : $this->limit;
        $this->_data['search'] = $request;
        $this->listing($request);
        return view(load_view(), $this->_data);
    }

    /**
     * @method listing
     * @desc prepare data to send react view
     * @param Request $request
     * @return array
     */
    public function listing(Request $request) {
        $filter_data = $request->all();
        $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;
        $query = Service::query();

        // add filter in records
        searching_string($query, $request->form);

        // sort records
        db_sort($query, $request);
        $data = $query->paginate($limit);

        $this->_data['data'] = $data;

        return $this->_data;
    }

    /**
     * show create form
     *
     * @method create
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {
        $this->_data["breadcrumb"]["Create"] =  "javascript:void(0)";
        $this->_data["service"] = services_list();
        return view(load_view(), $this->_data);
    }

    /**
     * store new records
     *
     * @method store
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) {
        $request->validate(Service::rules());
        $data = $request->all();
        $data["icon"] = upload_file("icon");
        $data["slug"] = Str::slug($request->get("name"));
        $store = Service::create($data);
        if($store) {
            $request->session()->flash('success', $this->success_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route($this->routes["index"]));
    }

    /**
     * show edit form
     *
     * @method edit
     * @param Service $service
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Service $service) {
        $this->_data["breadcrumb"]["Update"] =  "javascript:void(0)";
        $this->_data["service"] = services_list();
        $this->_data["data"] = $service;
        return view(load_view(), $this->_data);
    }

    /**
     * update information
     *
     * @method update
     * @param Request $request
     * @param Service $service
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Service $service) {
        $request->validate(Service::rules($service->id));
        $data = $request->except("_token");
        $icon = upload_file("icon");
        if($icon){
            $data["icon"] = $icon;
        }
        $udpate = $service->update($data);
        if($udpate) {
            $request->session()->flash('success', $this->update_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route($this->routes["index"]));
    }
}
