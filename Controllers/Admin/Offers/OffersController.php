<?php
namespace App\Http\Controllers\Admin\Offers;

use App\Http\Controllers\Controller;
use App\Model\Offer;
use App\Model\UserCoupon;
use App\Model\OfferActivityLog;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File; 
use DB;
use Carbon\Carbon;

class OffersController extends Controller
{
    protected $route_base = "admin.offers.offers";

    public function __construct() {
        parent::__construct();

        $this->controller = "Offers";

        $this->title = "Offers";

        $this->model = Offer::class;

        $this->_data["breadcrumb"] = [
            "Offer" => route("admin.offers.offers.index")
        ];
    }

    public function index(Request $request) {
        /**
         * send all params to view
         */
        $this->_data['params'] = $request->all();
        $this->_data['limit'] = $this->limit = $request->get('perpage') ? $request->get('perpage') : $this->limit;
        $this->_data['search'] = $request;
        $this->listing($request);
        return view(load_view(), $this->_data);
    }


    /**
     * @method listing
     * @desc prepare data to send react view
     * @param Request $request
     * @return array
     */
    public function listing(Request $request) {

        $filter_data = $request->all();

        $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;

        $query = Offer::with(["offerActivityDetail"])->where('status', '!=','');

        if (isset($request->form['like']['title']) && $request->form['like']['title']!='' && $request->form['like']['title']!=null) {
            $query->where('title', $request->form['like']['title']);
        }
        
        if (isset($request->form['like']['coupon_code']) && $request->form['like']['coupon_code']!='' && $request->form['like']['coupon_code']!=null) {
            $query->where('coupon_code', $request->form['like']['coupon_code']);
        }
        
        if (isset($request->form['like']['start_date']) && $request->form['like']['start_date']!=null) {
            $date = explode(' - ', $request->form['like']['start_date']);
            
            $end = date('Y-m-d',strtotime($date[1].'+1 day'));
           
            $query->where('start_date','>=',$date[0]);
            $query->where('end_date','<=',$date[1]);
        }
        

        // add filter in records
        //searching_string($query, $request->form);

        // sort records
        db_sort($query, $request);
        $data = $query->paginate($limit);
        foreach($data as $key => $datas){
            if($datas->coupon_type == 'date_range'){
                $data[$key]['status'] = ($datas->end_date < date('Y-m-d')) ? 'expired' : $datas->status;
            } elseif($datas->coupon_type == 'number_of_uses') {
                $data[$key]['status'] = ($datas->remaining_coupons > 0) ? $datas->status : 'expired';
            }
        }
        $permissions = \App\Model\UserPermission::getUserPermissions();
        $this->_data['permissions'] = $permissions;
        $this->_data['data'] = $data;
       
        return $this->_data;
    }


    /**
     * show create form
     *
     * @method create
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {       
        $this->_data["breadcrumb"]["Create"] =  "javascript:void(0)";
        $this->_data['coupon_for_user_type'] = ['sme'=>'SME (Organization)
', 'individual'=>'Individual'];
        return view(load_view(), $this->_data);
    }


    /**
     * store new records
     *
     * @method store
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) {

        $request->validate(Offer::rules());       
        $data = $request->all();
        //dd($data);
        $data["status"] = 1;
        $data["user_id"] = Auth::id();

        if ($data['number_of_uses']!='') {
            $data['remaining_coupons'] = $data['number_of_uses'];
        }

        $store = Offer::create($data);
        if($store) {
            OfferActivityLog::create(array('offer_id'=>$store->id, 'created_by'=>Auth::id(),'created_at'=>date('Y-m-d H:i:s'),'status'=>1));
            $request->session()->flash('success', $this->success_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route($this->routes["index"]));
    }

    /**
     * show edit form
     *
     * @method edit
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request, Offer $offer) {
        $this->_data["breadcrumb"]["Update"] =  "javascript:void(0)";
        $this->_data["data"] = $offer;
        //dd($offer->coupon_code);
        if ($this->checkAlreadyUsedCoupon($offer->coupon_code)) {
            $request->session()->flash('error', 'This offer code is used by someone so you can not edit.');
            return redirect()->to(route($this->routes["index"]));
        }
        $this->_data['coupon_for_user_type'] = ['sme'=>'SME (Organization)
', 'individual'=>'Individual'];

        return view(load_view(), $this->_data);
    }

    /**
     * Show Detail Form
     *
     * @method show
     * @param Request $request, $id
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, $id){
        try {
            $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;
            $this->_data["breadcrumb"]["Show"] =  "javascript:void(0)";
            $this->_data["data"] = Offer::where('id', $id)
                                    ->first();
            return view(load_view(), $this->_data); 
        } catch(\Illuminate\Database\QueryException $ex) {
            return redirect()->back()->with(['message' => $ex->getMessage(), 'alert-class' => 'alert-danger']);
        }
    }

    /**
     * update information
     *
     * @method update
     * @param Request $request
     * @param Offer $offer
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id) {
        $request->validate(Offer::updateRules($id));
        $offer = Offer::findOrFail($id);

        $input = $request->all();
        
        if ($input['coupon_type']=='date_range') {
           $input['number_of_uses'] = null;
           $input['remaining_coupons'] = null;
        }else{
            $input['start_date'] = null;
            $input['end_date'] = null;
        }
        if ($input['number_of_uses']!='') {
            $input['remaining_coupons'] = $input['number_of_uses'];
        }
        $update = $offer->fill($input)->save();

        if($update) {

            $request->session()->flash('success', $this->update_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route($this->routes["index"]));
    }
    /**
     * bulk actions information delete, active and inactive
     *
     * @method actions
     * @param Request $request
     * @param Offer $offer
     * @return \Illuminate\Http\RedirectResponse
     */
    public function actions(Request $request) {

        $ids = $request->get('bulk_ids');

        if($request->get('action') == 'inactive'){
            $statusType = 'deactivated';
        } elseif($request->get('action') == 'active'){
            $statusType = 'activated';
        } elseif($request->get('action') == 'delete'){
            $statusType = 'deleted';
        }
       
        $response = $this->model::mass_action($request->get('action'), $ids);
        if($response['status'] == true){
            $request->session()->flash('success', $response['message']);
        } else {
            $request->session()->flash('error', $response['message']);
        }
        return redirect()->back();
    }

    public function offer_review(Request $request){
        
        $data = $request->all();        

        $offer_id = $data['offer_id'];
        
        $status = OfferActivityLog::join('offers', 'offers.id', '=', 'offer_activity_log.offer_id')->where('offer_activity_log.offer_id',$offer_id)->update(['offer_activity_log.reviewed_by'=>Auth::user()->id,'offer_activity_log.reviewed_at'=>date('Y-m-d H:i:s')]);
        
        if ($status) {

            $send_data['success'] = true;
            $send_data['message'] = 'Offer has been reviewed successfully.'; 

        }else{ 

            $send_data['success'] = false;
            $send_data['message'] = 'Offer could not be reviewed.'; 
        }
        echo json_encode($send_data);
    }

    public function offer_approve(Request $request){
        
        $data = $request->all();        

        $offer_id = $data['offer_id'];
        
        $status = OfferActivityLog::join('offers', 'offers.id', '=', 'offer_activity_log.offer_id')->where('offer_activity_log.offer_id',$offer_id)->update(['offer_activity_log.published_by'=>Auth::user()->id,'offer_activity_log.published_at'=>date('Y-m-d H:i:s')]);
        
        if ($status) {

            $send_data['success'] = true;
            $send_data['message'] = 'Offer has been approved successfully.'; 

        }else{ 

            $send_data['success'] = false;
            $send_data['message'] = 'Offer could not be approved.'; 
        }
        echo json_encode($send_data);
    }
    /**
     * Check coupon is used by some one only once
     *
     * @method checkAlreadyUsedCoupon
     * @param $coupon_code
     * @return true|false
     */
    public function checkAlreadyUsedCoupon($coupon_code){
        
        $data = UserCoupon::where('coupon_number',$coupon_code)->get();
        
        if ($data->count()>0) {
            return true;
        }else{
            return false;
        }
    }
    
}
