<?php
namespace App\Http\Controllers\Admin\TestExamSettings;

use App\Http\Controllers\Controller;
use App\Model\TestExamSetting;
use App\Model\TestExamSettingsActivityLog;
use App\Model\Course;
use App\Model\Chapter;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File; 
use DB;

class TestExamSettingsController extends Controller{

    protected $route_base = "admin.testexamsettings.testexamsettings";
    
    public function __construct() {

        parent::__construct();

        /**
         * @desc set controller name
         */
        $this->controller = "TestExamSettings";

        /**
         * @desc set page title
         */
        $this->title = "Test Exam Settings";

        $this->model = TestExamSetting::class;

        $this->_data["breadcrumb"] = [
            $this->controller => route($this->routes["index"]),
        ];
    }

    public function index(Request $request) {
        $this->_data['params'] = $request->all();
        $this->_data['limit'] = $this->limit = $request->get('perpage') ? $request->get('perpage') : $this->limit;
        $this->_data['search'] = $request;
        $this->listing($request);
        return view(load_view(), $this->_data);
    }

    /**
     * @method listing
     * @desc prepare data to send view
     * @param Request $request
     * @return array
     */
    public function listing(Request $request) {
        $filter_data = $request->all();
        $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;

        $e_data = TestExamSetting::groupBy('course_id')->pluck("course_id");
        
        if ($e_data->count()>0) {
           $e_data = $e_data->toArray();
        }

        $query = Course::whereIN('id',$e_data);

        // add filter in records
        searching_string($query, $request->form);

        // sort records
        db_sort($query, $request);
        //dd($query);
        $data = $query->paginate($limit);
      
        $permissions = \App\Model\UserPermission::getUserPermissions();
        $this->_data['permissions'] = $permissions;
        $this->_data['data'] = $data;
        
        return $this->_data;
    }

    /**
     * Show Detail Form
     *
     * @method show
     * @param Request $request, $id
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
     public function showTest($id){
         
        try {
            $this->_data["breadcrumb"]["Show"] =  "javascript:void(0)";
            $this->_data["courses"] = Course::CourseList();
            $this->_data["chapters"] = Chapter::ChapterList();
            $this->_data["data"] = TestExamSetting::where('course_id',$id)->get();   
            //return view(load_view(), $this->_data); 
            return view('admin.testexamsettings.testexamsettings.show', $this->_data);

        } catch(\Illuminate\Database\QueryException $ex) {
            return redirect()->back()->with(['message' => $ex->getMessage(), 'alert-class' => 'alert-danger']);
        }
    }

    /**
     * show create form
     *
     * @method create
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {
        $this->_data["breadcrumb"]["Create"] =  "javascript:void(0)";
        $this->_data["courses"] = Course::getCourseListForExamSetting();
        $this->_data["chapters"] = Chapter::ChapterList();
        return view(load_view(), $this->_data);
    }

    /**
     * store new records
     *
     * @method store
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) {
       
       // $request->validate(TestExamSetting::rules());
        $data = $request->all();
        //dd($data);
        
        if(isset($data['final_title']) && $data['final_title']!=''){
            $finaldata['created_by'] = Auth::user()->id;
            $finaldata['course_id'] = $data['course_id'];
            $finaldata['title'] = $data['final_title'];
            $finaldata['no_of_question'] = $data['final_no_of_question'];
            $finaldata['duration'] = $data['final_duration'];
            $finaldata['status'] = $data['status'];
            $finaldata['test_type'] = 'final';
            
            $store = TestExamSetting::create($finaldata);
            TestExamSettingsActivityLog::create(array('testexam_id'=>$store->id, 'created_by'=>Auth::id(),'created_at'=>date('Y-m-d H:i:s'),'status'=>1));
        }

        if(isset($data['practice_title']) && $data['practice_title']!=''){
            $practicedata['created_by'] = Auth::user()->id;
            $practicedata['course_id'] = $data['course_id'];
            $practicedata['title'] = $data['practice_title'];
            $practicedata['no_of_question'] = $data['practice_no_of_question'];
            $practicedata['duration'] = $data['practice_duration'];
            $practicedata['practice_test_attempt'] = $data['practice_test_attempt'];
            $practicedata['status'] = $data['status'];
            $practicedata['test_type'] = 'practice';
            
            $store = TestExamSetting::create($practicedata);
            TestExamSettingsActivityLog::create(array('testexam_id'=>$store->id, 'created_by'=>Auth::id(),'created_at'=>date('Y-m-d H:i:s'),'status'=>1));
        }
        

        $store = '';
        if (!empty($data['chapter_id'])) {            
                   
           foreach ($data['chapter_id'] as $key => $value) {
                $new_data = array();
                $new_data['created_by'] = Auth::user()->id;
                $new_data['course_id'] = $data['course_id'];
                $new_data['chapter_id'] = $data['chapter_id'][$key];
                $new_data['title'] = $data['chapter_title'][$key];
                $new_data['no_of_question'] = $data['chapter_no_of_question'][$key];
                $new_data['final_test_chapter_no_of_question'] = $data['final_test_chapter_no_of_question'][$key];
                $new_data['practice_test_chapter_no_of_question'] = $data['practice_test_chapter_no_of_question'][$key];
                $new_data['duration'] = $data['chapter_duration'][$key];
                //$new_data['final_test_chapter_duration'] = $data['final_test_chapter_duration'][$key];
                //$new_data['practice_test_chapter_duration'] = $data['practice_test_chapter_duration'][$key];
                $new_data['status'] = $data['status'];
                $new_data['test_type'] = 'chapter';
                
                $store = TestExamSetting::create($new_data);
                TestExamSettingsActivityLog::create(array('testexam_id'=>$store->id, 'created_by'=>Auth::id(),'created_at'=>date('Y-m-d H:i:s'),'status'=>1));
           }

        }
       
        if($store) {
            $request->session()->flash('success', $this->success_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route($this->routes["index"]));
    }

    /**
     * show edit form
     * @method edit
     * @param TestExamSetting $testexamsetting
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editTest($course_id) {
        
        $this->_data["breadcrumb"]["Update"] =  "javascript:void(0)";
        $this->_data["courses"] = Course::CourseList();

        $this->_data["chapters"] = Chapter::ChapterList();
        $course_detail = Course::getCourseDetail($course_id);
        $removed_chapter_id_array = Chapter::where('chapter_removed','1')->where('course_id',$course_id)->pluck('id');
        //dd($removed_chapter_id_array);
        $chapter_sttting_data = TestExamSetting::whereNotIn('chapter_id', $removed_chapter_id_array)->where('course_id',$course_id)->get();
        
        $this->_data["data"] = $chapter_sttting_data;
       //dd($this->_data["data"]);
        $old_chapter_id_array = array();
        
        foreach ($this->_data["data"] as $key => $value) {
            if ($value->chapter_id>0) {
                $old_chapter_id_array[] =  $value->chapter_id;
            }
            
        }
               
        $this->_data["new_data"] = Chapter::where('course_id',$course_id)->where('chapter_removed','2')->where('status','1')->whereNotIn('id',$old_chapter_id_array)->get();
       
       //dd($this->_data["new_data"]);
       //dd($this->_data);
       return view('admin.testexamsettings.testexamsettings.edit', $this->_data);
    }

    /**
     * update information
     *
     * @method update
     * @param Request $request
     * @param TestExamSetting $testexamsetting
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request) {
        //$request->validate(TestExamSetting::rules($testexamsetting->id));
        $data = $request->except("_token");
        $data = $request->all();
//dd($data);
        if(isset($data['final_title']) && $data['final_title']!=''){
           
            $finaldata['title'] = $data['final_title'];
            $finaldata['no_of_question'] = $data['final_no_of_question'];
            $finaldata['duration'] = $data['final_duration'];
            $finaldata['status'] = $data['status'];
            
            $store = TestExamSetting::where('test_type','final')->where('course_id',$data['course_id'])->update($finaldata);
        }

        if(isset($data['practice_title']) && $data['practice_title']!=''){
            $practicedata['title'] = $data['practice_title'];
            $practicedata['no_of_question'] = $data['practice_no_of_question'];
            $practicedata['duration'] = $data['practice_duration'];
            $practicedata['practice_test_attempt'] = $data['practice_test_attempt'];
            $practicedata['status'] = $data['status'];
            
            $store = TestExamSetting::where('test_type','practice')->where('course_id',$data['course_id'])->update($practicedata);
        }
        
        $store = '';
        if (!empty($data['chapter_id'])) {            
                   
           foreach ($data['chapter_id'] as $key => $value) {
                $new_data = array();
                
                $new_data['title'] = $data['chapter_title'][$key];
                $new_data['no_of_question'] = $data['chapter_no_of_question'][$key];
                $new_data['final_test_chapter_no_of_question'] = $data['final_test_chapter_no_of_question'][$key];
                $new_data['practice_test_chapter_no_of_question'] = $data['practice_test_chapter_no_of_question'][$key];
                $new_data['duration'] = $data['chapter_duration'][$key];
                //$new_data['final_test_chapter_duration'] = $data['final_test_chapter_duration'][$key];
                //$new_data['practice_test_chapter_duration'] = $data['practice_test_chapter_duration'][$key];
                $new_data['status'] = $data['status'];
              
                 if ( $data['chapter_id'][$key]>0) {

                    $store = TestExamSetting::where('test_type','chapter')->where('course_id',$data['course_id'])->where('chapter_id',$data['chapter_id'][$key])->update($new_data);
                  
                }else{
                    
                    $new_data['chapter_id'] = $data['old_chapter_id'][$key];
                    $new_data['course_id'] = $data['course_id'];
                    $new_data['created_by'] = Auth::id();
                    $new_data['test_type'] = 'chapter';
                   
                     $store = TestExamSetting::create($new_data);
                      TestExamSettingsActivityLog::create(array('testexam_id'=>$store->id, 'created_by'=>Auth::id(),'created_at'=>date('Y-m-d H:i:s'),'status'=>1));
                }
           }
        }

        if($store) {
            $request->session()->flash('success', $this->update_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route($this->routes["index"]));
    }

    /**
     * @method actions
     * @desc apply mass actions on records
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */

    public function actions(Request $request) {
        $ids = $request->get('bulk_ids');
       
        $response = $this->model::mass_action($request->get('action'), $ids);
        if($response['status'] == true){
            $request->session()->flash('success', $response['message']);
        } else {
            $request->session()->flash('error', $response['message']);
        }
        return redirect()->back();
    }

    public function testexamsetting_review(Request $request){
        
        $data = $request->all();        

        $course_id = $data['course_id'];
        $status = false;
        $settings = TestExamSetting::where('course_id',$course_id)->where('status','1')->get();

        if($settings->count()>0){
            foreach($settings as $setting){
                TestExamSettingsActivityLog::join('test_exam_settings', 'test_exam_settings.id', '=', 'test_exam_settings_activity_log.testexam_id')->where('test_exam_settings_activity_log.testexam_id',$setting->id)->update(['test_exam_settings_activity_log.reviewed_by'=>Auth::user()->id,'test_exam_settings_activity_log.reviewed_at'=>date('Y-m-d H:i:s')]);
            }
            $status = true;
        }
        
        if($settings->count()<1){
           
            $send_data['success'] = false;
            $send_data['message'] = 'TestExamSetting has inactive status.'; 
        }else if ($status) {
 
            $send_data['success'] = true;
            $send_data['message'] = 'TestExamSetting has been reviewed successfully.'; 

        }else{ 

            $send_data['success'] = false;
            $send_data['message'] = 'TestExamSetting could not be reviewed.'; 
        }
        echo json_encode($send_data);
    }

    public function testexamsetting_approve(Request $request){
        
        $data = $request->all();        

        $course_id = $data['course_id'];
        //dd($course_id);
        $status = false;
        $settings = TestExamSetting::where('course_id',$course_id)->where('status','1')->get();
        if($settings->count()>0){
            foreach($settings as $setting){
                 TestExamSettingsActivityLog::join('test_exam_settings', 'test_exam_settings.id', '=', 'test_exam_settings_activity_log.testexam_id')->where('test_exam_settings_activity_log.testexam_id',$setting->id)->update(['test_exam_settings_activity_log.published_by'=>Auth::user()->id,'test_exam_settings_activity_log.published_at'=>date('Y-m-d H:i:s')]);
            }
            $status = true;
        }
        
        if($settings->count()<1){           
            $send_data['success'] = false;
            $send_data['message'] = 'TestExamSetting has inactive status.'; 
        }else if ($status) {
            
            $send_data['success'] = true;
            $send_data['message'] = 'TestExamSetting has been approved successfully.'; 

        }else{ 

            $send_data['success'] = false;
            $send_data['message'] = 'TestExamSetting could not be approved.'; 
        }
        echo json_encode($send_data);
    }
}
