<?php

namespace App\Http\Controllers\Admin\Users;

use App\Http\Controllers\Controller;
use App\Mail\NewUserAccount;
use App\Model\Permission;
use App\User;
use App\Model\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Model\Course;
use App\Model\UserCourse;
use App\Model\UserCoupon;
use App\Model\UserCertificate;
use App\Model\UserScreenActivity;
use App\Model\ActivityLog;
use App\Model\UserExam;
use App\Model\ChapterQuestionOption;
use App\Helpers\CommonHelper;

class UsersController extends Controller
{

    protected $route_base = "admin.users.users";


    /**
     * TechnologyController constructor.
     */

    public function __construct() {

        $this->addRoutes("permissions");
        parent::__construct();
        /**
         * @desc set controller name
         */
        $this->controller = "Users";
        /**
         * @desc set page title
         */
        $this->title = "Users";
        $this->model = User::class;
        $this->_data["breadcrumb"] = [
            "User" => route("admin.users.index"),
            $this->controller => route($this->routes["index"]),
        ];
    }

    public function index(Request $request) {
        /**
         * send all params to view
         */
        $this->_data['params'] = $request->all();
        $this->_data['limit'] = $this->limit = $request->get('perpage') ? $request->get('perpage') : $this->limit;
        $this->_data['search'] = $request;
        $this->listing($request);
        return view(load_view(), $this->_data);
    }

    public function adminUsers(Request $request) {
        
        $this->_data['params'] = $request->all();
        $this->_data["breadcrumb"]["Admin"] =  "javascript:void(0)";
        $this->_data['limit'] = $this->limit = $request->get('perpage') ? $request->get('perpage') : $this->limit;
        $this->_data['search'] = $request;
        $this->adminListing($request);
        return view(load_view(), $this->_data);
    }

    public function adminListing(Request $request) {
        $filter_data = $request->all();
        $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;
        $query = User::with("role")->whereNotIn('role_id', array(6,7));

        // add filter in records
        searching_string($query, $request->form);

        // sort records
        db_sort($query, $request);
        $data = $query->paginate($limit);
        $this->_data['data'] = $data;
        $this->_data['roles'] = Role::where('status',1)->whereNotIn('id', [6,7])->pluck('title','id');
        return $this->_data;
    }

    /**
     * @method listing
     * @desc prepare data to send react view
     * @param Request $request
     * @return array
     */
    public function listing(Request $request) {
        $filter_data = $request->all();
        $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;
        $query = User::with("role")->whereIn('role_id', array(6,7));
       
        if(isset($filter_data['role_id']) && $filter_data['role_id']!=''){
            $query = User::with("role")->where('role_id', $filter_data['role_id']);
        }
        // add filter in records
        searching_string($query, $request->form);

        // sort records
        db_sort($query, $request);
        $data = $query->paginate($limit);

        $this->_data['data'] = $data;
        $this->_data['roles'] = Role::where('status',1)->whereIn('id', [6, 7])->pluck('title','id');

        return $this->_data;
    }

    public function getUsers(Request $request) {
        $filter_data = $request->all();
        $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;
        $query = User::with("role");
       
        if($request->role_id!=''){
            $query = User::with("role")->where('role_id', $request->role_id);
        }
        searching_string($query, $request->form);

        // sort records
        db_sort($query, $request);
        $data = $query->paginate($limit);

        $this->_data['data'] = $data;
        $this->_data['roles'] = Role::where('status',1)->whereIn('id', [6, 7])->pluck('title','id');
        $data = $this->_data;
        
        $view = view('admin.users.users.role-user-listing',compact('data'))->render();
        return response()->json(array('success' => true, 'html'=>$view));
        
    }
    /**
     * show create form
     *
     * @method create
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {
        $this->_data["breadcrumb"]["Create"] =  "javascript:void(0)";
        $this->_data["roles"] = role_list();
        return view(load_view(), $this->_data);
    }
    /**
     * store new records
     *
     * @method store
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) {
        $request->validate(User::rules());
        $data = $request->all();
        $data["status"] = 1;
        $data["password"] = bcrypt($data["password"]);
        $data["social_account_type"] = 'Email';
        $store = User::create($data);
        if($store) {
             return redirect()->to(route('admin.users.users.admin'));
        } else {
             $request->session()->flash('error', $this->error_response);
        }

        return redirect()->to(route($this->routes["index"]));
    }
    /**
     * show edit form
     *
     * @method edit
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(User $user) {
        $this->_data["breadcrumb"]["Update"] =  "javascript:void(0)";
        $this->_data["roles"] = role_list();
        $this->_data["data"] = $user;
        return view(load_view(), $this->_data);
    }

    /**
     * Show Detail Form
     *
     * @method show
     * @param Request $request, $id
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, $id){
        try {
            $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;
            $this->_data["breadcrumb"]["Show"] =  "javascript:void(0)";
            $this->_data["data"] = User::with(['role'])->where('id', $id)->first(); 
            $this->_data["courses"] = UserCourse::with(["courseDetail"])
                                    ->where('user_id', $id)->paginate($limit);
            $this->_data["coupons"] = UserCoupon::with(["courseDetail","userDetail"])->where('user_id', $id)->paginate($limit);
            $this->_data["certificates"] = UserCertificate::with(["courseDetail"])->where('user_id', $id)->paginate($limit);
            $this->_data["activities"] = ActivityLog::where('action_by', $id)->paginate($limit);
            /* $this->_data["screen_activities"] = UserScreenActivity::where('user_id', $id)
                                                    ->paginate($limit); */
            $this->_data["screen_activities"] = UserScreenActivity::with('getCourseDetail')
                                                    ->select('course_id')
                                                    ->where('user_id', $id)
                                                    ->groupBy('course_id')
                                                    ->paginate($limit);
            $chapter_tests = UserExam::chapterExamDetails($id);
            $practice_tests = UserExam::practiceExamDetails($id);
            $final_tests    = UserExam::finalExamDetails($id);
            
            $this->_data["chapter_tests"] = CommonHelper::createCourseChapterAccordian($chapter_tests);
            $this->_data["practice_tests"] = CommonHelper::createCourseChapterAccordian($practice_tests);
            $this->_data["final_tests"] = CommonHelper::createCourseChapterAccordian($final_tests);
            
            return view(load_view(), $this->_data); 
        } catch(\Illuminate\Database\QueryException $ex) {
            dd($ex);
            return redirect()->back()->with(['message' => $ex->getMessage(), 'alert-class' => 'alert-danger']);
        }
    }

    /**
     * update information
     *
     * @method update
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, User $user) {
        $request->validate(User::rules($user->id));
        $data = $request->except("_token");
        if($data["password"]  == ""){
            unset($data["password"]);
        } else {
            $data["password"] = bcrypt($data["password"]);
        }
        $old_role = $user->role_id;
        $udpate = $user->update($data);

        if($udpate) {
            $new_role = $user->role_id;
            if($old_role != $new_role){
                //$user->permission()->delete();
                //$user->permission()->create($user->role->toArray());
            } else {
                // $detailUserPermission = $user->permission()->first();
                // $detailUserPermission->permissions = $user->role->permissions;
                // $detailUserPermission->save($detailUserPermission->toArray());
            }
            //$request->session()->flash('success', $this->update_response);
            if($user->role_id == User::SME || $user->role_id == User::Individual){
                return redirect()->to(route('admin.users.users.index'));
            } else {
                return redirect()->to(route('admin.users.users.admin'));
            }
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route($this->routes["index"]));
    }



    public function permissions(Request $request, User $user) {
        $this->_data["breadcrumb"]["Permissions"] =  "javascript:void(0)";
        if($request->isMethod("put")){
            $request->validate(["permissions" => "required"]);
            $permissions["permissions"]     =   implode(",", $request->get("permissions"));
            if($user->permission()->update($permissions)){
                //$user->permission()->delete();
                $request->session()->flash('success', "User permission has been successfully updated");
            } else {
                $request->session()->flash('error', $this->error_response);
            }
            return redirect()->route($this->routes["index"]);
        }

        $this->_data["data"] = $user;
        $this->_data["permissions"] = Permission::getPermissionList();
        return view(load_view(), $this->_data);
    }


    public function editProfile(Request $request){
        if($request->isMethod("put")){
            $request->validate([
                "name" => "required",
                "password" => "nullable|min:6|max:20"
            ]);
            $data = $request->except("_token", "email");
            if($data["password"]  == ""){
                unset($data["password"]);
            } else {
                $data["password"] = bcrypt($data["password"]);
            }
            $user = get_user_info();
            $udpate = $user->update($data);
            if($udpate) {
                $request->session()->flash('success', "Profile has been successfully updated.");
            } else {
                $request->session()->flash('error', $this->error_response);
            }
        }

        $this->_data["data"] = get_user_info();
        return view(load_view(), $this->_data);
    }

    public function actions(Request $request) {
        $ids = $request->get('bulk_ids');
        if($request->get('action') == 'inactive'){
            $statusType = 'deactivated';
        } elseif($request->get('action') == 'active'){
            $statusType = 'activated';
        } elseif($request->get('action') == 'delete'){
            $statusType = 'deleted';
        }
       
        $response = $this->model::mass_action($request->get('action'), $ids);
        if($response['status'] == true){
            $request->session()->flash('success', $response['message']);
        } else {
            $request->session()->flash('error', $response['message']);
        }
        return redirect()->back();
    }
}
