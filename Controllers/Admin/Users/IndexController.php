<?php
namespace App\Http\Controllers\Admin\Users;
use App\User;
use App\Model\Role;

use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index() {
        $this->_data["breadcrumb"] = [
            "Users" => "Javascript:void(0)",
        ];

        $modules = [
            ['title' => "Roles", 'icon' => icon("roles"), 'link' => ("admin.users.roles.index"), "count" => Role::count()],
            ['title' => "Users", 'icon' => icon("users"), 'link' => ("admin.users.users.index"), "count" => User::where("role_id", "!=", User::ADMIN)->count()],
        ];
        $this->_data["modules"] = $modules;
        return view("admin.index", $this->_data);
    }
}
