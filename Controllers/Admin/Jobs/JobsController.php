<?php
namespace App\Http\Controllers\Admin\Jobs;

use App\Http\Controllers\Controller;
use App\Model\Job;
use App\User;
use App\Model\JobDocument;
use App\Model\UserAppliedOnJob;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File; 
use DB;

class JobsController extends Controller
{
    protected $route_base = "admin.jobs.jobs";

    public function __construct() {
        parent::__construct();

        $this->controller = "Jobs";

        $this->title = "Jobs";

        $this->model = Job::class;

        $this->_data["breadcrumb"] = [
            "Job" => route("admin.jobs.jobs.index")
        ];
    }

    public function index(Request $request) {
        /**
         * send all params to view
         */
        $this->_data['params'] = $request->all();
        $this->_data['limit'] = $this->limit = $request->get('perpage') ? $request->get('perpage') : $this->limit;
        $this->_data['search'] = $request;
        
        if (isset($_GET['type'])) {
            if ($_GET['type']=='asc') {
                $this->_data['type'] = 'desc';
            }else{
                $this->_data['type'] = 'asc';
            }
        }else{
            $this->_data['type'] = 'asc';
        }
         $permissions = \App\Model\UserPermission::getUserPermissions();       
        $this->_data['permissions'] = $permissions;
        
        $this->listing($request);
        return view(load_view(), $this->_data);
    }


    /**
     * @method listing
     * @desc prepare data to send react view
     * @param Request $request
     * @return array
     */
    public function listing(Request $request) {
        $filter_data = $request->all();
        $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;
        $query = Job::join('users','users.id','=','user_created_jobs.user_id')
        ->whereNull('users.deleted_at')
        ->whereNull('user_created_jobs.deleted_at')
        ->select('user_created_jobs.id','user_created_jobs.user_id','user_created_jobs.title','user_created_jobs.status','users.name as created_name');

        // add filter in records
        searching_string($query, $request->form);
        //dd($request);
        // sort records
        db_sort($query, $request);
        $query->orderBy('user_created_jobs.id');
        $data = $query->paginate($limit);
       // dd($data);
        $this->_data['data'] = $data;
        $this->_data['user_list'] = User::where('role_id','7')->select('id','name')->pluck( "name", "id")->toArray();

        return $this->_data;
    }


    /**
     * show create form
     *
     * @method create
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {       
        $this->_data["breadcrumb"]["Create"] =  "javascript:void(0)"; 
        return view(load_view(), $this->_data);
    }


    /**
     * store new records
     *
     * @method store
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) {
        
        $request->validate(Job::rules(), Job::messages());       
        $data = $request->all();
        $data["user_id"] = Auth::id();

        if($files = $request->file('resume')){

            $name = time().'_'.$files->getClientOriginalName(); 
            $data["links"] = $name; 
            $files->move('images/jobs',$name);                       
        }
        if($files2 = $request->file('cover_image')){

            $name2 = time().'_'.$files2->getClientOriginalName(); 
            $data["cover_image"] = $name2; 
            $files2->move('images/jobs',$name2);                       
        }
        
        $store = Job::create($data);

        if($store) {
            $request->session()->flash('success', $this->success_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route($this->routes["index"]));
    }

    /**
     * show edit form
     *
     * @method edit
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Job $job) {
        $this->_data["breadcrumb"]["Update"] =  "javascript:void(0)";
        $this->_data["data"] = $job; 
        return view(load_view(), $this->_data);
    }

    /**
     * Show Detail Form
     *
     * @method show
     * @param Request $request, $id
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Job $job, Request $request){
        
        try {
            $this->_data["breadcrumb"]["Show"] =  "javascript:void(0)";
            $this->_data["data"] = $job;

            $this->_data['params'] = $request->all();
            $this->_data['limit'] = $this->limit = $request->get('perpage') ? $request->get('perpage') : $this->limit;
            $this->_data['search'] = $request;
            if (isset($_GET['type'])) {
            if ($_GET['type']=='asc') {
                $this->_data['type'] = 'desc';
                }else{
                    $this->_data['type'] = 'asc';
                }
            }else{
                $this->_data['type'] = 'asc';
            }
            $this->_data["applied_user"] = $this->jobAppliedListing($request, $job->id);
            
            return view(load_view(), $this->_data); 
        } catch(\Illuminate\Database\QueryException $ex) {
            return redirect()->back()->with(['message' => $ex->getMessage(), 'alert-class' => 'alert-danger']);
        }
    }
    /**
     * @method listing
     * @desc prepare data to send react view
     * @param Request $request
     * @return array
     */
    public function jobAppliedListing(Request $request, $job_id) {
       
        $filter_data = $request->all();
        $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;
        $query = UserAppliedOnJob::join('users','users.id','=','user_applied_on_jobs.user_id')
        ->select('user_applied_on_jobs.id', 'user_applied_on_jobs.user_id', 'user_applied_on_jobs.is_confirmed', 'user_applied_on_jobs.uploaed_resume', 'user_applied_on_jobs.created_at', 'users.name','users.email','users.phone')
        ->where('user_applied_on_jobs.job_id',$job_id)
        ->whereNull('user_applied_on_jobs.deleted_at')
        ->whereNull('users.deleted_at');
        // add filter in records
        searching_string($query, $request->form);

        // sort records
        db_sort($query, $request);
        $query->orderBy('user_applied_on_jobs.id');
        $data = $query->paginate($limit);

        return $data;
    }

    /**
     * update information
     *
     * @method update
     * @param Request $request
     * @param Job $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Job $job) {
        $request->validate(Job::edit_rules(), Job::messages());   
        $data = $request->except("_token");        
        
        if($files = $request->file('resume')){

            $name = time().'_'.$files->getClientOriginalName(); 
            $data["links"] = $name; 
            $files->move('images/jobs',$name);
        }
        if($files2 = $request->file('cover_image')){

            $name2 = time().'_'.$files2->getClientOriginalName(); 
            $data["cover_image"] = $name2; 
            $files2->move('images/jobs',$name2);                       
        }
        $res = Job::select('links','cover_image')->where('id',$job->id)->first();

        $udpate = $job->update($data);

        if($udpate) {   
                 
                if($files = $request->file('resume')){
                    if ($res->links!='') {
                        $image_path = public_path("images/jobs/{$res->links}");

                        if (File::exists($image_path)) {
                            unlink($image_path);
                        }
                    }
                }
                if($files2 = $request->file('cover_image')){
                    if ($res->cover_image!='') {
                        $image_path2 = public_path("images/jobs/{$res->cover_image}");

                        if (File::exists($image_path2)) {
                            unlink($image_path2);
                        }
                    }
                }

            $request->session()->flash('success', $this->update_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route($this->routes["index"]));
    }
    /**
     * bulk actions information delete, active and inactive
     *
     * @method actions
     * @param Request $request
     * @param Job $Job
     * @return \Illuminate\Http\RedirectResponse
     */
    public function actions(Request $request) {

        $ids = $request->get('bulk_ids');

        if($request->get('action') == 'inactive'){
            $statusType = 'deactivated';
        } elseif($request->get('action') == 'active'){
            $statusType = 'activated';
        } elseif($request->get('action') == 'delete'){
            $statusType = 'deleted';
        }
       
        if($request->get('action') == "delete"){

            if (is_array($ids) && count($ids)>0) {
                
                foreach ($ids as $key => $value) {

                   
                   $res = Job::select('links','cover_image')->where('id',$value)->first();
                   if ($res->links!='') {
                        $image_path = public_path("images/jobs/{$res->links}");

                        if (File::exists($image_path)) {
                            unlink($image_path);
                        }
                    }
                    if ($res->cover_image!='') {
                        $image_path2 = public_path("images/jobs/{$res->cover_image}");

                        if (File::exists($image_path2)) {
                            unlink($image_path2);
                        }
                    }
                   
                }
            }
        }
        $response = $this->model::mass_action($request->get('action'), $ids);
        if($response['status'] == true){
            $request->session()->flash('success', $response['message']);
        } else {
            $request->session()->flash('error', $response['message']);
        }
        return redirect()->back();
    }
}
