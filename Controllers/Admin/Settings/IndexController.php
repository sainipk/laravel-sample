<?php
/**
 * Created by PhpStorm.
 * User: jitendrameena
 * Date: 14/05/20
 * Time: 6:14 PM
 */

namespace App\Http\Controllers\Admin\Settings;


use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index() {
        $this->_data["breadcrumb"] = [
            "Settings" => "Javascript:void(0)",
        ];

        $modules = [
            ['title' => "Configuration", 'icon' => icon("configuration"), 'link' => ("admin.settings.configurations.index")],
            ['title' => "Activity Log", 'icon' => icon("activity-log"), 'link' => ("admin.settings.activity-log.index")],
        ];
        $this->_data["modules"] = $modules;
        return view("admin.index", $this->_data);
    }
}
