<?php
namespace App\Http\Controllers\Admin\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Model\Configuration;

class ConfigurationsController extends Controller
{
    /**
     * @desc all routes for this controller
     * @var string
     */

    public $route_base = "admin.settings.configurations";

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        /**
         * @desc set controller name
         */
        $this->controller = "Configuration";

        /**
         * @desc set page title
         */
        $this->title = "Configuration";

         /**
         * @desc set model name
         */
        $this->model = Configuration::class;


        $this->_data['routes'] = $this->routes;


        $this->_data["breadcrumb"] = [
            "Settings" => route("admin.settings.index"),
            $this->controller => route($this->routes["index"]),
        ];

    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $configurations = Configuration::all()->groupBy('type');
        $this->_data['data'] = array();
        if($configurations) {
            $configurations = $configurations->toArray();
            foreach($configurations as $type=>$configuration) {
                foreach($configuration as $configure){
                    $this->_data['data'][$type][$configure['key']] = $configure['value'];
                }
            }
        }
        return view(load_view(), $this->_data);
    }

    /**
     * store new records
     *
     * @method store
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) {
        $data = $request->all();
        $type = $request->type;
        $typed = Configuration::where('type', $type);
        $typed->delete();

        $insert = [];
        unset($data['type']);
        unset($data['_token']);
        $favicon = upload_file("favicon");
        $site_logo = upload_file("site_logo");
        foreach($data as $key=>$value) {
            if($key == 'favicon'){
                $value = $favicon;
            } elseif($key == 'site_logo') {
                $value = $site_logo;
            }
            $insert[] = array(
                'type' => $type,
                'key' => $key,
                'value' => $value,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );
        }
        $store = Configuration::insert($insert);

        if($store) {
            $request->session()->flash('success', $type." Setting Saved Successfully");
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route($this->routes["index"]));
    }

}
