<?php

namespace App\Http\Controllers\Admin\Settings;


use App\Http\Controllers\Controller;
use App\Model\ActivityLog;
use Illuminate\Http\Request;

class ActivityLogController extends Controller
{
    /**
     * @desc all routes for this controller
     * @var string
     */

    public $route_base = "admin.settings.activity-log";


    /**
     * TechnologyController constructor.
     */

    public function __construct() {
        parent::__construct();

        /**
         * @desc set controller name
         */
        $this->controller = "Activity Log";

        /**
         * @desc set page title
         */
        $this->title = "Activity Log";

        $this->model = ActivityLog::class;


        $this->_data["breadcrumb"] = [
            "Settings" => route("admin.settings.index"),
            $this->controller => route($this->routes["index"]),
        ];

    }


    public function index(Request $request) {
        /**
         * send all params to view
         */
        $this->_data['params'] = $request->all();
        $this->_data['limit'] = $this->limit = $request->get('perpage') ? $request->get('perpage') : $this->limit;
        $this->_data['search'] = $request;
        $this->listing($request);
        return view(load_view(), $this->_data);
    }


    /**
     * @method listing
     * @desc prepare data to send react view
     * @param Request $request
     * @return array
     */
    public function listing(Request $request) {
        $filter_data = $request->all();
        $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;
        $query = ActivityLog::whereNotNull("message");

        // add filter in records
        searching_string($query, $request->form);

        // sort records
        db_sort($query, $request);
        $data = $query->paginate($limit);

        $this->_data['data'] = $data;

        return $this->_data;
    }
}
