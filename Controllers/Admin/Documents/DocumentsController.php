<?php
namespace App\Http\Controllers\Admin\Documents;

use App\Http\Controllers\Controller;
use App\Model\Document;
use App\Model\DocumentActivityLog;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File; 
use DB;
use App\User;
use App\Helpers\NotificationHelper;
use App\Model\UserNotification;
class DocumentsController extends Controller{

    protected $route_base = "admin.documents.documents";
    
    public function __construct() {

        parent::__construct();

        /**
         * @desc set controller name
         */
        $this->controller = "Documents";

        /**
         * @desc set page title
         */
        $this->title = "Documents";

        $this->model = Document::class;

        $this->_data["breadcrumb"] = [
            $this->controller => route($this->routes["index"]),
        ];
    }

    public function index(Request $request) {
        $this->_data['params'] = $request->all();
        $this->_data['limit'] = $this->limit = $request->get('perpage') ? $request->get('perpage') : $this->limit;
        $this->_data['search'] = $request;
        $this->listing($request);
        return view(load_view(), $this->_data);
    }

    /**
     * @method listing
     * @desc prepare data to send view
     * @param Request $request
     * @return array
     */
    public function listing(Request $request) {
        $filter_data = $request->all();
        $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;

        $query = Document::join('users','users.id','=','documents.user_id')
        ->whereNull('users.deleted_at')
        ->whereNull('documents.deleted_at')
        ->select('documents.id','documents.user_id','documents.title','documents.status', 'documents.created_at','documents.document_type','documents.links','users.name as created_name')->with(["documentActivityDetail"]);

        // add filter in records
        searching_string($query, $request->form);

        // sort records
        db_sort($query, $request);
        $data = $query->paginate($limit);
        $permissions = \App\Model\UserPermission::getUserPermissions();
        $this->_data['permissions'] = $permissions;
        $this->_data['data'] = $data;
         $this->_data['user_list'] = User::whereNotIN('role_id',[6,7])->select('id','name')->pluck( "name", "id")->toArray();
        return $this->_data;
    }

    /**
     * Show Detail Form
     *
     * @method show
     * @param Request $request, $id
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
     public function show(Request $request, $id){
         
        try {
            $this->_data["breadcrumb"]["Show"] =  "javascript:void(0)";
            $this->_data["data"] = Document::with(["userDetail","documentCreatedDetail", "documentUpdatedDetail", "documentReviewedDetail", "documentApprovedDetail", "documentActivityDetail"])
                                    ->where('id', $id)
                                    ->first();
             $permissions = \App\Model\UserPermission::getUserPermissions();       
            $this->_data['permissions'] = $permissions;   
            return view(load_view(), $this->_data); 
        } catch(\Illuminate\Database\QueryException $ex) {
            return redirect()->back()->with(['message' => $ex->getMessage(), 'alert-class' => 'alert-danger']);
        }
    }

    /**
     * show create form
     *
     * @method create
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {
        $this->_data["breadcrumb"]["Create"] =  "javascript:void(0)";
        return view(load_view(), $this->_data);
    }

    /**
     * store new records
     *
     * @method store
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) {
        $request->validate(Document::rules(), Document::messages());
        $data = $request->all();
        
        $data['user_id'] = Auth::user()->id;
        
        if($files = $request->file('links')){  
            $name = time().'_'.$files->getClientOriginalName();  
            $files->move('images/documents', $name); 

            $data["links"] = $name;
            $data["document_type"] = $request->links->getClientOriginalExtension();                 
        }
        /*if($files2 = $request->file('cover_image')){
            $name2 = time().'_'.$files2->getClientOriginalName();  
            $files2->move('images/documents/cover_image', $name2);
            $data["cover_image"] = $name2;            
        }*/

        $store = Document::create($data);
       
        if($store) {
            DocumentActivityLog::create(array('document_id' => $store->id, 'created_by' => Auth::id(), 'created_at' => date('Y-m-d H:i:s'), 'status' => 1));
            $request->session()->flash('success', $this->success_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route($this->routes["index"]));
    }

    /**
     * show edit form
     * @method edit
     * @param Documents $documents
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Document $document) {
        $this->_data["breadcrumb"]["Update"] =  "javascript:void(0)";
        $this->_data["data"] = $document;
       return view(load_view(), $this->_data);
    }

    /**
     * update information
     *
     * @method update
     * @param Request $request
     * @param Documents $documents
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Document $document) {
        
       // dd($request);

        $request->validate(Document::rules($document->id), Document::messages($document->id));
        $data = $request->except("_token");
        $d_type = $data["document_type"];

        if($files=$request->file('links')){ 

            $name=time().'_'.$files->getClientOriginalName();  
            $files->move('images/documents',$name);
            
            $data["links"] = $name;
            $data["document_type"] = $request->links->getClientOriginalExtension();
                                            
        }else{
            $data["links"] = $document->links;
            unset($data["document_type"]);
        }

        if($d_type=='2' && $files2=$request->file('cover_image')){  
            
            $name2=time().'_'.$files2->getClientOriginalName();
            $files2->move('images/documents/cover_image',$name2);
            $data["cover_image"] = $name2;

        }
        if ($d_type=='1') {
            $data["cover_image"] = '';
        }
            


        $udpate = $document->update($data);

        if($udpate) {
            DocumentActivityLog::where('document_id',$document->id)->update(['updated_at'=>date('Y-m-d H:i:s'), "updated_by"=>Auth::user()->id]);
            $request->session()->flash('success', $this->update_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route($this->routes["index"]));
    }

    /**
     * @method actions
     * @desc apply mass actions on records
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */

    public function actions(Request $request) {
        $ids = $request->get('bulk_ids');
       
        $response = $this->model::mass_action($request->get('action'), $ids);
        if($response['status'] == true){
            $request->session()->flash('success', $response['message']);
        } else {
            $request->session()->flash('error', $response['message']);
        }
        return redirect()->back();
    }

    public function document_review(Request $request){
        
        $data = $request->all();        

        $document_id = $data['document_id'];
        
        $status = DocumentActivityLog::join('documents', 'documents.id', '=', 'document_activity_log.document_id')->where('document_activity_log.document_id',$document_id)->update(['document_activity_log.reviewed_by'=>Auth::user()->id,'document_activity_log.reviewed_at'=>date('Y-m-d H:i:s')]);
        
        if ($status) {

            $send_data['success'] = true;
            $send_data['message'] = 'Document has been reviewed successfully.'; 

        }else{ 

            $send_data['success'] = false;
            $send_data['message'] = 'Document could not be reviewed.'; 
        }
        echo json_encode($send_data);
    }

    public function document_approve(Request $request){
        
        $data = $request->all();        

        $document_id = $data['document_id'];
        
        $status = DocumentActivityLog::join('documents', 'documents.id', '=', 'document_activity_log.document_id')->where('document_activity_log.document_id',$document_id)->update(['document_activity_log.published_by'=>Auth::user()->id,'document_activity_log.published_at'=>date('Y-m-d H:i:s')]);

        $ans_data = Document::where('id',$document_id)->get()->first();
        
        $send_param = array('id'=>$document_id,'title'=>$ans_data->title,'module_type'=>'document_upload');

        $this->sendCoursePublishNotification($send_param);
        
        if ($status) {

            $send_data['success'] = true;
            $send_data['message'] = 'Document has been approved successfully.'; 

        }else{ 

            $send_data['success'] = false;
            $send_data['message'] = 'Document could not be approved.'; 
        }
        echo json_encode($send_data);
    }
    /**
     * send push notification to user when document approve
     *
     * @method sendCoursePublishNotification
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendCoursePublishNotification($params){

        $user_data = User::where('status','1')->whereIn('role_id',[6,7])->get();
        $title = 'Document';
        if ($user_data->count()>0) {
            
            foreach ($user_data as $key => $value) {
                
                $send_status = 2;

                $message = $params['title'].' Document Created';
                
                $device_id = $value->device_id;            

                $response = NotificationHelper::sendNotification($message, $device_id, $title);
                $result = json_decode($response);
                
                if (is_object($result) && property_exists($result, 'success') && $result->success) {
                    $send_status = 1;
                }
                
                $data = array(
                    'module_id'=>$params['id'],
                    'user_id'=>$value->id,
                    'icon'=>Null,
                    'module_type'=>$params['module_type'],
                    'message'=>$message,
                    'is_send'=>$send_status
                    );
                
                UserNotification::create($data);
            }
        }
        
    }
}
