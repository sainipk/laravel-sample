<?php
namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use DB;

class UserController extends Controller
{
    public function __construct() {
        parent::__construct();

        $this->controller = "User";

        $this->title = "User";

        $this->model = User::class;

        $this->_data["breadcrumb"] = [
            "User" => route("admin.users.index")
        ];
    }

    public function index(Request $request) {
        
        $data = User::get();
        $this->_data['params'] = $request->all();
        $this->_data['limit'] = $this->limit = $request->get('perpage') ? $request->get('perpage') : $this->limit;
        $this->_data['search'] = $request;
        $this->listing($request);
        return view('admin.user.index', $this->_data);
    }

    public function listing(Request $request) {
        $filter_data = $request->all();
        $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;
        $query = User::where('role_id', '!=', User::ADMIN);

        // add filter in records
        searching_string($query, $request->form);

        // sort records
        db_sort($query, $request);
        $data = $query->paginate($limit);

        $this->_data['data'] = $data;

        return $this->_data;
    }

    public function create() {
        $this->_data["breadcrumb"]["Create"] =  "javascript:void(0)";
        return view('admin.user.create', $this->_data);
    }

    public function store(Request $request) {
        $request->validate(User::adminUserRules());
        $request->request->add([
            'role_id' => 1,
            'status' => 1,
            'password' => bcrypt($request->get('password'))
        ]);
        $store = User::create($request->all());
        if($store) {
            $request->session()->flash('success', $this->success_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route('admin.users.index'));
    }

    public function edit(User $user){
        $this->_data["breadcrumb"]["Update"] =  "javascript:void(0)";
        $this->_data["data"] = $user;
        return view('admin.user.edit',$this->_data);
    }

    /**
     * update information
     *
     * @method update
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */

    public function update(Request $request, $id){
        
        $request->validate(User::adminUserRules($id));
          
        $user = User::findOrFail($id);
      
        $input = $request->all();
        if(!empty($request->input('password'))){
            $input['password'] = bcrypt($request->input('password'));
        } else {
            $request->request->remove('password');
            $input = $request->all();
        }

        $update = $user->fill($input)->save();

        if($update) {
            $request->session()->flash('success', $this->update_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route('admin.users.index'));
    }

    public function destroy(Request $request,$id){
        $user = User::findOrFail($id);
        $user->delete();

        $request->session()->flash('success', $this->success_response);
        return redirect()->to(route('admin.users.index'));
    }

    public function editProfile(Request $request){
        if($request->isMethod("put")){
            
            $data = $request->except("_token");
           
            $request->validate(User::editProfile($data,$data["id"]));

            $data = $request->except("_token");
            
            $user = get_user_info();

            if($data["password"]  == ""){

                unset($data["password"]);
                unset($data["old_password"]);
                unset($data["confirm_password"]);

            } else {

                if (!Hash::check($request['old_password'], $user->password)) { 
              
                    $request->session()->flash('error', 'The old password does not match our records.'); 

                }else{

                     $data["password"] = bcrypt($data["password"]);
                     unset($data["old_password"]);
                     unset($data["confirm_password"]);

                    $udpate = $user->update($data);

                    if($udpate) {
                        $request->session()->flash('success', "Profile has been successfully updated.");
                    } else {
                        $request->session()->flash('error', $this->error_response);
                    }
                }                
            }
        }

        $this->_data["data"] = get_user_info();
        //dd($this->_data["data"]);
        return view(load_view(), $this->_data);
    }
    
}
