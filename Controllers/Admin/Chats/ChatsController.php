<?php
namespace App\Http\Controllers\Admin\Chats;

use App\Http\Controllers\Controller;
use App\Model\Chat;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Model\Conversation;
use DB;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class ChatsController extends Controller{

    protected $route_base = "admin.chats.chats";

    public function __construct() {
        parent::__construct();

        $this->controller = "Chats";

        $this->title = "Chats";

        $this->model = Chat::class;

        /*$this->_data["breadcrumb"] = [
            "Chats" => route("admin.chats.chats.index")
        ];*/
    }

    public function index(Request $request) {
        
        return view(load_view(), $this->_data);
    }

    public function getUserName(Request $request){
        $userId  = $request->user_id;
        $userName = User::find($userId)->name;
        return response()->json(array('userName'=> $userName), 200);
    }
}