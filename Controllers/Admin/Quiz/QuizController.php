<?php
namespace App\Http\Controllers\Admin\Quiz;

use App\Http\Controllers\Controller;
use App\Model\ChapterQuestions as Quiz;
use App\Model\ChapterQuestionOption as QuizOption;
use App\Model\QuizApproval;
use App\Model\Course;
use App\Model\Chapter;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File; 
use DB;
use Carbon\Carbon;

class QuizController extends Controller
{
    protected $route_base = "admin.quiz.quiz";

    public function __construct() {
        parent::__construct();

        $this->controller = "Quiz";

        $this->title = "Quiz";

        $this->model = Quiz::class;

        $this->_data["breadcrumb"] = [
            "Quiz" => route("admin.quiz.quiz.index")
        ];
    }

    public function index(Request $request) {
        /**
         * send all params to view
         */
        $this->_data['params'] = $request->all();
        $this->_data['limit'] = $this->limit = $request->get('perpage') ? $request->get('perpage') : $this->limit;
        $this->_data['search'] = $request;
        
        $this->listing($request);
        return view(load_view(), $this->_data);
    }


    /**
     * @method listing
     * @desc prepare data to send react view
     * @param Request $request
     * @return array
     */
    public function listing(Request $request) {
        $filter_data = $request->all();
        $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;
        $query = Course::with(["courseChapters","chapterQuiz","quizApprovalDetails"]);
        
        if(isset($filter_data['course_id']) && $filter_data['course_id']!=''){
            $query = $query->where('courses.id',$filter_data['course_id']);
        }

        // add filter in records
        searching_string($query, $request->form);

        // sort records
        db_sort($query, $request);
        $data = $query->paginate($limit);
        $permissions = \App\Model\UserPermission::getUserPermissions();
        $this->_data['permissions'] = $permissions;
        $this->_data['data'] = $data;
        $this->_data['courses'] = Course::where('status',1)->pluck('title','id');
        $this->_data['chapters'] = Chapter::where('status',1)->pluck('title','id');

        return $this->_data;
    }


    /**
     * show create form
     *
     * @method create
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {       
        $this->_data["breadcrumb"]["Create"] =  "javascript:void(0)";
        $this->_data["courses"] = Course::CourseList();
        $this->_data["chapters"] = Chapter::ChapterList();
        return view(load_view(), $this->_data);
    }


    /**
     * store new records
     *
     * @method store
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) {

        $data = $request->all();
        //dd($data);
        $data = $request->except("_token");
        $res =  Quiz::quiz_create_validation($data,$request);
        if($res!=''){
            $request->session()->flash('error', $res);
            return redirect()->to(route($this->routes["create"]));
        }
        //$data['created_by'] = Auth::user()->id;
        //$data['status'] = 1;
        $store = '';
        
        if(Course::coursePublishStatus($data['course_id'])){
            $request->session()->flash('error', "This course has been published already, so questions can't be added.");
            return redirect()->to(route($this->routes["create"]));
        }else{
            if (!empty($data['course_id']) && $res=='') {            
               foreach ($data['course_id'] as $key => $value) {
                    $new_data = array();
                    $new_data['course_id'] = $value;
                    $new_data['chapter_id'] = $data['chapter_id'][$key];
                    $new_data['title'] = $data['title'][$key];
                    $new_data['question_type'] = current(array_slice($data['question_type'],$key,1))[0];
                    $new_data['status'] = current(array_slice($data['status'],$key,1));
                    $new_data['created_by'] = Auth::user()->id;
                   

                    $store = Quiz::create($new_data);
                    
                    if ($store) {
                        QuizApproval::create(array('question_id'=>$store->id, 'created_by'=>Auth::id(),'created_at'=>date('Y-m-d H:i:s')));

                        if(!empty($data['quiz_answer'])){ 

                            foreach(current(array_slice($data['quiz_answer'],$key,1)) as $ks=>$quizanswer){

                                if($quizanswer!=''){

                                    $answer['question_id'] = $store->id;
                                    $answer['title'] = $quizanswer;

                                    if(isset(current(array_slice($data['right_answer'],$key,1))[0]) && current(array_slice($data['right_answer'],$key,1))[0]!=''){

                                        if($ks==current(array_slice($data['right_answer'],$key,1))[0]){
                                            $answer['is_answer'] = 1;
                                        }else{
                                            $answer['is_answer'] = 0;
                                        }
                                    }
                                    
                                    $answer['created_by'] = Auth::user()->id;
                                    $answer['status'] = 1;
                                    QuizOption::create($answer);   
                                }
                                
                            } 
                                            
                        }

                  /*      if(!empty($data['quiz_yesno_answer']) && $data['question_type'][$key]=='yesno'){ 
                            foreach($data['quiz_yesno_answer'][$key] as $quizanswer){
                                if($quizanswer!=''){
                                    $yesnoanswer['question_id'] = $store->id;
                                    $yesnoanswer['title'] = $quizanswer;
                                    if(isset($data['right_quiz_yesno_answer'][$key][0]) && $data['right_quiz_yesno_answer'][$key][0]!=''){
                                        if($quizanswer==$data['right_quiz_yesno_answer'][$key][0]){
                                            $yesnoanswer['is_answer'] = 1;
                                        }else{
                                            $yesnoanswer['is_answer'] = 0;
                                        }
                                    }
                                    
                                    $yesnoanswer['created_by'] = Auth::user()->id;
                                    $yesnoanswer['status'] = 1;
                                    QuizOption::create($yesnoanswer); 
                                }
                            } 
                                            
                        }*/
                    }
                    
               }
            }
        }
        

        if($store) {
            $request->session()->flash('success', $this->success_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route($this->routes["index"]));
    }

    /**
     * Show Detail Form
     *
     * @method show
     * @param Request $request, $id
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, $id){
        try {
            $this->_data["breadcrumb"]["Show"] =  "javascript:void(0)";
            $this->_data["data"] = Quiz::with(["userDetail", "courseDetails", "chapterDetails",'quizOptions',"quizCreatedDetail", "quizUpdatedDetail", "quizReviewedDetail", "quizApprovedDetail"])
                                    ->where('id', $id)
                                    ->first(); 
             //dd($this->_data["data"]);
            $permissions = \App\Model\UserPermission::getUserPermissions();
            $this->_data['permissions'] = $permissions;
            
            return view(load_view(), $this->_data); 
        } catch(\Illuminate\Database\QueryException $ex) {
            return redirect()->back()->with(['message' => $ex->getMessage(), 'alert-class' => 'alert-danger']);
        }
    }

    /**
     * show edit form
     *
     * @method edit
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Quiz $quiz) {

        $this->_data["breadcrumb"]["Update"] =  "javascript:void(0)";
        $this->_data["courses"] = Course::CourseList();
        $this->_data["chapters"] = Chapter::ChapterList('',$quiz['course_id']);
        $this->_data["question_options"] = QuizOption::where('question_id', $quiz->id)->get();
        //echo '<pre>';
        //print_r($this->_data["question_options"]);
        //dd($this->_data["question_options"]);
        $this->_data["data"] = $quiz;
        
        return view(load_view(), $this->_data);
    }
    /**
     * update information
     *
     * @method update
     * @param Request $request
     * @param Quiz $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Quiz $quiz) {
        //dd($quiz);
        $request->validate(Quiz::rules());  

        $data = $request->except("_token");
        $result = Course::coursePublishStatus($data['course_id']);
        
        if(Course::coursePublishStatus($data['course_id'])){
            $request->session()->flash('error', "This course has been published already, so questions can't be edit.");
            return redirect()->to(route($this->routes["index"]));
        }else{
            $udpate = $quiz->update($data);

            if($data['question_type']=='mcq' && sizeof($data['quiz_answer']) != 0 ){ 

                QuizOption::where('question_id',$quiz->id)->delete();

                foreach($data['quiz_answer'] as $key => $quizanswer){

                    $answer['question_id'] = $quiz->id;
                    $answer['title'] = $quizanswer;

                    if(isset($data['right_answer'][0]) && $data['right_answer'][0]!=''){
                        if($key==$data['right_answer'][0]){
                            $answer['is_answer'] = 1;
                        }else{
                            $answer['is_answer'] = 0;
                        }
                    }
                    
                    $answer['created_by'] = Auth::user()->id;
                    $answer['status'] = 1;

                    QuizOption::create($answer); 
                }              
            }else{

                QuizOption::where('question_id',$quiz->id)->delete();

                foreach($data['quiz_yesno_answer'] as  $key => $quizanswer){
                    $answer['question_id'] = $quiz->id;
                    $answer['title'] = $quizanswer;
                    if(isset($data['right_quiz_yesno_answer'][0]) && $data['right_quiz_yesno_answer'][0]!=''){
                        if($key==$data['right_quiz_yesno_answer'][0]){
                            $answer['is_answer'] = 1;
                        }else{
                            $answer['is_answer'] = 0;
                        }
                    }
                    
                    $answer['created_by'] = Auth::user()->id;
                    $answer['status'] = 1;
                    QuizOption::create($answer); 
                } 
            }

            if($udpate) {
                QuizApproval::where('question_id',$quiz->id)->update(['updated_at'=>date('Y-m-d H:i:s'), "updated_by"=>Auth::user()->id]);
                $request->session()->flash('success', $this->update_response);
            } else {
                $request->session()->flash('error', $this->error_response);
            }
            return redirect()->to(route($this->routes["index"]));
        }
        
    }
    /**
     * bulk actions information delete, active and inactive
     *
     * @method actions
     * @param Request $request
     * @param quiz $quiz
     * @return \Illuminate\Http\RedirectResponse
     */
    public function actions(Request $request) {

        $ids = $request->get('bulk_ids');
       
        $response = $this->model::mass_action($request->get('action'), $ids);
        if($response['status'] == true){
            $request->session()->flash('success', $response['message']);
        } else {
            $request->session()->flash('error', $response['message']);
        }
        return redirect()->back();
    }

    public function approval(Request $request) {
        //$data = $request->except("_token");
        //$data['question_id'] =$data['question_id'];
        $data['approved_by'] = Auth::user()->id;
        $data['approved_at'] = date('Y-m-d H:i:s');
        QuizApproval::where('question_id',$request->question_id)->update($data);
    }

    public function quiz_review(Request $request){
        
            $data = $request->all();     
            
            $question_id = $data['question_id'];
            
            $ques_status  = Quiz::where('id',$question_id)->where('status','1');

            if ( $ques_status->count()>0) {
                $status = QuizApproval::join('chapter_questions', 'chapter_questions.id', '=', 'quiz_approvals.question_id')->where('quiz_approvals.question_id',$question_id)->update(['quiz_approvals.reviewed_by'=>Auth::user()->id,'quiz_approvals.reviewed_at'=>date('Y-m-d H:i:s')]);
            
                if ($status) {

                    $send_data['success'] = true;
                    $send_data['message'] = 'Question has been reviewed successfully.'; 

                }else{ 

                    $send_data['success'] = false;
                    $send_data['message'] = 'Question could not be reviewed.'; 
                }
            }else{
                $send_data['success'] = false;
                $send_data['message'] = 'Question has inactive status.'; 
            }
            
            echo json_encode($send_data);
        
    }
    public function quiz_approve(Request $request){
        
            $data = $request->all();     
            
            $question_id = $data['question_id'];
            
            $status = QuizApproval::join('chapter_questions', 'chapter_questions.id', '=', 'quiz_approvals.question_id')->where('quiz_approvals.question_id',$question_id)->update(['quiz_approvals.approved_by'=>Auth::user()->id,'quiz_approvals.approved_at'=>date('Y-m-d H:i:s')]);
            
            if ($status) {

                $send_data['success'] = true;
                $send_data['message'] = 'Question has been approved successfully.'; 

            }else{ 

                $send_data['success'] = false;
                $send_data['message'] = 'Question could not be approved.'; 
            }
            echo json_encode($send_data);
        
    }
}
