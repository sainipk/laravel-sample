<?php
namespace App\Http\Controllers\Admin\Pages;

use App\Http\Controllers\Controller;
use App\Model\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use DB;

class PagesController extends Controller
{
    protected $route_base = "admin.pages.pages";

    public function __construct() {
        parent::__construct();

        $this->controller = "Pages";

        $this->title = "Pages";

        $this->model = Page::class;

        $this->_data["breadcrumb"] = [
            "Page" => route("admin.pages.pages.index")
        ];
    }

    public function index(Request $request) {
        /**
         * send all params to view
         */
        $this->_data['params'] = $request->all();
        $this->_data['limit'] = $this->limit = $request->get('perpage') ? $request->get('perpage') : $this->limit;
        $this->_data['search'] = $request;
        $this->listing($request);
        return view(load_view(), $this->_data);
    }


    /**
     * @method listing
     * @desc prepare data to send react view
     * @param Request $request
     * @return array
     */
    public function listing(Request $request) {
        $filter_data = $request->all();
        $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;
        $query = Page::where('status', '!=','');

        // add filter in records
        searching_string($query, $request->form);

        // sort records
        db_sort($query, $request);
        $data = $query->paginate($limit);

        $this->_data['data'] = $data;

        return $this->_data;
    }
    /**
     * show edit form
     *
     * @method edit
     * @param Page $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Page $page) {
        $this->_data["breadcrumb"]["Update"] =  "javascript:void(0)";
        $this->_data["data"] = $page; 
        return view(load_view(), $this->_data);
    }

    /**
     * Show Detail Form
     *
     * @method show
     * @param Request $request, $id
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Page $page){
        try {
            $this->_data["breadcrumb"]["Show"] =  "javascript:void(0)";
            $this->_data["data"] = $page;           
            return view(load_view(), $this->_data); 
        } catch(\Illuminate\Database\QueryException $ex) {
            return redirect()->back()->with(['message' => $ex->getMessage(), 'alert-class' => 'alert-danger']);
        }
    }
    /**
     * update information
     *
     * @method update
     * @param Request $request
     * @param Page $page
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Page $page) {
       
        $request->validate(Page::edit_rules());   
        $data = $request->except("_token");
        //$data['slug'] = strtolower(str_replace(' ', '-', $data['title']));
        $udpate = $page->update($data);

        if($udpate) {            
                
            $request->session()->flash('success', $this->update_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route($this->routes["index"]));
    }
}