<?php
namespace App\Http\Controllers\Admin\Categories;

use App\Http\Controllers\Controller;
use App\Model\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class CategoriesController extends Controller
{
    protected $route_base = "admin.categories.categories";

    public function __construct() {
        parent::__construct();

        $this->controller = "Categories";

        $this->title = "Categories";

        $this->model = Category::class;

        $this->_data["breadcrumb"] = [
            "Category" => route("admin.categories.categories.index")
        ];
    }

    public function index(Request $request) {
        /**
         * send all params to view
         */
        $this->_data['params'] = $request->all();
        $this->_data['limit'] = $this->limit = $request->get('perpage') ? $request->get('perpage') : $this->limit;
        $this->_data['search'] = $request;
        $this->listing($request);
        return view(load_view(), $this->_data);
    }


    /**
     * @method listing
     * @desc prepare data to send react view
     * @param Request $request
     * @return array
     */
    public function listing(Request $request) {
        $filter_data = $request->all();
        $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;
        $query = Category::where('status', '!=','');

        // add filter in records
        searching_string($query, $request->form);

        // sort records
        db_sort($query, $request);
        $data = $query->paginate($limit);

        $this->_data['data'] = $data;

        return $this->_data;
    }
    /**
     * show create form
     *
     * @method create
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {       
        $this->_data["breadcrumb"]["Create"] =  "javascript:void(0)";
        $this->_data["course_type"] = CourseTypes::CourseTypeList();
        return view(load_view(), $this->_data);
    }
    /**
     * store new records
     *
     * @method store
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) {
        $request->validate(Announcement::rules());       
        $data = $request->all();
        $data["status"] = 1;
        
        $store = Announcement::create($data);
        if($store) {

            if($files=$request->file('file')){  
                $name=time().'_'.$files->getClientOriginalName();  
                $files->move('images/announcements',$name); 

                $new_data = array(
                    'announcement_id'=>$store->id,
                    'link'=>$name
                    );  
                AnnouncementDocument::create($new_data);                 
            }

            $request->session()->flash('success', $this->success_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route($this->routes["index"]));
    }

    /**
     * show edit form
     *
     * @method edit
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Category $category) {
        $this->_data["breadcrumb"]["Update"] =  "javascript:void(0)";
        $this->_data["data"] = $category;
        return view(load_view(), $this->_data);
    }

    /**
     * Show Detail Form
     *
     * @method show
     * @param Request $request, $id
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, $id){
        try {
            $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;
            $this->_data["breadcrumb"]["Show"] =  "javascript:void(0)";
            $this->_data['link'] = $this->getDocumentName($id);
            $this->_data["data"] = Announcement::with(["courseTypes"])
                                    ->where('id', $id)
                                    ->first();
            $this->_data["subscribers"] = AnnouncementSubscriber::with(["userDetail","announcementDetail"])
                                    ->where('announcement_id', $id)
                                    ->paginate($limit);                       
            return view(load_view(), $this->_data); 
        } catch(\Illuminate\Database\QueryException $ex) {
            return redirect()->back()->with(['message' => $ex->getMessage(), 'alert-class' => 'alert-danger']);
        }
    }

    /**
     * update information
     *
     * @method update
     * @param Request $request
     * @param Announcement $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Category $category) {
          
        $data = $request->except("_token"); 
        $status = Category::checkCategoryExist($data['title'],$category->id);
        
        if (!$status) {
            
            if($files=$request->file('image')){ 
         
                $name=time().'_'.$files->getClientOriginalName();  
                $files->move('images/categories',$name);            
                $data["image"] = $name;
                                                
            }
            $udpate = $category->update($data);

            if($udpate) {

                $request->session()->flash('success', $this->update_response);
            } else {
                $request->session()->flash('error', $this->error_response);
            }
        }else{
           $request->session()->flash('error', 'Category title should be unique.'); 
        }
        return redirect()->to(route($this->routes["index"]));
    }
    /**
     * bulk actions information delete, active and inactive
     *
     * @method actions
     * @param Request $request
     * @param Announcement $announcement
     * @return \Illuminate\Http\RedirectResponse
     */
    public function actions(Request $request) {

        $ids = $request->get('bulk_ids');

        if($request->get('action') == 'inactive'){
            $statusType = 'deactivated';
        } elseif($request->get('action') == 'active'){
            $statusType = 'activated';
        } elseif($request->get('action') == 'delete'){
            $statusType = 'deleted';
        }
       
        if($request->get('action') == "delete"){
            if (is_array($ids) && count($ids)>0) {
                
                foreach ($ids as $key => $value) {

                   $file_name = '';
                   $file_name = $this->getDocumentName($value);
                   if ($file_name!='') {
                        $image_path = public_path("images/announcements/{$file_name}");

                        if (File::exists($image_path)) {
                            unlink($image_path);
                        }
                   }
                   
                }
            }
        }
        $response = $this->model::mass_action($request->get('action'), $ids);
        if($response['status'] == true){
            $request->session()->flash('success', $response['message']);
        } else {
            $request->session()->flash('error', $response['message']);
        }
        return redirect()->back();
    }
}
