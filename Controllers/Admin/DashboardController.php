<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use App\Model\Announcement;
use App\Model\Job;
use App\Model\Course;
use App\Model\Page;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{

    public function __construct() {
        parent::__construct();

        $this->controller = "Dashboard";

        $this->title = "Dashboard";

        $this->model = Dashboard::class;

        $this->_data["breadcrumb"] = [
            "User" => route("admin.users.index")
        ];
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        if (!Auth::check()){
            return redirect()->to(route('home'));
        }

        $modules = [
                    
                    ['title' => "Users", 'icon' => icon("users"), 'link' => ("admin.users.users.index"), "count" => User::where("role_id", "!=", User::ADMIN)->count()],
                    ['title' => "Announcements", 'icon' => icon("announcements"), 'link' => ("admin.announcements.announcements.index"), "count" => Announcement::where("status", "!=", '')->count()],
                    ['title' => "Jobs", 'icon' => icon("jobs"), 'link' => ("admin.jobs.jobs.index"), "count" => Job::where("status", "!=", '')->count()],
                    ['title' => "Courses", 'icon' => icon("Courses"), 'link' => ("admin.courses.courses.index"), "count" => Course::where("status", "!=", '')->count()],
                    /*['title' => "Web Pages", 'icon' => icon("Web Pages"), 'link' => ("admin.pages.pages.index"), "count" => Page::where("status", "!=", '')->count()],*/
                   ];
        $this->_data["modules"] = $modules;

        return view('admin.dashboard', $this->_data);

    }
}
