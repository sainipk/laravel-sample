<?php
namespace App\Http\Controllers\Admin\Announcements;

use App\Http\Controllers\Controller;
use App\Model\Announcement;
use App\Model\AnnouncementDocument;
use App\Model\CourseTypes;
use App\Model\AnnouncementSubscriber;
use App\Model\AnnouncementApproval;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File; 
use DB;
use Carbon\Carbon;
use App\User;
use App\Helpers\NotificationHelper;
use App\Model\UserNotification;

class AnnouncementsController extends Controller
{
    protected $route_base = "admin.announcements.announcements";

    public function __construct() {
        parent::__construct();

        $this->controller = "Announcements";

        $this->title = "Announcements";

        $this->model = Announcement::class;

        $this->_data["breadcrumb"] = [
            "Announcement" => route("admin.announcements.announcements.index")
        ];
    }

    public function index(Request $request) {
        /**
         * send all params to view
         */
        $this->_data['params'] = $request->all();
        $this->_data['limit'] = $this->limit = $request->get('perpage') ? $request->get('perpage') : $this->limit;
        $this->_data['search'] = $request;
        $this->_data['status'] = ['1'=>'Active','2'=>'Deactive','3'=>'Expired'];
        $this->_data['course_type'] = ['1'=>'Individual','2'=>'SME (Organization)'];
        $this->listing($request);
        return view(load_view(), $this->_data);
    }


    /**
     * @method listing
     * @desc prepare data to send react view
     * @param Request $request
     * @return array
     */
    public function listing(Request $request) {
        $filter_data = $request->all();
        $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;
        $query = Announcement::with(['courseTypes','approvalDetails'])->where('status', '!=','');

        if (isset($request->form['like']['event_date_time']) && $request->form['like']['event_date_time']!=null) {
            $date = explode(' - ', $request->form['like']['event_date_time']);
            
            $end = date('Y-m-d',strtotime($date[1].'+1 day'));
           
            $query->where('event_date_time','>=',$date[0]);
            $query->where('event_date_time','<=',$end);
        }
        
        // add filter in records
        searching_string($query, $request->form);

        // sort records
        db_sort($query, $request);
        $data = $query->paginate($limit);
        $permissions = \App\Model\UserPermission::getUserPermissions();
        $this->_data['permissions'] = $permissions;
        $this->_data['data'] = $data;
       
        return $this->_data;
    }


    /**
     * show create form
     *
     * @method create
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {       
        $this->_data["breadcrumb"]["Create"] =  "javascript:void(0)";
        $this->_data["course_type"] = CourseTypes::CourseTypeList();
        return view(load_view(), $this->_data);
    }


    /**
     * store new records
     *
     * @method store
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) {
        $request->validate(Announcement::rules());       
        $data = $request->all();
        $data["status"] = 1;
        
        $store = Announcement::create($data);
        if($store) {

            if($files=$request->file('file')){  
                $name=time().'_'.$files->getClientOriginalName();  
                $files->move('images/announcements',$name); 

                $new_data = array(
                    'announcement_id'=>$store->id,
                    'link'=>$name,
                    'status'=>1
                    );  
                AnnouncementDocument::create($new_data);                 
            }

            AnnouncementApproval::create(array('announcement_id'=>$store->id, 'created_by'=>Auth::id(),'created_at'=>date('Y-m-d H:i:s'),'status'=>1));

            $request->session()->flash('success', $this->success_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route($this->routes["index"]));
    }

    /**
     * show edit form
     *
     * @method edit
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Announcement $announcement) {
        $this->_data["breadcrumb"]["Update"] =  "javascript:void(0)";
        $this->_data["course_type"] = CourseTypes::CourseTypeList();
        
        $this->_data['link'] = $this->getDocumentName($announcement->id);
        $this->_data["data"] = $announcement;

        
        return view(load_view(), $this->_data);
    }

    /**
     * Show Detail Form
     *
     * @method show
     * @param Request $request, $id
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, $id){
        try {
            $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;
            $this->_data["breadcrumb"]["Show"] =  "javascript:void(0)";
            $this->_data['link'] = $this->getDocumentName($id);
            $this->_data["data"] = Announcement::with(["courseTypes"])
                                    ->where('id', $id)
                                    ->first();
            $this->_data["subscribers"] = AnnouncementSubscriber::with(["userDetail","announcementDetail"])
                                    ->where('announcement_id', $id)
                                    ->paginate($limit);                       
            return view(load_view(), $this->_data); 
        } catch(\Illuminate\Database\QueryException $ex) {
            return redirect()->back()->with(['message' => $ex->getMessage(), 'alert-class' => 'alert-danger']);
        }
    }

    /**
     * update information
     *
     * @method update
     * @param Request $request
     * @param Announcement $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Announcement $announcement) {
        $request->validate(Announcement::rules());   
        $data = $request->except("_token");        
        
        $udpate = $announcement->update($data);

        if($udpate) {

            if($files=$request->file('file')){ 

                $name=time().'_'.$files->getClientOriginalName();  
                $files->move('images/announcements',$name);
                
                $res = AnnouncementDocument::where('announcement_id',$announcement->id)->update(['link' => $name]); 

                if (!$res) {
                    $new_data = array(
                    'announcement_id'=>$announcement->id,
                    'link'=>$name
                    );  
                    AnnouncementDocument::create($new_data);
                }
                /*$file_name = $this->getDocumentName($announcement->id);
                
                if ($file_name!='') {
                    $image_path = public_path("images/announcements/{$file_name}");

                    if (File::exists($image_path)) {
                        unlink($image_path);
                    }
                } */                               
            }

            $request->session()->flash('success', $this->update_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route($this->routes["index"]));
    }
    /**
     * bulk actions information delete, active and inactive
     *
     * @method actions
     * @param Request $request
     * @param Announcement $announcement
     * @return \Illuminate\Http\RedirectResponse
     */
    public function actions(Request $request) {

        $ids = $request->get('bulk_ids');

        if($request->get('action') == 'inactive'){
            $statusType = 'deactivated';
        } elseif($request->get('action') == 'active'){
            $statusType = 'activated';
        } elseif($request->get('action') == 'delete'){
            $statusType = 'deleted';
        }
       
        if($request->get('action') == "delete"){
            if (is_array($ids) && count($ids)>0) {
                
                foreach ($ids as $key => $value) {

                   $file_name = '';
                   $file_name = $this->getDocumentName($value);
                   if ($file_name!='') {
                        $image_path = public_path("images/announcements/{$file_name}");

                        if (File::exists($image_path)) {
                            unlink($image_path);
                        }
                   }
                   
                }
            }
        }
        $response = $this->model::mass_action($request->get('action'), $ids);
        if($response['status'] == true){
            $request->session()->flash('success', $response['message']);
        } else {
            $request->session()->flash('error', $response['message']);
        }
        return redirect()->back();
    }
    public function getDocumentName($announcement_id){
        return AnnouncementDocument::where('announcement_id',$announcement_id)->pluck('link')->first();
    }

    public function announcements_review(Request $request){
        
            $data = $request->all();     
            
            $announcement_id = $data['announcement_id'];
            
            $status = AnnouncementApproval::join('announcements', 'announcements.id', '=', 'announcement_approvals.announcement_id')->where('announcement_approvals.announcement_id',$announcement_id)->update(['announcement_approvals.reviewed_by'=>Auth::user()->id,'announcement_approvals.reviewed_at'=>date('Y-m-d H:i:s')]);
            
            if ($status) {

                $send_data['success'] = true;
                $send_data['message'] = 'Announcement has been reviewed successfully.'; 

            }else{ 

                $send_data['success'] = false;
                $send_data['message'] = 'Announcement could not be reviewed.'; 
            }
            echo json_encode($send_data);
        
    }
    public function delete_file(Request $request){
        
        AnnouncementDocument::where('announcement_id',$request->announcement_id)->update(['link' => '']); 
        if($request->link!='') {
            $image_path = public_path("images/announcements/{$request->link}");
            if (File::exists($image_path)) {
                unlink($image_path);
            }
        }
    }

    public function approval(Request $request) {
       
        $data['approved_by'] = Auth::user()->id;
        $data['approved_at'] = date('Y-m-d H:i:s');
        
        AnnouncementApproval::where('announcement_id',$request->announcement_id)->update($data);
        $ans_data = Announcement::where('id',$request->announcement_id)->get()->first();
       
        $send_param = array('id'=>$request->announcement_id,'title'=>$ans_data->title,'module_type'=>'announcement');

        $this->sendCoursePublishNotification($send_param);
            
    }
    /**
     * send push notification to user announcement approve
     *
     * @method sendCoursePublishNotification
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendCoursePublishNotification($params){

        $user_data = User::where('status','1')->whereIn('role_id',[6,7])->get();
        $title = 'Announcement';
        if ($user_data->count()>0) {
            
            foreach ($user_data as $key => $value) {
                
                $send_status = 2;

                $message = $params['title'].' Announcement Created';
                
                $device_id = $value->device_id;            

                $response = NotificationHelper::sendNotification($message, $device_id, $title);
                $result = json_decode($response);
                
                if (is_object($result) && property_exists($result, 'success') && $result->success) {
                    $send_status = 1;
                }
                
                $data = array(
                    'module_id'=>$params['id'],
                    'user_id'=>$value->id,
                    'icon'=>Null,
                    'module_type'=>$params['module_type'],
                    'message'=>$message,
                    'is_send'=>$send_status
                    );
                
                UserNotification::create($data);
            }
        }
        
    }


}
