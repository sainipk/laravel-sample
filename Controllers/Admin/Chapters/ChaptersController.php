<?php

namespace App\Http\Controllers\Admin\Chapters;

use App\Http\Controllers\Controller;
use App\Model\Permission;
use App\User;
use App\Model\Course;
use App\Model\Chapter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class ChaptersController extends Controller{

    protected $route_base = "admin.chapters.chapters";
    
    public function __construct() {

        parent::__construct();

        /**
         * @desc set controller name
         */
        $this->controller = "Chapters";

        /**
         * @desc set page title
         */
        $this->title = "Chapters";

        $this->model = Chapter::class;

        $this->_data["breadcrumb"] = [
            "Chapters" => route("admin.chapters.index"),
            $this->controller => route($this->routes["index"]),
        ];
    }

    public function index(Request $request) {
        $this->_data['params'] = $request->all();
        $this->_data['limit'] = $this->limit = $request->get('perpage') ? $request->get('perpage') : $this->limit;
        $this->_data['search'] = $request;
        $this->listing($request);
        return view(load_view(), $this->_data);
    }

    /**
     * @method listing
     * @desc prepare data to send view
     * @param Request $request
     * @return array
     */
    public function listing(Request $request) {
        $filter_data = $request->all();
        $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;

        $query = Chapter::with(["userDetail"]);

        // add filter in records
        searching_string($query, $request->form);

        // sort records
        db_sort($query, $request);
        $data = $query->paginate($limit);

        $this->_data['data'] = $data;
        
        return $this->_data;
    }

    /**
     * Show Detail Form
     *
     * @method show
     * @param Request $request, $id
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
     public function show(Request $request, $id){
         
        try {
            $this->_data["breadcrumb"]["Show"] =  "javascript:void(0)";
            $this->_data["data"] = Chapter::with(["userDetail"])
                                    ->where('id', $id)
                                    ->first();                   
            return view(load_view(), $this->_data); 
        } catch(\Illuminate\Database\QueryException $ex) {
            return redirect()->back()->with(['message' => $ex->getMessage(), 'alert-class' => 'alert-danger']);
        }
    }

    /**
     * show create form
     *
     * @method create
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {
        $this->_data["breadcrumb"]["Create"] =  "javascript:void(0)";
        $this->_data["courses"] = Course::CourseList();
        return view(load_view(), $this->_data);
    }

    /**
     * store new records
     *
     * @method store
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) {
        $request->validate(Chapter::rules(), Chapter::messages());
        $data = $request->all();
        
        $data['created_by'] = Auth::user()->id;
        $store = Chapter::create($data);
       
        if($store) {
            $request->session()->flash('success', $this->success_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route($this->routes["index"]));
    }

    /**
     * show edit form
     * @method edit
     * @param Chapters $chapter
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Chapter $chapters) {
        $this->_data["breadcrumb"]["Update"] =  "javascript:void(0)";
        $this->_data["data"] = $chapters;
        $this->_data["courses"] = courses();   
        return view(load_view(), $this->_data);
    }

    /**
     * update information
     *
     * @method update
     * @param Request $request
     * @param Chapters $chapters
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Chapter $chapters) {
        $request->validate(Chapter::rules($chapters->id));
        $data = $request->except("_token");
        $udpate = $chapters->update($data);

        if($udpate) {
            $request->session()->flash('success', $this->update_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route($this->routes["index"]));
    }

    /**
     * @method actions
     * @desc apply mass actions on records
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */

    public function actions(Request $request) {
        $ids = $request->get('bulk_ids');
       
        $response = $this->model::mass_action($request->get('action'), $ids);
        if($response['status'] == true){
            $request->session()->flash('success', $response['message']);
        } else {
            $request->session()->flash('error', $response['message']);
        }
        return redirect()->back();
    }
    
}
