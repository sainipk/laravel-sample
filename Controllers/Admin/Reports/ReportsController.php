<?php

namespace App\Http\Controllers\Admin\Reports;

use App\Http\Controllers\Controller;
use App\Model\Permission;
use App\User;
use App\Model\Report;
use App\Model\Course;
use App\Model\Category;
use App\Model\CourseTypes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;

class ReportsController extends Controller{

    protected $route_base = "admin.reports.reports";
    
    public function __construct() {

        parent::__construct();

        /**
         * @desc set controller name
         */
        $this->controller = "Reports";

        /**
         * @desc set page title
         */
        $this->title = "Reports";

        $this->model = Report::class;

        $this->_data["breadcrumb"] = [
            "Reports" => route("admin.reports.index"),
            $this->controller => route($this->routes["index"]),
        ];
    }
    /**
     * @method listing
     * @desc prepare data to send view
     * @param Request $request
     * @return array
     */
    public function index(Request $request) {
        
        $params = $request->all();
        
        $this->_data["courseCategory"] = Category::categoryList();
        $this->_data["coursetypes"] = CourseTypes::CourseTypeList();
        $this->_data['params'] = $request->all();
        $this->_data['limit'] = $this->limit = $request->get('perpage') ? $request->get('perpage') : $this->limit;
        $this->_data['search'] = $request;

        $filter_data = $request->all();
        $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;

        $query = Report::join('courses','courses.id','=','user_courses.course_id')
                        ->join('users','users.id','=','user_courses.user_id')
                        ->join('categories','categories.id','=','courses.category_id')
                        ->join('course_types','course_types.id','=','courses.course_type')
                        ->join('payments','payments.user_course_id','=','user_courses.id');

        /*
        * apply search filters
        */
        if (isset($params['title']) && $params['title']!='') {
            $query->where('courses.title', 'like', '%' . $params['title'] . '%');
        }
        if (isset($params['name']) && $params['name']!='') {
            $query->where('users.name', 'like', '%' . $params['name']. '%');
        }
        if (isset($params['course_type']) && $params['course_type']!='') {
            $query->where('courses.course_type', $params['course_type']);
        }
        if (isset($params['category_id']) && $params['category_id']!='') {
            $query->where('courses.category_id',$params['category_id']);
        }
        if (isset($params['from_date']) && $params['from_date']!='') {
            
            $date = explode(' - ', $params['from_date']);
            
            $end = date('Y-m-d',strtotime($date[1].'+1 day'));
           
            $query->where('user_courses.created_at','>=',$date[0]);
            $query->where('user_courses.created_at','<=',$end);
        }

        /*
        * export to excel
        */
        if (isset($params['export']) && $params['export']=='export') {
            
            $query->select('courses.title', 'categories.title as category_name', 'course_types.title as course_type', 'users.name',  'user_courses.created_at','payments.payment_method', 'payments.amount', 'payments.discount', 'payments.total_amount', 'payments.payment_status');

            $new_data = $query->get()->toArray();
            
            if (!empty($new_data)) {
                foreach ($new_data as $key => $value) {
                    $new_data[$key]['created_at'] =  date('Y-m-d H:i:s',strtotime($value['created_at']));
                }
            }
            return Excel::download(new UsersExport($new_data), time().'_reports.xlsx');

        }else{

            $query->select('user_courses.id', 'user_courses.course_id', 'user_courses.user_id', 'courses.title', 'users.name',  'user_courses.created_at','payments.payment_method', 'payments.amount', 'payments.discount', 'payments.total_amount', 'payments.payment_status', 'categories.title as category_name', 'course_types.title as course_type');

        }
        
        /*
        * get total record for showing total recrod
        */
        $this->_data['total_record'] =$query->count();
        /*
        * get all record to calculate total amount
        */
        $this->_data['total_data'] =$query->get();
        $this->_data['currency'] =icon('currency');

        $total_amount = 0;
        
        if (!empty($this->_data['total_data'])) {
            foreach ($this->_data['total_data'] as $key => $value) {
                
                $total_amount +=$value->total_amount;
            }
        }

        $this->_data['total_amount'] = $total_amount;
        // sort records
        db_sort($query, $request);
        $data = $query->paginate($limit);
        
        $this->_data['data'] = $data;
        
        return view(load_view(), $this->_data);
    }

    /**
     * @method searchCourse
     * @auto complete search
     * @param Request $request
     * @return array
     */
    public function searchCourse(Request $request) {

        $params = $request->all();
        
        $title = $params['title'];
        $data = Report::join('courses','courses.id','=','user_courses.course_id')
                        ->select('courses.title')
                        ->where('courses.title', 'like', '%' . $title . '%')
                        ->groupBy('courses.id')
                        ->get();

        // sort records
        $new_data = array();
        if ($data->count()>0) {
            $new_data = $data->toArray();
        }
        echo json_encode($new_data);
    }
    /**
     * @method searchCourse
     * @auto complete search
     * @param Request $request
     * @return array
     */
    public function searchUser(Request $request) {

        $params = $request->all();
        
        $user_name = $params['user_name'];
        $data = Report::join('users','users.id','=','user_courses.user_id')
                        ->select('users.name')
                        ->where('users.name', 'like', '%' . $user_name . '%')
                        ->groupBy('users.id')
                        ->get();

        // sort records
        $new_data = array();
        if ($data->count()>0) {
            $new_data = $data->toArray();
        }
        echo json_encode($new_data);
    }

    public function export() 
    {

        return Excel::download(new UsersExport, 'ussers.xlsx');
    }
    
}
