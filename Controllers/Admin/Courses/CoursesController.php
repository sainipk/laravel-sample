<?php

namespace App\Http\Controllers\Admin\Courses;

use App\Http\Controllers\Controller;
use App\Model\Permission;
use App\User;
use App\Model\Course;
use App\Model\Chapter;
use App\Model\ChapterDocument;
use App\Model\Category;
use App\Model\CourseActivityLog;
use App\Model\ChapterActivityLog;
use App\Model\CourseVersion;
use App\Model\ChapterVersion;
use App\Model\TestExamSetting;
use App\Model\TestExamSettingVersion;
use App\Model\ActivityLog;
use App\Model\CourseVersionActivityLog;
use App\Model\ChapterVersionActivityLog;
use App\Model\UserNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\File;
use App\Helpers\NotificationHelper;

class CoursesController extends Controller{

    protected $route_base = "admin.courses.courses";
    
    public function __construct() {

        parent::__construct();

        /**
         * @desc set controller name
         */
        $this->controller = "Courses";
        $this->NotificationHelper = NotificationHelper::class; 

        /**
         * @desc set page title
         */
        $this->title = "Courses";

        $this->model = Course::class;

        $this->_data["breadcrumb"] = [
            "Courses" => route("admin.courses.index"),
            $this->controller => route($this->routes["index"]),
        ];
    }
    /**
     * @method listing
     * @desc prepare data to send view
     * @param Request $request
     * @return array
     */
    public function index(Request $request) {

        $this->_data['params'] = $request->all();
        $this->_data['limit'] = $this->limit = $request->get('perpage') ? $request->get('perpage') : $this->limit;
        $this->_data['search'] = $request;
        $this->_data['course_type'] = ['1'=>'Individual','2'=>'SME (Organization)'];
        $this->_data['status'] = ['1'=>'Active','2'=>'Deactive'];
        $this->_data['categories'] = Category::categoryList();
        $this->listing($request);
        return view(load_view(), $this->_data);
    }
    /**
     * @method listing
     * @desc prepare data to send view
     * @param Request $request
     * @return array
     */
    public function listing(Request $request) {
        $filter_data = $request->all();
        $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;

        $query = Course::with(["courseTypeDetail", "courseCategoryDetail", "courseActivityDetail"]);

        // add filter in records
        searching_string($query, $request->form);

        // sort records
        db_sort($query, $request);
        $data = $query->paginate($limit);
        
        foreach ($data as $key => $value) {
            $value->course_has_exam_setting= Course::checkCourseHasExamSetting($value->id);
            $value->course_has_chapters    = Chapter::checkCourseHasChapters($value->id);
            $value->chapter_has_quiz       = Chapter::checkChapterHasQuiz($value->id);
            $value->chapter_review_status  = Chapter::chaptersReviewStatus($value->id);
            $value->chapter_approve_status = Chapter::chaptersApproveStatus($value->id);
            $value->chapter_publish_status = Chapter::chaptersPublishStatus($value->id);
        }
        
        $permissions = \App\Model\UserPermission::getUserPermissions();
        $this->_data['data'] = $data;
        $this->_data['permissions'] = $permissions;
        $this->_data['currency'] = icon('currency');
        
        return $this->_data;
    }
    /**
     * save course detail using ajax
     *
     * @method save_course_detail
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save_course_detail(Request $request){
                
        $data = $request->all();
        $data = $request->except("_token");
        /*
        * course updat code
        */
        if (isset($data['course_create_id']) && $data['course_create_id']!='') {
            
            $data['id'] = $data['course_create_id'];
           
            $res =  Course::course_edit_rules($data,$request);

            if ($res=='') {
               
                $data['slug'] = strtolower(str_replace(' ', '-', $data['title']));
                $data['org_title'] = $data['title'];
                    
                /*if($files = $request->file('banner')){

                    $name = time().'_'.$files->getClientOriginalName(); 
                    $data["banner"] = $name; 
                    $files->move('images/courses/banners',$name);                       
                }
                if($files2 = $request->file('video_link')){

                    $name2 = time().'_'.$files2->getClientOriginalName(); 
                    $data["video_link"] = $name2; 
                    $files2->move('images/courses/video',$name2);                       
                }*/

                //$old_data = Course::select('banner','video_link')->where('id',$data['id'])->first();
                
                unset($data['course_create_id']);
                unset($data['course_id']);
                
                $udpate = Course::where('id',$data['id'])->update($data);

                $browser = ActivityLog::getBrowser();           
                $activity_data = array(
                    'model'=>'App\Model\Course',
                    'model_id'=>$data['id'],
                    'type'=>'UPDATE',
                    'old_data'=>serialize($data),
                    'new_data'=>serialize($data),
                    'action_by'=>Auth::user()->id,
                    'ip'=>$_SERVER['REMOTE_ADDR'],
                    'browser'=>$browser['name'],
                    'platform'=>'Web',
                    'action_at'=>date('y-m-d H:i:s'),
                    );
                ActivityLog::create($activity_data);

                $send_data = array();

                if($udpate) {
                    
                    CourseActivityLog::where('course_id',$data['id'])->update(['updated_at'=>date('Y-m-d H:i:s')]);

                    /*if($files = $request->file('banner')){
                        if ($old_data->banner!='') {
                            $image_path = public_path("images/courses/banners/{$old_data->banner}");

                            if (File::exists($image_path)) {
                                unlink($image_path);
                            }
                        }
                    }
                    if($files2 = $request->file('video_link')){
                        if ($old_data->video_link!='') {
                            $image_path2 = public_path("images/courses/video/{$old_data->video_link}");

                            if (File::exists($image_path2)) {
                                unlink($image_path2);
                            }
                        }
                    }*/

                    $send_data['success'] = true;
                    $send_data['message'] = 'Course detail has been updated successfully.';
                    $send_data['course_create_id'] = $data['id'];            

                } else {

                    $send_data['success'] = false;
                    $send_data['message'] = 'Course could not added.';
                }

            }else{

                $send_data['success'] = false;
                $send_data['message'] = $res;
            }
            echo json_encode($send_data);

        }else{
           /*
           * course created code
           */ 
           $res = Course::course_create_validation($data,$request);
           
           if($res!=''){
            
                $send_data['success'] = false;
                $send_data['message'] = $res;
                echo json_encode($send_data);
                
            }else{

                $data['slug'] = strtolower(str_replace(' ', '-', $data['title']));
                $data['org_title'] = $data['title'];
                /*if($files = $request->file('banner')){

                    $name = time().'_'.$files->getClientOriginalName(); 
                    $data["banner"] = $name; 
                    $files->move('images/courses/banners',$name);                       
                }
                if($files2 = $request->file('video_link')){

                    $name2 = time().'_'.$files2->getClientOriginalName(); 
                    $data["video_link"] = $name2; 
                    $files2->move('images/courses/video',$name2);                       
                }*/
               
                $store = Course::create($data);
                
                $send_data = array();

                if($store) {

                     $version_id = CourseVersion::create(['course_id'=>$store->id,'version_no'=>'V1.0','created_by'=>Auth::user()->id]);
                     
                     Course::where('id',$store->id)->update(['version_no'=>'V1.0','course_version_id'=>$version_id->id]);   

                    CourseActivityLog::create(array('course_id'=>$store->id, 'created_by'=>Auth::user()->id,'created_at'=>date('Y-m-d H:i:s')));

                    $send_data['success'] = true;
                    $send_data['message'] = 'New course has been successfully added.';
                    $send_data['course_create_id'] = $store->id;            

                } else {

                    $send_data['success'] = false;
                    $send_data['message'] = 'Course could not added.';
                }
                echo json_encode($send_data);
            } 
        }        
    }
    /**
     * @method save_chapter_detail
     * @desc add multiple chapter of course
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save_chapter_detail(Request $request){
               
        $data = $request->all();
        $data = $request->except("_token");
        /*
        * chapter create code
        */
        $res =  Chapter::chapter_create_validation($data,$request);
        if (isset($data['course_id']) && $data['course_id'] != '') {
            if($res == ''){
                ChapterDocument::join('chapters','chapter_documents.chapter_id','=','chapters.id')->where('chapters.course_id',$data['course_id'])->forceDelete();
                Chapter::where('course_id',$data['course_id'])->forceDelete();
               $course_detail = Course::getCourseDetailView($data['course_id']);
               foreach ($data['title'] as $key => $value) {
                    $new_data = array();
                    $new_data['title'] = $value;
                    $new_data['course_id'] = $data['course_id'];
                    $new_data['course_version_id'] = $course_detail->course_version_id;
                    $new_data['description'] = $data['description'][$key];
                    $new_data['status'] = current(array_slice($data['status'],$key,1));
                    /*
                    * create chapter
                    */
                    $store = Chapter::create($new_data);
                    /*
                        $browser = ActivityLog::getBrowser();           
                        $activity_data = array(
                            'model'=>'App\Model\Chapter',
                            'model_id'=>$store->id,
                            'type'=>'CREATE',
                            'old_data'=>serialize([]),
                            'new_data'=>serialize($new_data),
                            'action_by'=>Auth::user()->id,
                            'ip'=>$_SERVER['REMOTE_ADDR'],
                            'browser'=>$browser['name'],
                            'platform'=>'Web',
                            'action_at'=>date('y-m-d H:i:s'),
                            );
                        ActivityLog::create($activity_data);
                    */
                    /* chapter unit pdf and video */
                    if($files = $request->file('link_pdf')){
                        $name = time().'_'.$files[$key]->getClientOriginalName();
                        $files[$key]->move('images/chapters/pdf',$name);                       
                    }
                    if($files2 = $request->file('link_video')){
                        $name2 = time().'_'.$files2[$key]->getClientOriginalName();
                        $files2[$key]->move('images/chapters/video',$name2);                       
                    }
                    if ($store) {
                        ChapterActivityLog::create(array('chapter_id' => $store->id, 'created_by' => Auth::id(),'created_at' => date('Y-m-d H:i:s')));
                        ChapterDocument::create(['chapter_id' => $store->id, 'link' => $name, 'document_type' => 'PDF','status' => current(array_slice($data['status'], $key, 1))]);
                        ChapterDocument::create(['chapter_id' => $store->id, 'link' => $name2, 'document_type' => 'Video','status' => current(array_slice($data['status'], $key, 1))]);
                    }
               }
                $send_data['success'] = true;
                $send_data['message'] = 'Chapter has been successfully added.'; 
            } else {
                $send_data['success'] = false;
                $send_data['message'] = $res;
            }
        } else {
             $send_data['success'] = false;
             $send_data['message'] = 'Please add course detail first.';
        }
        echo json_encode($send_data);
    }
    /**
     * save course detail using ajax
     *
     * @method save_course_detail
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update_course_detail(Request $request){
                
        $data = $request->all();
        $data = $request->except("_token");
        //dd($data);
        /*
        * course updat code
        */
        $data['id'] = $data['course_id'];
        $data['course_create_id'] = $data['course_id'];
        $res =  Course::course_edit_rules($data,$request);

        if ($res=='') {
           
            $data['slug'] = strtolower(str_replace(' ', '-', $data['title']));
            $data['org_title'] = $data['title'];
            /*$old_data = Course::select('banner','video_link')->where('id',$data['id'])->first();

            if($files = $request->file('banner')){

                $name = time().'_'.$files->getClientOriginalName(); 
                $data["banner"] = $name; 
                $files->move('images/courses/banners',$name);                       
            }
            if($files2 = $request->file('video_link')){

                $name2 = time().'_'.$files2->getClientOriginalName(); 
                $data["video_link"] = $name2; 
                $files2->move('images/courses/video',$name2);                       
            }

            $old_data = Course::select('banner','video_link')->where('id',$data['id'])->first();*/
            
            unset($data['course_id']);
            unset($data['course_create_id']);
            
            $udpate = Course::where('id',$data['id'])->update($data);
            
           
            $browser = ActivityLog::getBrowser();           
            $activity_data = array(
                'model'=>'App\Model\Course',
                'model_id'=>$data['id'],
                'type'=>'UPDATE',
                'old_data'=>serialize($data),
                'new_data'=>serialize($data),
                'action_by'=>Auth::user()->id,
                'ip'=>$_SERVER['REMOTE_ADDR'],
                'browser'=>$browser['name'],
                'platform'=>'Web',
                'action_at'=>date('y-m-d H:i:s'),
                );
            ActivityLog::create($activity_data);

            $send_data = array();

            if($udpate) {
                
                CourseActivityLog::where('course_id',$data['id'])->update(['updated_at'=>date('Y-m-d H:i:s'), "updated_by"=>Auth::user()->id]);

                /*if($files = $request->file('banner')){
                    if ($old_data->banner!='') {
                        $image_path = public_path("images/courses/banners/{$old_data->banner}");

                        if (File::exists($image_path)) {
                            unlink($image_path);
                        }
                    }
                }
                if($files2 = $request->file('video_link')){
                    if ($old_data->video_link!='') {
                        $image_path2 = public_path("images/courses/video/{$old_data->video_link}");

                        if (File::exists($image_path2)) {
                            unlink($image_path2);
                        }
                    }
                }*/

                $send_data['success'] = true;
                $send_data['message'] = 'Course detail has been successfully updated.';
                $send_data['course_create_id'] = $data['id'];     
                       

            } else {

                $send_data['success'] = false;
                $send_data['message'] = 'Course could not added.';
            }

        }else{

            $send_data['success'] = false;
            $send_data['message'] = $res;
        }
        echo json_encode($send_data);
    }
    /**
     * save course detail using ajax
     *
     * @method save_course_detail
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function version_update_course_detail(Request $request){
                
        $data = $request->all();
        $data = $request->except("_token");
        //dd($data);
        /*
        * course updat code
        */
        $data['id'] = $data['course_id'];
        $data['course_create_id'] = $data['course_id'];
        $res =  Course::version_update_course_detail_rule($data);

        if ($res=='') {
            
            unset($data['course_id']);
            unset($data['course_create_id']);
            
            $udpate = Course::where('id',$data['id'])->update($data);

            $browser = ActivityLog::getBrowser();           
            $activity_data = array(
                'model'=>'App\Model\Course',
                'model_id'=>$data['id'],
                'type'=>'UPDATE Version',
                'old_data'=>serialize($data),
                'new_data'=>serialize($data),
                'action_by'=>Auth::user()->id,
                'ip'=>$_SERVER['REMOTE_ADDR'],
                'browser'=>$browser['name'],
                'platform'=>'Web',
                'action_at'=>date('y-m-d H:i:s'),
                );
            ActivityLog::create($activity_data);

            $send_data = array();

            if($udpate) {
                
                CourseActivityLog::where('course_id',$data['id'])->update(['updated_at'=>date('Y-m-d H:i:s'), "updated_by"=>Auth::user()->id]);

                $send_data['success'] = true;
                $send_data['message'] = 'Course detail has been successfully updated.';
                $send_data['course_create_id'] = $data['id'];            

            } else {

                $send_data['success'] = false;
                $send_data['message'] = 'Course could not added.';
            }

        }else{

            $send_data['success'] = false;
            $send_data['message'] = $res;
        }
        echo json_encode($send_data);
    }
    /**
     * @method save_chapter_detail
     * @desc add multiple chapter of course
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update_chapter_detail(Request $request){
                
        $data = $request->all();
        $data = $request->except("_token");
        //dd($data );
        /*
        * chapter create code
        */

        $res =  Chapter::chapter_edit_validation($data,$request);

        if (isset($data['course_id']) && $data['course_id']!='') {            
            
            if($res ==''){
               
               $course_detail = Course::getCourseDetail($data['course_id']);
                   
               foreach ($data['title'] as $key => $value) {
                    
                    if ($data['chapter_id'][$key]=='') {
                    
                    
                        $new_data = array();
                        
                        $new_data['title'] = $value;
                        $new_data['course_id'] = $data['course_id'];
                        $new_data['course_version_id'] = $course_detail->course_version_id;
                        $new_data['description'] = $data['description'][$key];
                        $new_data['status'] = current(array_slice($data['status'],$key,1));
                          
                        /*
                        * create chapter
                        */
                        $store = Chapter::create($new_data);
                        
                        /*$browser = ActivityLog::getBrowser();           
                        $activity_data = array(
                            'model'=>'App\Model\Chapter',
                            'model_id'=>$store->id,
                            'type'=>'CREATE',
                            'old_data'=>serialize([]),
                            'new_data'=>serialize($new_data),
                            'action_by'=>Auth::user()->id,
                            'ip'=>$_SERVER['REMOTE_ADDR'],
                            'browser'=>$browser['name'],
                            'platform'=>'Web',
                            'action_at'=>date('y-m-d H:i:s'),
                            );
                        ActivityLog::create($activity_data);*/

                        /*
                        * chapter unit pdf and video
                        */
                        if($files = $request->file('link_pdf')){

                            $name = time().'_'.$files[$key]->getClientOriginalName();
                            
                            $files[$key]->move('images/chapters/pdf',$name);                       
                        }
                        if($files2 = $request->file('link_video')){

                            $name2 = time().'_'.$files2[$key]->getClientOriginalName(); 
                            
                            $files2[$key]->move('images/chapters/video',$name2);                       
                        }

                        if ($store) {

                            ChapterActivityLog::create(array('chapter_id'=>$store->id, 'created_by'=>Auth::id(),'created_at'=>date('Y-m-d H:i:s')));

                            ChapterDocument::create(['chapter_id'=>$store->id, 'link'=>$name, 'document_type'=>'PDF','status'=>current(array_slice($data['status'],$key,1))]);
                            ChapterDocument::create(['chapter_id'=>$store->id, 'link'=>$name2, 'document_type'=>'Video','status'=>current(array_slice($data['status'],$key,1))]);
                        }

                    }else{

                        $new_data = array();                      

                        $new_data['title'] = $value;
                        $new_data['description'] = $data['description'][$key];
                        $new_data['status'] = current(array_slice($data['status'],$key,1)); 
                          
                        /*
                        * create chapter
                        */
                        $store = Chapter::where('id',$data['chapter_id'][$key])->update($new_data);
                        /*$browser = ActivityLog::getBrowser();           
                        $activity_data = array(
                            'model'=>'App\Model\Chapter',
                            'model_id'=>$data['chapter_id'][$key],
                            'type'=>'UPDATE',
                            'old_data'=>serialize($new_data),
                            'new_data'=>serialize($new_data),
                            'action_by'=>Auth::user()->id,
                            'ip'=>$_SERVER['REMOTE_ADDR'],
                            'browser'=>$browser['name'],
                            'platform'=>'Web',
                            'action_at'=>date('y-m-d H:i:s'),
                            );
                        ActivityLog::create($activity_data);*/

                        /*
                        * chapter unit pdf and video
                        */
                        $name = '';
                        $name2 = '';
                        
                        if($files = $request->file('link_pdf')){

                            if (array_key_exists($key, $files)) {

                                $name = time().'_'.$files[$key]->getClientOriginalName();
                                
                                $files[$key]->move('images/chapters/pdf',$name);
                            }
                        }
                        if($files2 = $request->file('link_video')){

                            if (array_key_exists($key, $files2)) {

                                $name2 = time().'_'.$files2[$key]->getClientOriginalName(); 
                            
                                $files2[$key]->move('images/chapters/video',$name2); 
                            }
                                                  
                        }

                        if ($store) {
                           
                           $update_activity_log_status =  ChapterActivityLog::where('chapter_id',$data['chapter_id'][$key])->update(['updated_by'=>Auth::user()->id,'updated_at'=>date('Y-m-d H:i:s')]);

                            if ($name=='') {

                                ChapterDocument::where('chapter_id',$data['chapter_id'][$key])->where('document_type','pdf')->update(['updated_at'=>date('Y-m-d H:i:s'),'status'=>current(array_slice($data['status'],$key,1))]);

                            }else{
                               

                                $old_pdf_data = ChapterDocument::select('link')->where('chapter_id',$data['chapter_id'][$key])->where('document_type','pdf')->first();

                                $update_pdf_status = ChapterDocument::where('chapter_id',$data['chapter_id'][$key])->where('document_type','pdf')->update(['updated_at'=>date('Y-m-d H:i:s'),'link'=>$name, 'status'=>current(array_slice($data['status'],$key,1))]); 

                                if ($update_pdf_status) {

                                   if ($old_pdf_data->link!='') {
                                        $image_path = public_path("images/chapters/pdf/{$old_pdf_data->link}");

                                        if (File::exists($image_path)) {
                                            unlink($image_path);
                                        }
                                    }
                               } 
                            }

                            if ($name2=='') {

                                ChapterDocument::where('chapter_id',$data['chapter_id'][$key])->where('document_type','video')->update(['updated_at'=>date('Y-m-d H:i:s'),'status'=>current(array_slice($data['status'],$key,1))]);

                            }else{
                                
                                $old_video_data = ChapterDocument::select('link')->where('chapter_id',$data['chapter_id'][$key])->where('document_type','video')->first();

                                $update_video_status = ChapterDocument::where('chapter_id',$data['chapter_id'][$key])->where('document_type','video')->update(['updated_at'=>date('Y-m-d H:i:s'),'link'=>$name2, 'status'=>current(array_slice($data['status'],$key,1))]);

                                if ($update_video_status) {
                                   if ($old_video_data->link!='') {
                                        $image_path2 = public_path("images/chapters/video/{$old_video_data->link}");

                                        if (File::exists($image_path2)) {
                                            unlink($image_path2);
                                        }
                                    }
                               }
                            }
                            
                            
                        }
                    }
                    
               }

                $send_data['success'] = true;
                $send_data['message'] = 'Chapter has been successfully updated.'; 
            }else{
                $send_data['success'] = false;
                $send_data['message'] = $res;
            }

        }else{
             $send_data['success'] = false;
             $send_data['message'] = 'Please add course detail first.';
        }
        echo json_encode($send_data);
        
    }
    /**
     * @method save_chapter_detail
     * @desc add multiple chapter of course new version course
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function version_update_chapter_detail(Request $request){
                
        $data = $request->all();
        $data = $request->except("_token");
        //dd($data );
        /*
        * chapter create code
        */

        $res =  Chapter::version_update_chapter_detail_validation($data,$request);

        if (isset($data['course_id']) && $data['course_id']!='') {            
            
            if($res ==''){
               
                $course_detail = Course::getCourseDetail($data['course_id']);
             
               foreach ($data['title'] as $key => $value) {
                    
                    if ($data['chapter_id'][$key]=='') {
                    
                    
                        $new_data = array();
                        
                        $new_data['title'] = $value;
                        $new_data['course_id'] = $data['course_id'];
                        $new_data['course_version_id'] = $course_detail->course_version_id;
                        $new_data['description'] = $data['description'][$key];
                        $new_data['status'] = current(array_slice($data['status'],$key,1));
                          
                        /*
                        * create chapter
                        */
                        $store = Chapter::create($new_data);
                        /*
                        * chapter unit pdf and video
                        */
                        if($files = $request->file('link_pdf')){

                            $name = time().'_'.$files[$key]->getClientOriginalName();
                            
                            $files[$key]->move('images/chapters/pdf',$name);                       
                        }
                        if($files2 = $request->file('link_video')){

                            $name2 = time().'_'.$files2[$key]->getClientOriginalName(); 
                            
                            $files2[$key]->move('images/chapters/video',$name2);                       
                        }

                        if ($store) {

                            ChapterActivityLog::create(array('chapter_id'=>$store->id, 'created_by'=>Auth::id(),'created_at'=>date('Y-m-d H:i:s')));

                            ChapterDocument::create(['chapter_id'=>$store->id, 'link'=>$name, 'document_type'=>'pdf','status'=>current(array_slice($data['status'],$key,1))]);
                            ChapterDocument::create(['chapter_id'=>$store->id, 'link'=>$name2, 'document_type'=>'video','status'=>current(array_slice($data['status'],$key,1))]);
                        }

                    }else{

                        $new_data = array();

                        $chapter_status = Chapter::checkChepterIsCreatedAfterCoursePublish($data['chapter_id'][$key]);

                        if (!$chapter_status) {

                            $new_data['status'] = current(array_slice($data['status'],$key,1));

                        }else{

                            $new_data['title'] = $value;
                            $new_data['description'] = $data['description'][$key];
                            $new_data['status'] = current(array_slice($data['status'],$key,1)); 
                        }
                        
                          
                        /*
                        * create chapter
                        */
                        $store = Chapter::where('id',$data['chapter_id'][$key])->update($new_data);

                        /*
                        * chapter unit pdf and video
                        */
                        $name = '';
                        $name2 = '';

                        if ($chapter_status) {
                        
                            if($files = $request->file('link_pdf')){

                                if (array_key_exists($key, $files)) {

                                    $name = time().'_'.$files[$key]->getClientOriginalName();
                                    
                                    $files[$key]->move('images/chapters/pdf',$name);
                                }
                            }
                            if($files2 = $request->file('link_video')){

                                if (array_key_exists($key, $files2)) {

                                    $name2 = time().'_'.$files2[$key]->getClientOriginalName(); 
                                
                                    $files2[$key]->move('images/chapters/video',$name2); 
                                }
                                                      
                            }
                        }

                        if ($store) {
                           
                           $update_activity_log_status =  ChapterActivityLog::where('chapter_id',$data['chapter_id'][$key])->update(['updated_by'=>Auth::user()->id,'updated_at'=>date('Y-m-d H:i:s')]);

                            if ($name=='') {

                                ChapterDocument::where('chapter_id',$data['chapter_id'][$key])->where('document_type','pdf')->update(['updated_at'=>date('Y-m-d H:i:s'),'status'=>current(array_slice($data['status'],$key,1))]);

                            }else{

                                if ($chapter_status) {

                                    $old_pdf_data = ChapterDocument::select('link')->where('chapter_id',$data['chapter_id'][$key])->where('document_type','pdf')->first();

                                    $update_pdf_status = ChapterDocument::where('chapter_id',$data['chapter_id'][$key])->where('document_type','pdf')->update(['updated_at'=>date('Y-m-d H:i:s'),'link'=>$name, 'status'=>current(array_slice($data['status'],$key,1))]); 

                                    if ($update_pdf_status) {

                                       if ($old_pdf_data->link!='') {
                                            $image_path = public_path("images/chapters/pdf/{$old_pdf_data->link}");

                                            if (File::exists($image_path)) {
                                                unlink($image_path);
                                            }
                                        }
                                   } 
                               }  
                            }

                            if ($name2=='') {

                                ChapterDocument::where('chapter_id',$data['chapter_id'][$key])->where('document_type','video')->update(['updated_at'=>date('Y-m-d H:i:s'),'status'=>current(array_slice($data['status'],$key,1))]);

                            }else{

                                if ($chapter_status) {
                                    $old_video_data = ChapterDocument::select('link')->where('chapter_id',$data['chapter_id'][$key])->where('document_type','video')->first();

                                    $update_video_status = ChapterDocument::where('chapter_id',$data['chapter_id'][$key])->where('document_type','video')->update(['updated_at'=>date('Y-m-d H:i:s'),'link'=>$name2, 'status'=>current(array_slice($data['status'],$key,1))]);

                                    if ($update_video_status) {
                                       if ($old_video_data->link!='') {
                                            $image_path2 = public_path("images/chapters/pdf/{$old_video_data->link}");

                                            if (File::exists($image_path2)) {
                                                unlink($image_path2);
                                            }
                                        }
                                   }
                               }   
                            }
                            
                            
                        }
                    }
                    
               }

                $send_data['success'] = true;
                $send_data['message'] = 'Chapter has been successfully updated.'; 
            }else{
                $send_data['success'] = false;
                $send_data['message'] = $res;
            }

        }else{
             $send_data['success'] = false;
             $send_data['message'] = 'Please add course detail first.';
        }
        echo json_encode($send_data);
        
    }    
    /**
     * @method getCourseChapter
     * @desc get all course chapter
     * @param Request $request
     * @return array
     */
    public function getCourseChapter(Request $request) {
        $filter_data = $request->all();
        
        $result = Chapter::where('course_id',$request->course_id)->where('chapter_removed','2')->pluck('title','id');

        $this->_data['data'] = $result;
        
        return $this->_data;
    }

    /**
     * Show Detail Form
     *
     * @method show
     * @param Request $request, $id
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
     public function show(Request $request, $id){
         
        try {
            $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;
            $this->_data["breadcrumb"]["Show"] =  "javascript:void(0)";
            $this->_data["data"] = Course::with(["courseTypeDetail", "courseCategoryDetail", "courseCreatedDetail", "courseUpdatedDetail", "courseReviewedDetail", "courseApprovedDetail", "coursePublishedDetail", "courseActivityDetail"])
                                    ->where('id', $id)
                                    ->first(); 
            //dd($this->_data["data"]);
            $this->_data["course_has_chapters"]  = Chapter::checkCourseHasChapters($id);                        
            $this->_data["coursetypes"]          = course_types();  
            $this->_data["courseCategory"]       = Category::categoryList(); 
            $this->_data["courseChapters"]       = Course::courseChaptersListWithDocumentsView($id);
            $this->_data["chapterReviewStatus"]  = Chapter::chaptersReviewStatus($id);
            $this->_data["chapterApproveStatus"] = Chapter::chaptersApproveStatus($id);
            $this->_data["chapterPublishStatus"] = Chapter::chaptersPublishStatus($id);
            $this->_data["allCourseTest"]        = Course::getAllCourseTest($id);
            $this->_data["activities"]           = ActivityLog::where('model', 'App\Model\Course')->where('model_id', $id)->paginate($limit);
            $this->_data["chapter_activities"]           = ActivityLog::where('model', 'App\Model\Chapter')->paginate($limit);
            $this->_data['currency'] = icon('currency');
            $permissions = \App\Model\UserPermission::getUserPermissions();       
            $this->_data['permissions'] = $permissions;
            //dd($this->_data);

            return view(load_view(), $this->_data); 
        } catch(\Illuminate\Database\QueryException $ex) {
            dd($ex);
            return redirect()->back()->with(['message' => $ex->getMessage(), 'alert-class' => 'alert-danger']);
        }
    }
    /**
     * show create form
     *
     * @method create
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {
        $this->_data["breadcrumb"]["Create"] =  "javascript:void(0)";
        $this->_data["coursetypes"] = course_types();
        $this->_data["courseCategory"] = Category::categoryList();
        $this->_data['currency'] = icon('currency');
        return view(load_view(), $this->_data);
    }

    /**
     * store new records
     *
     * @method store
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) {
        $request->validate(Course::course_create_rules(), Course::messages());
        $data = $request->all();
        
        $data['created_by'] = Auth::user()->id;
        $store = Course::create($data);
       
        if($store) {

            $request->session()->flash('success', $this->success_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route($this->routes["index"]));
    }

    /**
     * show edit form
     * @method edit
     * @param Courses $course
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request, Course $course) {
        $this->_data["breadcrumb"]["Update"] =  "javascript:void(0)";
        $this->_data["data"] = $course;
        $this->_data["coursetypes"] = course_types();  
        $this->_data["courseCategory"] = Category::categoryList(); 
        $this->_data["courseChapters"] = Course::courseChaptersListWithDocumentsView($course->id);
        $this->_data['currency'] = icon('currency');
        /*
        * check whether course was published previously.
        * if yes than user can not edit course and previously added chapters
        */
        if(Course::previousPublishStatus($course->id)){
            
            $request->session()->flash('error', 'Course can not be edit after publish.');
            return redirect()->to(route($this->routes["index"]));
        }

        //dd($this->_data["courseChapters"]);
        return view(load_view(), $this->_data);
    }

    /**
     * update information
     *
     * @method update
     * @param Request $request
     * @param Courses $courses
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Course $course) {
        $request->validate(Course::rules($course->id));
        $data = $request->except("_token");
        $udpate = $course->update($data);

        if($udpate) {
            $request->session()->flash('success', $this->update_response);
        } else {
            $request->session()->flash('error', $this->error_response);
        }
        return redirect()->to(route($this->routes["index"]));
    }

    /**
     * @method actions
     * @desc apply mass actions on records
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */

    public function actions(Request $request) {
        $ids = $request->get('bulk_ids');
       
        $response = $this->model::mass_action($request->get('action'), $ids);
        if($response['status'] == true){
            $request->session()->flash('success', $response['message']);
        } else {
            $request->session()->flash('error', $response['message']);
        }
        return redirect()->back();
    }
    /**
     * @method remove_chapter
     * @desc soft delete chapter from db
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function remove_chapter(Request $request){
        
        $data = $request->all();

        $status =  Chapter::removeChapter($data['chapter_id'], Auth::user()->id);
        
        if ($status) {
            $send_data['success'] = true;
            $send_data['message'] = 'Chapter has been removed successfully.'; 
        }else{
            $send_data['success'] = false;
            $send_data['message'] = 'Chapter courld not be removed.'; 
        }
        echo json_encode($send_data);
    }
     /**
     * @method course_review
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function course_review(Request $request){
        
        $data = $request->all();        

        $course_id = $data['course_id'];
        
        $chapter_data = Chapter::join('chapter_activity_log','chapter_activity_log.chapter_id','=','chapters.id')
        ->whereNull('chapter_activity_log.reviewed_by')
        ->whereNull('chapter_activity_log.deleted_at')
        ->where('chapters.course_id',$course_id)
        ->where('chapters.status',1)
        ->where('chapters.chapter_removed',2)
        ->select('chapters.id')->get();
        
        if ($chapter_data->count()>0) {

            $send_data['success'] = false;
            $send_data['message'] = 'Please review the chapters before review the course.'; 

        }else{

            $chapter_active = Chapter::where('status','1')->where('course_id',$course_id)->where('chapters.chapter_removed',2)->get();
            $status = 0;
            if ($chapter_active->count()>0) {
               
                Course::where('id',$course_id)->update(['status'=>'1']);

                $browser = ActivityLog::getBrowser();           
                $activity_data = array(
                    'model'=>'App\Model\Course',
                    'model_id'=>$course_id,
                    'type'=>'Course Reviewed',
                    'old_data'=>serialize(['status'=>'1']),
                    'new_data'=>serialize(['status'=>'1']),
                    'action_by'=>Auth::user()->id,
                    'ip'=>$_SERVER['REMOTE_ADDR'],
                    'browser'=>$browser['name'],
                    'platform'=>'Web',
                    'action_at'=>date('y-m-d H:i:s'),
                    );
                ActivityLog::create($activity_data);

                $status = CourseActivityLog::where('course_id',$course_id)->update(['reviewed_by'=>Auth::user()->id,'reviewed_at'=>date('Y-m-d H:i:s')]); 
            }
            
            if($chapter_active->count()<1){
                $send_data['success'] = false;
                $send_data['message'] = 'Course does not have any chapter.'; 
            }else if ($status) {
                $send_data['success'] = true;
                $send_data['message'] = 'Course has been successfully reviewed.'; 
            }else{
                $send_data['success'] = false;
                $send_data['message'] = 'Course could not be reviewed.'; 
            }
            
        }
        echo json_encode($send_data);
    }
    /**
     * @method course_approve
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function course_approve(Request $request){
        
        $data = $request->all();        

        $course_id = $data['course_id'];
        
        $chapter_data = Chapter::join('chapter_activity_log','chapter_activity_log.chapter_id','=','chapters.id')
        ->whereNull('chapter_activity_log.approved_by')
        ->whereNull('chapter_activity_log.approved_at')
        ->where('chapters.course_id',$course_id)
        ->select('chapters.id')->get();
        
        if ($chapter_data->count()>0) {

            $send_data['success'] = false;
            $send_data['message'] = 'Please approve the chapters before approve the course.'; 

        }else{

            Course::where('id',$course_id)->update(['status'=>'1']);
            
            $browser = ActivityLog::getBrowser();           
            $activity_data = array(
                'model'=>'App\Model\Course',
                'model_id'=>$course_id,
                'type'=>'Course Approved',
                'old_data'=>serialize(['status'=>'1']),
                'new_data'=>serialize(['status'=>'1']),
                'action_by'=>Auth::user()->id,
                'ip'=>$_SERVER['REMOTE_ADDR'],
                'browser'=>$browser['name'],
                'platform'=>'Web',
                'action_at'=>date('y-m-d H:i:s'),
                );
            ActivityLog::create($activity_data);

            $status = CourseActivityLog::where('course_id',$course_id)->update(['approved_by'=>Auth::user()->id,'approved_at'=>date('Y-m-d H:i:s')]); 
            
            if ($status) {
                $send_data['success'] = true;
                $send_data['message'] = 'Course has been successfully approved.'; 
            }else{
                $send_data['success'] = true;
                $send_data['message'] = 'Course could not be approved.'; 
            }
            
        }
        echo json_encode($send_data);
    }
    /**
     * @method course_publish
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function course_publish(Request $request){
        
        $data = $request->all();        

        $course_id = $data['course_id'];

        $error_message = $this->validateCourse($course_id);
        
        if ($error_message!=false) {

            $send_data['success'] = false;
            $send_data['message'] = $error_message; 

        }else{

            Course::where('id',$course_id)->update(['status'=>'1']);

            $browser = ActivityLog::getBrowser();           
            $activity_data = array(
                'model'=>'App\Model\Course',
                'model_id'=>$course_id,
                'type'=>'Course Published',
                'old_data'=>serialize(['status'=>'1']),
                'new_data'=>serialize(['status'=>'1']),
                'action_by'=>Auth::user()->id,
                'ip'=>$_SERVER['REMOTE_ADDR'],
                'browser'=>$browser['name'],
                'platform'=>'Web',
                'action_at'=>date('y-m-d H:i:s'),
                );
            ActivityLog::create($activity_data);

            $status = CourseActivityLog::where('course_id',$course_id)->update(['published_by'=>Auth::user()->id,'published_at'=>date('Y-m-d H:i:s')]);

            $course_data = Course::where('id',$course_id)->select('id','course_version_id','title','version_no')->first();
            
            Chapter::manageCourseVersionInChapters($course_id, $course_data->course_version_id);
            TestExamSetting::manageCourseVersionInExams($course_id,$course_data->course_version_id);

            if (strtolower($course_data->version_no)=='v1.0') {
               $module_type = 'course_publish';
            }else{
                $module_type = 'course_version_launch';
            }
           
            $send_param = array('id'=>$course_data->id,'title'=>$course_data->title,'module_type'=>$module_type);

            $this->sendCoursePublishNotification($send_param);
        
            
            if ($status) {
                $send_data['success'] = true;
                $send_data['message'] = 'Course has been successfully published.'; 
            }else{
                $send_data['success'] = false;
                $send_data['message'] = 'Course could not be published.'; 
            }
        }
        echo json_encode($send_data);
    }
    /**
     * @method chapter_review
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function chapter_review(Request $request){
        
        $data = $request->all();        

        $course_id = $data['course_id'];
        $all_chapter_id = $data['all_chapter_id'];
        
        if ($all_chapter_id!='') {

            $chapter_id_array = array_filter(explode(',', $all_chapter_id));
            
        }else{

            $chapter_id_array = Chapter::where('course_id',$course_id)->where('chapter_removed','2')->pluck('id');

        }
        
        $error_message = $this->validateChapters($course_id, $chapter_id_array);
        $status = false;

        if ($error_message==false) {
            
            if (!empty($chapter_id_array)) {

               foreach ($chapter_id_array as $key => $value) {
                   
                   Chapter::where('id',$value)->whereNull('deleted_at')->update(['status'=>'1']);

                  /* $browser = ActivityLog::getBrowser();           
                    $activity_data = array(
                        'model'=>'App\Model\Chapter',
                        'model_id'=>$value,
                        'type'=>'Chapter Reviewed',
                        'old_data'=>serialize(['status'=>'1']),
                        'new_data'=>serialize(['status'=>'1']),
                        'action_by'=>Auth::user()->id,
                        'ip'=>$_SERVER['REMOTE_ADDR'],
                        'browser'=>$browser['name'],
                        'platform'=>'Web',
                        'action_at'=>date('y-m-d H:i:s'),
                        );
                    ActivityLog::create($activity_data); */

                    $status = ChapterActivityLog::where('chapter_id',$value)->update(['reviewed_by'=>Auth::user()->id,'reviewed_at'=>date('Y-m-d H:i:s')]);
               }

            }else{

                Chapter::where('course_id',$course_id)->whereNull('deleted_at')->update(['status'=>'1']);
                /*$browser = ActivityLog::getBrowser();           
                    $activity_data = array(
                        'model'=>'App\Model\Chapter',
                        'model_id'=>$course_id,
                        'type'=>'Chapter Reviewed',
                        'old_data'=>serialize(['status'=>'1']),
                        'new_data'=>serialize(['status'=>'1']),
                        'action_by'=>Auth::user()->id,
                        'ip'=>$_SERVER['REMOTE_ADDR'],
                        'browser'=>$browser['name'],
                        'platform'=>'Web',
                        'action_at'=>date('y-m-d H:i:s'),
                        );
                    ActivityLog::create($activity_data);*/
                $status = ChapterActivityLog::join('chapters', 'chapters.id', '=', 'chapter_activity_log.chapter_id')->where('chapters.course_id',$course_id)->update(['chapter_activity_log.reviewed_by'=>Auth::user()->id,'chapter_activity_log.reviewed_at'=>date('Y-m-d H:i:s')]);
            }
            
        }else{
            $send_data['success'] = false;
            $send_data['message'] = $error_message;  
        }
        
        if ($status) {

            $send_data['success'] = true;
            $send_data['message'] = 'Chapters has been reviewed successfully.'; 

        }else{ 
            if ($error_message==false) {
                $send_data['success'] = false;
                $send_data['message'] = 'Chapters could not be reviewed.'; 
            }
        }
        echo json_encode($send_data);
    }
    /**
     * @method chapter_approve
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function chapter_approve(Request $request){
        
        $data = $request->all();        

        $course_id = $data['course_id'];
        $all_chapter_id = $data['all_chapter_id'];
        
        if ($all_chapter_id!='') {

            $chapter_id_array = array_filter(explode(',', $all_chapter_id));
            
        }else{

            $chapter_id_array = Chapter::where('course_id',$course_id)->where('chapter_removed','2')->pluck('id');           
        }
        
        $error_message = $this->validateChapters($course_id, $chapter_id_array);
        $status = false;

        if ($error_message==false) {
            
            if (!empty($chapter_id_array)) {

               foreach ($chapter_id_array as $key => $value) {
                   
                   Chapter::where('id',$value)->whereNull('deleted_at')->update(['status'=>'1']);
                   
                   /*$browser = ActivityLog::getBrowser();           
                    $activity_data = array(
                        'model'=>'App\Model\Chapter',
                        'model_id'=>$value,
                        'type'=>'Chapter Approved',
                        'old_data'=>serialize(['status'=>'1']),
                        'new_data'=>serialize(['status'=>'1']),
                        'action_by'=>Auth::user()->id,
                        'ip'=>$_SERVER['REMOTE_ADDR'],
                        'browser'=>$browser['name'],
                        'platform'=>'Web',
                        'action_at'=>date('y-m-d H:i:s'),
                        );
                    ActivityLog::create($activity_data); */

                    $status = ChapterActivityLog::where('chapter_id',$value)->update(['approved_by'=>Auth::user()->id,'approved_at'=>date('Y-m-d H:i:s')]);
               }

            }else{

                Chapter::where('course_id',$course_id)->whereNull('deleted_at')->update(['status'=>'1']);
                 /*$browser = ActivityLog::getBrowser();           
                    $activity_data = array(
                        'model'=>'App\Model\Chapter',
                        'model_id'=>$course_id,
                        'type'=>'Chapter Approved',
                        'old_data'=>serialize(['status'=>'1']),
                        'new_data'=>serialize(['status'=>'1']),
                        'action_by'=>Auth::user()->id,
                        'ip'=>$_SERVER['REMOTE_ADDR'],
                        'browser'=>$browser['name'],
                        'platform'=>'Web',
                        'action_at'=>date('y-m-d H:i:s'),
                        );
                    ActivityLog::create($activity_data);*/
                $status = ChapterActivityLog::join('chapters', 'chapters.id', '=', 'chapter_activity_log.chapter_id')->where('chapters.course_id',$course_id)->update(['chapter_activity_log.approved_by'=>Auth::user()->id,'chapter_activity_log.approved_at'=>date('Y-m-d H:i:s')]);
            }
            
        }else{
            $send_data['success'] = false;
            $send_data['message'] = $error_message;  
        }        
        
        if ($status) {

            $send_data['success'] = true;
            $send_data['message'] = 'Chapters has been approved successfully.'; 

        }else{ 

            if ($error_message==false) {
                $send_data['success'] = false;
                $send_data['message'] = 'Chapters could not be approved.'; 
            }
        }
        echo json_encode($send_data);
    }
    /**
     * @method chapter publish
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function chapter_publish(Request $request){
        
        $data = $request->all();        

        $course_id = $data['course_id'];
        
        Chapter::where('course_id',$course_id)->where('chapter_removed','2')->whereNull('deleted_at')->update(['status'=>'1']);

        /*$browser = ActivityLog::getBrowser();           
        $activity_data = array(
            'model'=>'App\Model\Chapter',
            'model_id'=>$course_id,
            'type'=>'Chapter Published',
            'old_data'=>serialize(['status'=>'1']),
            'new_data'=>serialize(['status'=>'1']),
            'action_by'=>Auth::user()->id,
            'ip'=>$_SERVER['REMOTE_ADDR'],
            'browser'=>$browser['name'],
            'platform'=>'Web',
            'action_at'=>date('y-m-d H:i:s'),
            );
        ActivityLog::create($activity_data);*/

        $status = ChapterActivityLog::join('chapters', 'chapters.id', '=', 'chapter_activity_log.chapter_id')->where('chapters.course_id',$course_id)->update(['chapter_activity_log.published_by'=>Auth::user()->id,'chapter_activity_log.published_at'=>date('Y-m-d H:i:s')]);
        
        if ($status) {

            $send_data['success'] = true;
            $send_data['message'] = 'Chapters has been published successfully.'; 

        }else{ 

            $send_data['success'] = false;
            $send_data['message'] = 'Chapters could not be published.'; 
        }
        echo json_encode($send_data);
    }
    /**
     * @method chapter publish
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function courseVersion($course_id){

        $this->_data["breadcrumb"]["Create"] =  "javascript:void(0)";
        $this->_data["course_id"] = $course_id;
        return view(load_view('admin.courses.courseVersion.create'), $this->_data);
    }
    /**
     * @method add_course_version
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function add_course_version(Request $request){
        
        $data = $request->all();        
        
        $course_id      = $data['course_id'];
        $version_number = strtoupper($data['version_number']);

        $version_status = CourseVersion::checkDuplicateVersion($course_id, $version_number);
        $status         = '';

        if (!$version_status ) {
            
            /*
            * Create version entry
            */
            $store = CourseVersion::create(['course_id'=>$course_id,'version_no'=>$version_number,'created_by'=>Auth::user()->id]);
            
            $browser = ActivityLog::getBrowser();           
            $activity_data = array(
                'model'=>'App\Model\Course',
                'model_id'=>$course_id,
                'type'=>'Course Version Changed',
                'old_data'=>serialize(['status'=>'1']),
                'new_data'=>serialize(['status'=>'1']),
                'action_by'=>Auth::user()->id,
                'ip'=>$_SERVER['REMOTE_ADDR'],
                'browser'=>$browser['name'],
                'platform'=>'Web',
                'action_at'=>date('y-m-d H:i:s'),
                );
            ActivityLog::create($activity_data);
            
            /*
            * get origin al title from course table
            */
            $course_data = Course::where('id',$course_id)->select('org_title','course_version_id')->get()->first();
            
            /*
            * update course table with version number and title
            */
            Course::where('id',$course_id)->update(['course_version_id'=>$store->id, 'version_no'=>'('.$version_number.')', 'title'=> $course_data->org_title.' '.'('.$version_number.')']);

            Chapter::where('course_id',$course_id)->where('status','1')->update(['course_version_id'=>$store->id]);
            
            //Chapter::where('course_id',$course_id)->where('status','1')->where('chapter_removed','2')->update(['course_version_id'=>$store->id]);

            $course_activity_data   = CourseActivityLog::where('course_id',$course_id)->get()->first()->toArray();

            /*
            * create course version activity history
            */
            $course_activity_data['course_version_id'] =$course_data->course_version_id;
            $course_activity_data['course_id']         = $course_id;
            $course_activity_data['course_activity_log_id']  = $course_activity_data['id'];
            
            unset($course_activity_data['id']);
           
            $store_course_activity  = CourseVersionActivityLog::create($course_activity_data);
           
            /*
            * update course activity log table 
            */
            CourseActivityLog::where('course_id',$course_id)->update(['reviewed_by'=>Null,'approved_by'=>Null,'published_by'=>Null]);

            /*
            * creae chapter version activity log
            */
            $chapter_activities_log = Chapter::getChapterActivityLogs($course_id);
            
            if (!empty($chapter_activities_log)) {
                
                foreach ($chapter_activities_log as $key => $value) {
                    /*$value['course_version_id'] = $course_data->course_version_id;
                    $value['course_id']         = $course_id;
                    $value['chapter_activity_log_id']         = $value['id'];*/
                    $value = array(
                        'course_version_id'=>$course_data->course_version_id,
                        'course_id'=>$course_id,
                        'chapter_activity_log_id'=>$value['id'],
                        'chapter_id' => $value['chapter_id'],
                        'created_by' => Auth::user()->id,
                    );
                    unset($value['id']);
                    $store_chapter_activity  = ChapterVersionActivityLog::create($value);


                }
            }
           
            /*
            * update chapter activity log table
            */
            $status = ChapterActivityLog::join('chapters', 'chapters.id', '=', 'chapter_activity_log.chapter_id')->where('chapters.course_id',$course_id)->update(['chapter_activity_log.reviewed_by'=>Null, 'chapter_activity_log.approved_by'=>Null, 'chapter_activity_log.published_by'=>Null]);
        }

        if ($version_status ) {
            $send_data['success'] = false;
            $send_data['message'] = 'Duplicate version number found.'; 
        }
        else if ($status && !$version_status ) {

            $send_data['success'] = true;
            $send_data['message'] = 'Course version has been successfully added.'; 

        }else{ 

            $send_data['success'] = false;
            $send_data['message'] = 'Course version could not be added.'; 
        }
        echo json_encode($send_data);
    }
    /**
     * show edit form
     * @method edit
     * @param Courses $course
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function courseVersionMagage($course_id) {
        $this->_data["breadcrumb"]["Update"] =  "javascript:void(0)";
        $this->_data["data"] = Course::getCourseDetail($course_id);
        $this->_data["coursetypes"] = course_types();  
        $this->_data["courseCategory"] = Category::categoryList(); 
        $this->_data["courseChapters"] = Course::courseChaptersListWithDocumentsManageVersionFinal($course_id);
        $this->_data['course_last_version_id'] = Course::getCoruseLastVersionId($course_id,$this->_data["data"]->course_version_id);
        $this->_data['previous_chapters_id'] = ChapterVersion::getPreviousChaptersId($this->_data['course_last_version_id']);
        //dd($this->_data["courseChapters"] );
        /*
        * check whether course was published previously.
        * if yes than user can not edit course and previously added chapters
        */        
        $this->_data["readonly"] = 'readonly="readonly"';
       

        //dd($this->_data);
        return view(load_view('admin.courses.courseVersion.course_version_magage'), $this->_data);
    }
    
    /**
     * @method manageChapters
     * @desc review, approve and publish a chapters
     * @param course_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function manageChapter($course_id){

        $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;
        $this->_data["breadcrumb"]["Show"] =  "javascript:void(0)";

        $this->_data["data"] = Course::with(["courseTypeDetail", "courseCategoryDetail", "courseCreatedDetail", "courseUpdatedDetail", "courseReviewedDetail", "courseApprovedDetail", "coursePublishedDetail", "courseActivityDetail"])
                                ->where('id', $course_id)
                                ->first(); 
        //dd($this->_data["data"]);

        $this->_data["coursetypes"]          = course_types();  
        $this->_data["courseCategory"]       = Category::categoryList(); 
        $this->_data["courseChapters"]       = Course::courseChaptersListWithDocumentsManageVersion($course_id);
        $this->_data["chapterReviewStatus"]  = Chapter::chaptersReviewStatus($course_id);
        $this->_data["chapterApproveStatus"] = Chapter::chaptersApproveStatus($course_id);
        $this->_data["chapterPublishStatus"] = Chapter::chaptersPublishStatus($course_id);
        $this->_data["allCourseTest"]        = Course::getAllCourseTest($course_id);
        $this->_data["activities"]           = ActivityLog::where('model', 'App\Model\Course')->where('model_id', $course_id)->paginate($limit);
        $this->_data['currency'] = icon('currency');
         $permissions = \App\Model\UserPermission::getUserPermissions();       
            $this->_data['permissions'] = $permissions;
        
        return view(load_view('admin.courses.courses.manage_chapter'), $this->_data);
    }
    /**
     * @method manageCourse
    * @desc review, approve and publish a course
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function manageCourse($course_id){

        $limit = isset($filter_data['limit']) ? $filter_data['limit'] : $this->limit;
        $this->_data["breadcrumb"]["Show"] =  "javascript:void(0)";

        $this->_data["data"] = Course::with(["courseTypeDetail", "courseCategoryDetail", "courseCreatedDetail", "courseUpdatedDetail", "courseReviewedDetail", "courseApprovedDetail", "coursePublishedDetail", "courseActivityDetail"])
                                ->where('id', $course_id)
                                ->first(); 
        //dd($this->_data["data"]);

        $this->_data["coursetypes"]          = course_types();  
        $this->_data["courseCategory"]       = Category::categoryList(); 
        $this->_data["courseChapters"]       = Course::courseChaptersListWithDocumentsManageVersion($course_id);
        $this->_data["chapterReviewStatus"]  = Chapter::chaptersReviewStatus($course_id);
        $this->_data["chapterApproveStatus"] = Chapter::chaptersApproveStatus($course_id);
        $this->_data["chapterPublishStatus"] = Chapter::chaptersPublishStatus($course_id);
        $this->_data["allCourseTest"]        = Course::getAllCourseTest($course_id);
        $this->_data["activities"]           = ActivityLog::where('model', 'App\Model\Course')->where('model_id', $course_id)->paginate($limit);
        $this->_data['currency'] = icon('currency');
         $permissions = \App\Model\UserPermission::getUserPermissions();       
            $this->_data['permissions'] = $permissions;
        
        return view(load_view('admin.courses.courses.manage_course'), $this->_data);
    }
    /**
     * show add chapter form
     * @method addchaper
     * @param Courses $course
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addchapter(Request $request, $course_id) {

        $this->_data["breadcrumb"]["Update"] =  "javascript:void(0)";
        $this->_data["data"] = Course::getCourseDetailView($course_id);

        $this->_data["coursetypes"] = course_types();  
        $this->_data["courseCategory"] = Category::categoryList();
        $chapter_status = Chapter::checkCourseHasChapters($course_id); 
        $this->_data["courseChapters"] = array();
        if ($chapter_status) {
            $this->_data["courseChapters"] = Course::courseChaptersListWithDocuments($course_id,$this->_data["data"]->course_version_id);
        }
        
        $this->_data['currency'] = icon('currency');
        
        /*
        * check whether course was published previously.
        * if yes than user can not edit course and previously added chapters
        */
        if(Course::previousPublishStatus($course_id)){
            
            $request->session()->flash('error', 'Course can not be edit after publish.');
            return redirect()->to(route($this->routes["index"]));
        }

        //dd($this->_data["courseChapters"]);
        return view(load_view('admin.courses.courses.addchapter'), $this->_data);
    }
    /**
     * validate the course chapters
     * @method validateChapters
     * @param course id
     * @return error status
     */
    public function validateChapters($course_id, $chapter_id_array){

        $exam_setting = TestExamSetting::whereIN('chapter_id', $chapter_id_array)->where('status','1')->get();
        /*
        * if chapter does not have quesion setting data
        */
        if ($exam_setting->count()<1 || $exam_setting->count()!= count($chapter_id_array)) {
            return 'Please create an exam setting for the chapters';
        }
        
        $error = '';

        /*
        * chack all chapter has proper question
        */
        foreach ($exam_setting as $key => $value) {
            
            $big_number = 0;
            
            if ($value->no_of_question>$value->final_test_chapter_no_of_question) {
                $big_number = $value->no_of_question;
            }else{
                $big_number = $value->final_test_chapter_no_of_question;
            }
            if ($big_number<$value->practice_test_chapter_no_of_question) {
                $big_number = $value->practice_test_chapter_no_of_question;
            }
           
            $chapter_question_count = Chapter::getChapterQuestionCount($value->chapter_id);
            
            if ($chapter_question_count<$big_number) {
                $error .= "Add ".($big_number-$chapter_question_count)." more question in ".$value->title."<br>";
            }
        }

        if ($error!='') {
            return $error;
        }
        /*
        * chack all chapter review questions
        */
        foreach ($exam_setting as $key => $value) {
            
            //$big_number = 0;
            
           /* if ($value->no_of_question>$value->final_test_chapter_no_of_question) {
                $big_number = $value->no_of_question;
            }else{
                $big_number = $value->final_test_chapter_no_of_question;
            }
            if ($big_number<$value->practice_test_chapter_no_of_question) {
                $big_number = $value->practice_test_chapter_no_of_question;
            }*/
           
            $chapter_question_count = Chapter::getUnReviewedQuestionCount($value->chapter_id);
            
            if ($chapter_question_count>0) {
                $error .= $value->title." has un reviewed questions.<br>";
            }
        }

        if ($error!='') {
            return $error;
        }
        /*
        * chack all chapter approved questions
        */
        foreach ($exam_setting as $key => $value) {
            
            //$big_number = 0;
            
           /* if ($value->no_of_question>$value->final_test_chapter_no_of_question) {
                $big_number = $value->no_of_question;
            }else{
                $big_number = $value->final_test_chapter_no_of_question;
            }
            if ($big_number<$value->practice_test_chapter_no_of_question) {
                $big_number = $value->practice_test_chapter_no_of_question;
            }*/
           
            $chapter_question_count = Chapter::getUnApprovedQuestionCount($value->chapter_id);
            
            if ($chapter_question_count>0) {
                $error .= $value->title." has un approved questions.<br>";
            }
        }

        if ($error!='') {
            return $error;
        }

        /*
        * check all chapter has active status
        */
        $inactive_chapter_data = Chapter::where('status',2)->whereIn('id',$chapter_id_array)->get();

        if ($inactive_chapter_data->count()>0) {
            
            foreach ($inactive_chapter_data as $key => $value) {
                $error .= $value->title." has Inactive status.\n";
            }
        }
        if ($error!='') {
            return $error;
        }
        return false;
    }
    /**
     * validate the course
     * @method validateCourse
     * @param course id
     * @return error status
     */
    public function validateCourse($course_id){

        /*
        * check All Test Exam Added In Course
        */
        $tests_exam_added = Course::checkAllTestExamAddedInCourse($course_id);

        if ($tests_exam_added!=false) {

           return $tests_exam_added;

        }

        /*
        * check Number Of Question In Final Test
        */
        $final_test_question_status = Course::checkNumberOfQuestionInFinalTest($course_id);
        
        if ($final_test_question_status!=false) {

            return $tests_exam_added;

        }

        /*
        * check Number Of Question In Practice Test
        */
        $practice_test_question_status = Course::checkNumberOfQuestionInPracticeTest($course_id);
        
        if ($practice_test_question_status!=false) {

            return $practice_test_question_status;

        }

        /*
        * check Number Of Question In Chapter Test
        */
        $chapter_test_question_status = Course::checkNumberOfQuestionInChapterTest($course_id);
        
        if ($chapter_test_question_status!=false) {

            return $chapter_test_question_status;

        }

        /*
        * chapter Publish Status
        */
        $chapter_publish_status = Course::chapterPublishStatus($course_id);
        
        if ($chapter_publish_status!=false) {

            return $chapter_publish_status;

        }
    }
    /**
     * @method save_chapter_detail
     * @desc add multiple chapter of course
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function change_chapter_status(Request $request){
                
        $data = $request->all();
        $data = $request->except("_token");
        
        if (isset($data['chapter_id']) && $data['chapter_id']!='') {  
                          
            /*
            * change status
            */
            if ($data['status']==1) {
                $chapter_removed_status = 2;
            }else{
                $chapter_removed_status = 1;
            }
            $store = Chapter::where('id',$data['chapter_id'])->update(['status'=>$data['status'],'chapter_removed'=>$chapter_removed_status]);
            $send_data['success'] = true;
            $send_data['message'] = 'Chapter status change successfully.';
        }
        echo json_encode($send_data);
        
    }
    /**
     * @method save_chapter_detail
     * @desc add multiple chapter of course
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function viewCourseVersions($course_id){
        /*
        * course current version
        */
        $this->_data["current_version"] = Course::with(["courseTypeDetail", "courseCategoryDetail", "courseCreatedDetail", "courseUpdatedDetail", "courseReviewedDetail", "courseApprovedDetail", "coursePublishedDetail", "courseActivityDetail","courseVersionChapters"])
                                    ->where('id', $course_id)
                                    ->first();
       
        foreach ($this->_data["current_version"]['courseVersionChapters'] as $key => $value) {

             $value->chapter_creater = Chapter::getChapterCreaterName($value->id);   
             $value->chapter_rviewer = Chapter::getChapterReviewerName($value->id);
             $value->chapter_approver = Chapter::getChapterApproverName($value->id);
             $value->chapter_publisher = Chapter::getChapterPublisherName($value->id);
        }

        /*
        * course old versions
        */
        $this->_data["old_versions"] = CourseVersionActivityLog::join('courses','courses.id', '=','course_version_activity_log.course_id')->join('course_version', 'course_version.id', '=', 'course_version_activity_log.course_version_id')
        ->select(
            'course_version_activity_log.id', 
            'course_version_activity_log.course_version_id', 
            'course_version_activity_log.course_id', 
            'course_version_activity_log.reviewed_at', 
            'course_version_activity_log.approved_at', 
            'course_version_activity_log.published_at', 
            'course_version_activity_log.created_at', 
            'course_version_activity_log.updated_at', 
            'course_version_activity_log.deleted_at', 
            'course_version_activity_log.course_activity_log_id', 
            'courses.category_id', 
            'course_version.version_no', 
            'courses.title', 
            'courses.org_title', 
            'courses.short_description', 
            'courses.description', 
            'courses.price', 
            'courses.after_final_exam_price', 
            'courses.after_course_expiry_price', 
            'courses.course_duration', 
            'courses.course_type', 
            'courses.status'
            )
        ->where('course_version_activity_log.course_id',$course_id)
        ->orderBy('course_version_activity_log.id','DESC')
        ->get();

       if ($this->_data["old_versions"]->count()>0) {
            foreach ($this->_data["old_versions"] as $key => $value) {
             
              $value->course_creater = Course::getCourseCreaterName($value->course_id);
              $value->course_reviewer = Course::getCourseReviewerName($value->course_id);
              $value->course_approver = Course::getCourseApproverName($value->course_id);
              $value->course_publisher = Course::getCoursePublisherName($value->course_id);

              $value->courseChapters = ChapterVersionActivityLog::getChapters($value->course_version_id);
              //dd( $value->courseChapters);
              
              foreach ($value->courseChapters as $key => $value_innner) {
                    $value_innner->chapter_creater = Chapter::getChapterCreaterNameVersion($value_innner->chapter_id);   
                    $value_innner->chapter_reviewer = Chapter::getChapterReviewerNameVersion($value_innner->chapter_id);
                    $value_innner->chapter_approver = Chapter::getChapterApproverNameVersion($value_innner->chapter_id);
                    $value_innner->chapter_publisher = Chapter::getChapterPublisherNameVersion($value_innner->chapter_id);
              }
            }
        } 
       
       //dd($this->_data["old_versions"]);
       return view(load_view('admin.courses.courseVersion.view_course_versions'), $this->_data);
        
    }
    /**
     * send push notification to user when course published
     *
     * @method sendCoursePublishNotification
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendCoursePublishNotification($params){

        $user_data = User::where('status','1')->whereIn('role_id',[6,7])->get();

        if ($user_data->count()>0) {
            
            foreach ($user_data as $key => $value) {
                
                $send_status = 2;

                if ($params['module_type']=='course_publish') {
                    $title = 'New Course Created';
                    $message = $params['title'].' Course Created';
                }else{
                    $title = 'New Course Version Launche';
                    $message = $params['title'].' Course Version Launche';
                }
                
                $device_id = $value->device_id;            

                $response = NotificationHelper::sendNotification($message, $device_id, $title);
                $result = json_decode($response);
                
                if (is_object($result) && property_exists($result, 'success') && $result->success) {
                    $send_status = 1;
                }
                
                $data = array(
                    'module_id'=>$params['id'],
                    'user_id'=>$value->id,
                    'icon'=>Null,
                    'module_type'=>$params['module_type'],
                    'message'=>$message,
                    'is_send'=>$send_status
                    );
                
                UserNotification::create($data);
            }
        }
        
    }

}
