<?php

namespace App\Http\Middleware;

use Closure;

class Permission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // allow all routes to super admin
        if(get_user_info("role_id") == 1){
            return $next($request);
        }

        // check permission for all other admin users

        $current_route = current_route();
        
        $explodeCurrentPath = explode('.', $current_route);

        if(is_route_for_permission($current_route)){

            if(is_permit($current_route)){
                return $next($request);
            }

            if (in_array('admin', $explodeCurrentPath)) {
                $request->session()->flash('error', 'Sorry, You are not an authorized user to perform this action.');
                return redirect()->to(route('home'));
                 //return response()->view("admin.error.unauthorise", ["menus" => get_menus()]);
            } else {
                $request->session()->flash('error', 'Sorry, You are not an authorized user to perform this action.');
                return redirect()->to(route('home'));
                // return response()->view("front.error.unauthorise", ["menus" => get_menus()]);
            }
        }

        return $next($request);
    }
}
